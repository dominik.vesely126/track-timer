import {
  ApiMethod,
  ApiResponseError,
  Either,
  TaskEither,
  TE,
} from "../lib/types";
import * as express from "express";
import { json } from "body-parser";
import * as cors from "cors";
import { C, camelCaseToUnderscore, Type, E } from "../lib/utils";
import { pipe } from "../lib/utils/fptsUtil";

export type ExpressRequest = express.Request;

interface ErrorResponse {
  contentType: "JSON" | "PLAIN" | "XML";
  statusCode: number;
  statusText: string;
}

export const server = express();

server.use(cors());
server.use(json());

export interface Router {
  addRoute<E extends ApiResponseError, T, P = any>(
    options: RouteOptions<E, T, P>
  ): Promise<void>;
}

export const router: Router = {
  addRoute,
};

type HttpMethod = "GET" | "POST" | "PUT" | "DELETE" | "ALL";
// type TransformCallback<S, D> = (source: S) => D;
type OutRouteCallback = (req: express.Request) => string;
type OutRoute = string | OutRouteCallback;

type ApiParams = Record<string, any>;

interface RouteOptions<E, T, P extends ApiParams> {
  inRoute: string;
  //   outRoute: OutRoute;
  paramNames?: Props<P>;
  method: HttpMethod;
  codec?: C;
  result: (req: express.Request, params?: P) => TaskEither<E, T>;
}

async function addRoute<E extends ApiResponseError, T, P>(
  options: RouteOptions<E, T, P>
): Promise<void> {
  server.all(options.inRoute, async function (req, res, next) {
    if (
      options.method !== "ALL" &&
      !compareMethod(options.method, req.method)
    ) {
      next("route");
      return;
    } else {
      try {
        const getParams = processParams(req, options.paramNames);
        const encode = options.codec?.encode ?? ((x) => x);

        const either: Either<E, T> = await pipe(
          options.result(req, getParams),
          TE.map(encode)
        )();

        switch (either._tag) {
          case "Left":
            const { statusCode, code, message, error } = either.left;
            // if (res.headersSent) {
            //   next(either.left);
            //   return;
            // }
            res.status(statusCode).send(either.left);
            next("router");
            return;

          case "Right":
            res.send(either.right);
            break;
        }
      } catch (error) {
        handleGeneralRouteError(res, error);
        next("router");
        return;
      }
    }
    next();
  });
}

type PropType = "string" | "number" | "date" | "object" | C;

type Props<T> = {
  [P in keyof T]: { type: PropType } | PropType;
};

function getProperty<T, K extends keyof T>(obj: T, key: K): T[K] {
  return obj[key];
}

function processParams<T>(
  req: express.Request,
  props?: Props<T>
): T | undefined {
  if (!props) {
    return undefined;
  }
  const method: ApiMethod = <ApiMethod>req.method.toUpperCase();
  const result = {} as T;

  for (const name in props) {
    const prop = props[name];
    let type: PropType;

    switch (typeof prop) {
      case "object":
        const encoder = getEncoder(prop);
        if (encoder) {
          type = encoder;
        } else {
          ({ type } = prop as any);
        }
        break;
      case "string":
        type = prop;
    }

    result[name] = parseValueByMethod(method, req, name, type);
  }

  return result;
}

function getEncoder(type: any): C | undefined {
  return typeof type.encode === "function" ? type : undefined;
}

function parseValueByMethod(
  method: ApiMethod,
  req: express.Request,
  name: string,
  type: PropType
) {
  const queryValue = req.query[camelCaseToUnderscore(name)];

  if (queryValue !== undefined) {
    return parseValue(queryValue, type);
  }

  switch (method) {
    case "POST":
    case "PUT":
      return parseValue(req.body && req.body[name], type);
  }

  return queryValue;
}

function parseValue(value: any, type: PropType) {
  if (value === undefined || value === null) {
    return value;
  }

  const encoder = getEncoder(type);

  if (encoder) {
    const validation = encoder.decode(value);
    switch (validation._tag) {
      case "Right":
        return validation.right;

      case "Left":
        throw new Error(
          `Decode value '${value}' with decoder '${encoder.name}' failed.`
        );
    }
  }

  switch (type) {
    case "date":
      return new Date(value);
    case "number":
      return parseFloat(value);

    case "object":
      return value;

    default:
      return `${value}`;
  }
}

function handleGeneralRouteError(res: express.Response, error) {
  let statusCode, statusText, contentType;

  if (error.response) {
    statusCode = error.response.status;
    statusText = error.response.statusText;

    if (error.response.data) {
      contentType = "application/json";
      statusText = JSON.stringify(error.response.data);
    }
  }

  const result = mergeDefaults(
    {
      contentType: "plain/text",
      statusCode: 500,
      statusText: "General error",
    },
    {
      statusCode,
      statusText,
      contentType,
    }
  );

  return res
    .contentType(result.contentType)
    .status(result.statusCode)
    .send(result.statusText);
}

function mergeDefaults<T extends Record<string, any>>(
  defaults: T,
  overwrite: Partial<T>
): T {
  const keys = new Set(
    Object.keys(defaults).concat(Object.keys(overwrite))
  ).values();
  const result = {} as T;

  for (const name of keys) {
    if (
      isNullOrUndefined(overwrite[name]) &&
      !isNullOrUndefined(defaults[name])
    ) {
      //@ts-ignore
      result[name] = defaults[name];
    } else {
      //@ts-ignore
      result[name] = overwrite[name];
    }
  }

  return result;
}

function isNullOrUndefined(value: any) {
  return value === null || value === undefined || value === "";
}

function compareMethod(m1: HttpMethod, m2: string) {
  return m1.toLowerCase() === m2.toLowerCase();
}
