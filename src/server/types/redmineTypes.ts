import {
  buildDecoder,
  C,
  DateFromSqlString,
  DurationTime,
  SafeDurationFromHours,
  T,
  t,
} from "../../lib/utils";

export type RMNativeCurrentUser = t.TypeOf<typeof RMNativeCurrentUser>;
export const RMNativeCurrentUser = t.type({
  user: t.type({
    id: t.number,
    firstname: t.string,
    lastname: t.string,
  }),
});

export type RMNativeUser = t.TypeOf<typeof RMNativeUser>;
export const RMNativeUser = t.type({
  id: t.number,
  name: t.string,
});

export interface RMNativeError {
  errors: string[];
}

export type RMNativeProject = t.TypeOf<typeof RMNativeProject>;

const RMNativeProject = t.type({
  id: t.number,
  name: t.string,
});

export interface RMNativeIssueCollection {
  issues: RMNativeIssue[];
}

export type RMNativeIssue = t.TypeOf<typeof RMNativeIssue>;

const RMNativeIssue = t.type({
  id: t.number,
  subject: t.string,
  project: RMNativeProject,
  custom_fields: t.array(
    t.type({
      id: t.number,
      name: t.string,
      value: t.string,
    })
  ),
});

export type RMNativeActivity = t.TypeOf<typeof RMNativeActivity>;

const RMNativeActivity = t.type({
  id: t.number,
  name: t.string,
});

export interface RMNativeActivityCollection {
  time_entry_activities: RMNativeActivity[];
}

export type RMNativeTimeEntry = t.TypeOf<typeof RMNativeTimeEntry>;

export const RMNativeTimeEntry = t.type({
  issue_id: t.number,
  spent_on: DateFromSqlString,
  hours: SafeDurationFromHours,
  activity_id: t.number,
  comments: t.union([t.string, t.undefined]),
  user_id: t.number,
});

export type RMNativeAddTimeEntry = t.TypeOf<typeof RMNativeAddTimeEntry>;
export const RMNativeAddTimeEntry = t.type({
  time_entry: RMNativeTimeEntry,
});

export type RMNativeTimeEntryHours = t.TypeOf<typeof RMNativeTimeEntryHours>;

export const RMNativeTimeEntryHours = t.type({
  id: t.number,
  project: RMNativeProject,
  issue: t.type({
    id: t.number,
  }),
  user: RMNativeUser,
  activity: RMNativeActivity,
  hours: SafeDurationFromHours,
  spent_on: DateFromSqlString,
});

export type RMNativeTimeEntryCollection = t.TypeOf<
  typeof RMNativeTimeEntryCollection
>;
export const RMNativeTimeEntryCollection = t.type({
  time_entries: t.array(RMNativeTimeEntryHours),
});

export interface RMNativeTimeEntryDayHour {
  issueId: number;
  projectId: number;
  user: RMNativeUser;
  date: Date;
  duration: DurationTime;
}

export type RMNativeSearchResultCollection =
  | {
      results: RMNativeSearchResult[];
    }
  | string;

export interface RMNativeSearchResult {
  id: number;
  title: string;
  type: "issue";
  url: string;
}
