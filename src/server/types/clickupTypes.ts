import {
  C,
  DateFromISOString,
  DateFromUnixTimestamp,
  DateFromUnixTimestampString,
  DurationFromNumberMillis,
  DurationFromStringMillis,
  t,
} from "../../lib/utils";

export interface ClickNativeError {
  ECODE: string;
  err: string;
}
export interface ClickNativeSpaceCollection {
  spaces: ClickNativeSpace[];
}

export interface ClickNativeSpace {
  id: string;
  name: string;
}

export interface ClickNativeTeamCollection {
  teams: ClickNativeTeam[];
}

export interface ClickNativeTeam {
  id: string;
  name: string;
}

export interface ClickNativeFolderCollection {
  folders: ClickNativeFolder[];
}

export interface ClickNativeFolder {
  id: string;
  name: string;
}

export interface ClickNativeListCollection {
  lists: ClickNativeList[];
}

export interface ClickNativeList {
  id: string;
  name: string;
}

export interface ClickNativeCurrentUser {
  user: ClickNativeUser;
}

export interface ClickNativeUser {
  id: number;
  username: string;
}

export interface ClickNativeTask {
  id: string;
  name: string;
  //   custom_fields: ClickNativeTaskCustomField[];
  list: {
    id: string;
  };
  folder: {
    id: string;
  };
  space: {
    id: string;
  };
}

export interface ClickNativeTaskCollection {
  tasks: ClickNativeTask[];
}

export interface ClickNativeTimesheetCollection {
  data: ClickNativeTimesheet[];
}

const ClickNativeTimeEntryBase = t.type({
  start: DateFromUnixTimestamp,
  billable: t.boolean,
  //   assignee: t.number,
  tid: t.string,
  description: t.string,
});

export type ClickNativeNewTimeEntry = t.TypeOf<typeof ClickNativeNewTimeEntry>;

export const ClickNativeNewTimeEntry = t.intersection([
  ClickNativeTimeEntryBase,
  t.type({
    stop: DateFromUnixTimestamp,
    assignee: t.number,
  }),
]);

export type ClickNativeUpdateTimeEntry = t.TypeOf<
  typeof ClickNativeUpdateTimeEntry
>;

export const ClickNativeUpdateTimeEntry = t.intersection([
  ClickNativeTimeEntryBase,
  t.type({
    id: t.string,
    end: DateFromUnixTimestamp,
  }),
]);

// export type ClickNativeTimeEntryResult = t.TypeOf<
//   typeof ClickNativeTimeEntryResult
// >;

function ClickNativeTimeEntryResultFnc(dateC: C, timeC: C) {
  return t.type({
    id: t.string,
    task: t.type({
      id: t.string,
      name: t.string,
    }),
    user: t.type({
      id: t.number,
      username: t.string,
    }),
    billable: t.boolean,
    start: dateC,
    end: dateC,
    duration: timeC,
    description: t.string,
  });
}

export type ClickNativeTimeEntryResult = t.TypeOf<
  typeof ClickNativeTimeEntryResultNumber
> &
  t.TypeOf<typeof ClickNativeTimeEntryResultString>;

const ClickNativeTimeEntryResultNumber = ClickNativeTimeEntryResultFnc(
  DateFromUnixTimestamp,
  DurationFromNumberMillis
);

const ClickNativeTimeEntryResultString = ClickNativeTimeEntryResultFnc(
  DateFromUnixTimestampString,
  DurationFromStringMillis
);

export type ClickNativeNewTimeEntryResult = t.TypeOf<
  typeof ClickNativeNewTimeEntryResult
>;

export const ClickNativeNewTimeEntryResult = t.type({
  data: ClickNativeTimeEntryResultNumber,
});

export type ClickNativeUpdateTimeEntryResult = t.TypeOf<
  typeof ClickNativeUpdateTimeEntryResult
>;

export const ClickNativeUpdateTimeEntryResult = t.type({
  data: t.array(ClickNativeTimeEntryResultNumber),
});

export type ClickNativeDeleteTimeEntryResult = t.TypeOf<
  typeof ClickNativeDeleteTimeEntryResult
>;

export const ClickNativeDeleteTimeEntryResult = t.type({
  data: t.array(ClickNativeTimeEntryResultString),
});

export interface ClickNativeTimesheet {
  id: string;
  task?: Pick<ClickNativeTask, "id" | "name">;
  user: ClickNativeUser;
  start: string;
  end: string;
  duration: string;
}

export type ClickOutTimesheet = Omit<
  ClickNativeTimesheet,
  "start" | "end" | "duration"
> & {
  start: Date;
  end: Date;
  durationMilis: number;
};

export interface ClickNativeCustomListFields {
  listId: number;
  fields: ClickNativeCustomField[];
}

export interface ClickNativeCustomField {
  id: string;
  name: string;
  type: string;
}

export interface ClickNativeTaskCustomField extends ClickNativeCustomField {
  value: string;
}

export interface ClickNativeCustomFieldCollection {
  fields: ClickNativeCustomField[];
}

export interface ClickNativeView {
  id: string;
  filters: {
    search: string;
  };
}

export interface ClickNativeGenericView {
  list: {
    groups: {
      task_ids: string[];
    }[];
  };
}

type ValueOf<T> = T[keyof T];

export type ClickNativeParentType = ValueOf<typeof ClickNativeParentType>;

export const ClickNativeParentType = Object.freeze({
  space: 4 as 4,
  folder: 5 as 5,
  list: 6 as 6,
  team: 7 as 7,
});

export interface ClickNativeGenericViewData {
  id: string;
  name: string;
  parent: {
    id: string;
    type: ClickNativeParentType;
  };
  type: number;
  filters: {
    search: string;
    show_closed: boolean;
    search_custom_fields: boolean;
    search_description: boolean;
    search_name: boolean;
    op: "AND" | "OR";
  };
}

export interface ClickServerList {
  id: number;
  name: string;
  customFields: ClickNativeCustomField[];
}

export function clickOutTimesheetMapper(
  original: ClickNativeTimesheet
): ClickOutTimesheet {
  return {
    ...original,
    start: new Date(Number(original.start)),
    end: new Date(Number(original.end)),
    durationMilis: Number(original.duration),
  };
}
