import * as NodeCache from "node-cache";
import { DurationTime, parseSecondsFromDuration } from "../../lib/utils";

const cache = new NodeCache();

export type CacheGetValue<T> = () => Promise<T>;

export interface CacheParams<T> {
  key: string;
  ttl: DurationTime;
  getValue: CacheGetValue<T>;
}

export function fromCacheHour<T>(params: Omit<CacheParams<T>, "ttl">) {
  return fromCache({
    ...params,
    ttl: new DurationTime({
      hours: 1,
      minutes: 0,
      seconds: 0,
    }),
  });
}

async function fromCache<T>({ key, ttl, getValue }: CacheParams<T>) {
  let value = cache.get<T>(key);

  if (value === undefined) {
    const seconds = parseSecondsFromDuration(ttl);
    value = await getValue();
    cache.set<T>(key, value, seconds);
  }

  return value;
}
