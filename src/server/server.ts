import { SERVER_HOST, SERVER_PORT } from "../config";
import { useClickup, useNativeClickup } from "./apis/clickupServer";
import { useIFS, useNativeIFS } from "./apis/ifsServer";
import { useNativeRedmine, useRedmine } from "./apis/redmineServer";
import { server, router } from "./router";

useNativeRedmine(router);
useNativeClickup(router);
useNativeIFS(router);

useClickup(router);
useRedmine(router);
useIFS(router);

// Add error handling middleware that Express will call
// in the event of malformed JSON.
server.use(function (err, req, res, next) {
  // 'SyntaxError: Unexpected token n in JSON at position 0'
  //   err.message;
  next(err);
});

server.listen(SERVER_PORT, () => {
  console.log(`listening... port ${SERVER_PORT}.`);
});
