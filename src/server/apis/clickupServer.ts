import {
  ClickupConfig,
  CLICK_OAUTH_CLIENT_ID,
  IfsConfig,
} from "../../../src/config";
import { ApiError } from "../../lib/Errors";
import {
  ApiResponseError,
  ClickApiResponse,
  ClickApiResponseError,
  ClickApiTimesheetOptions,
  ClickApi,
  ClickTask,
  ClickApiTasksByCustomFieldParams,
  ClickTimesheet,
  TE,
  ClickUser,
  ClickTeam,
  ClickSpace,
  ClickTimesheetHours,
  ClickExternalName,
  ClickApiUpdateTaskParams,
  ClickApiTaskByIdParams,
  ClickApiUpdateExternalIdsParams,
  ClickApiExternalIds,
  ClickApiSearchTasksParams,
  ClickUpdateTimesheetParams,
  Task,
  ClickUpdateTimesheetItem,
  Either,
  TaskEither,
  ClickUpdateTimesheetResult,
  ClickApiAuthorizeParams,
  ClickApiAuthorizeResult,
  ClickApiLoginParams,
  ClickApiLoginResult,
  ClickApiRefreshResult,
} from "../../lib/types";
import {
  A,
  addDuration,
  arrayEveryAll,
  dateEndOfDay,
  dateMinAndMax,
  dateStartOfDay,
  dateStartOfDayUtc,
  DurationTime,
  E,
  formatDateSql,
  getPositiveNumber,
  getUpdateSelection,
  parseDurationFromMilis,
  pipe,
  T,
  t,
} from "../../lib/utils";
import { Router } from "../router";
import { ApiMethod, ApiParams } from "../../lib/types";
import {
  ClickNativeCustomField,
  ClickNativeCustomFieldCollection,
  ClickNativeTaskCustomField,
  ClickNativeCustomListFields,
  ClickNativeFolder,
  ClickNativeFolderCollection,
  ClickNativeList,
  ClickNativeListCollection,
  ClickNativeTeamCollection,
  ClickNativeSpaceCollection,
  ClickNativeTask,
  ClickNativeTaskCollection,
  ClickNativeTimesheetCollection,
  ClickOutTimesheet,
  ClickServerList,
  clickOutTimesheetMapper,
  ClickNativeTimesheet,
  ClickNativeCurrentUser,
  ClickNativeError,
  ClickNativeView,
  ClickNativeGenericView,
  ClickNativeGenericViewData,
  ClickNativeParentType,
  ClickNativeNewTimeEntry,
  ClickNativeUpdateTimeEntry,
  ClickNativeNewTimeEntryResult,
  ClickNativeUpdateTimeEntryResult,
  ClickNativeDeleteTimeEntryResult,
  ClickNativeTimeEntryResult,
} from "../types/clickupTypes";
import {
  ApiServerIncomingRequest,
  createApi,
  createApiWithRequest,
  handleApiServerError,
} from "./apiServer";
import { CacheGetValue, fromCacheHour } from "../utils";
import { CLICK_OAUTH_CLIENT_SECRET } from "../serverConfig";

const clickupApiWithRequest = createApiWithRequest({
  baseServerUrl: ClickupConfig.CLICK_URL,
  headers: {
    "Content-Type": "application/json",
    // Authorization: ClickupConfig.CLICK_API_KEY,
  },
  allowedReqHeaders: ["Authorization"],
});

const clickupWebWithRequest = createApiWithRequest({
  baseServerUrl: ClickupConfig.CLICK_WEB_URL,
  headers: {
    "Content-Type": "application/json",
  },
  allowedReqHeaders: ["Web-Authorization"],
  transformHeaders: (headers) => {
    const webToken = headers["web-authorization"];
    delete headers["web-authorization"];
    if (webToken) {
      headers["authorization"] = `Bearer ${webToken}`;
    }

    return headers;
  },
});

export function useNativeClickup(router: Router) {
  router.addRoute<ApiResponseError, any, any>({
    method: "ALL",
    inRoute: "/click_native/*",
    result: (req) => {
      let path = req.url.replace("/click_native/", "");

      const matches = path.match(/^v(\d)\//);

      if (!matches) {
        path = `api/v2/${path}`;
      } else {
        path = path.substring(matches[0].length);

        switch (Number(matches[1])) {
          case 1:
            path = `v1/${path}`;
            break;

          default:
            path = `api/v2/${path}`;
        }
      }

      return tryCatch(() =>
        clickupApiWithRequest.all({
          request: req,
          baseUrl: ClickupConfig.CLICK_WEB_URL,
          path,
          method: <ApiMethod>req.method,
          data: req.body,
        })
      );
    },
  });
}

type TimeEntryWithoutDuration = Omit<ClickOutTimesheet, "durationMilis"> & {
  seconds: number;
};

export function useClickup(router: Router) {
  router.addRoute<ClickApiResponseError, any, any>({
    method: "GET",
    paramNames: {
      code: "string",
    },
    inRoute: "/click/oauth/token",
    result: (req, params) => api(req).oauthToken(params),
  });

  router.addRoute<
    ClickApiResponseError,
    ClickApiLoginResult,
    ClickApiLoginParams
  >({
    method: "POST",
    paramNames: {
      email: "string",
      password: "string",
    },
    inRoute: "/click/login",
    result: (req, params) => api(req).login(params),
  });

  router.addRoute<ClickApiResponseError, any, any>({
    method: "POST",
    inRoute: "/click/refresh",
    result: (req) => api(req).refresh(),
  });

  router.addRoute<
    ClickApiResponseError,
    ClickTimesheet[],
    ClickApiTimesheetOptions
  >({
    method: "GET",
    inRoute: "/click/timesheet",
    codec: t.array(ClickTimesheet),
    paramNames: {
      teamId: "number",
      userId: "number",
      start: "date",
      end: "date",
    },
    result: (req, params) => api(req).getTimesheets(params),
  });

  router.addRoute<
    ClickApiResponseError,
    ClickUpdateTimesheetResult[],
    ClickUpdateTimesheetParams
  >({
    method: "PUT",
    inRoute: "/click/timesheet/update",
    codec: t.array(ClickUpdateTimesheetResult),
    paramNames: {
      items: t.array(ClickUpdateTimesheetItem),
      teamId: "number",
    },
    result: (req, params) => api(req).updateTimesheets(params),
  });

  router.addRoute<
    ClickApiResponseError,
    ClickTimesheetHours[],
    ClickApiTimesheetOptions
  >({
    method: "GET",
    inRoute: "/click/timesheet/hours",
    codec: t.array(ClickTimesheetHours),
    paramNames: {
      teamId: "number",
      userId: "number",
      start: "date",
      end: "date",
    },
    result: (req, params) => api(req).getTimesheetsHours(params),
  });

  router.addRoute<ClickApiResponseError, ClickTask, ClickApiTaskByIdParams>({
    method: "GET",
    inRoute: "/click/task/id",
    paramNames: {
      taskId: "string",
    },
    codec: ClickTask,
    result: (req, params) => api(req).getTaskById(params.taskId),
  });

  //   router.addRoute<
  //     ClickApiResponseError,
  //     ClickTask[],
  //     ClickApiTasksByCustomFieldParams
  //   >({
  //     method: "GET",
  //     inRoute: "/click/task",
  //     paramNames: {
  //       spaceId: "number",
  //       field: "string",
  //       value: "string",
  //     },
  //     result: (req, { spaceId, field, value }) =>
  //       getTasksByCustomField({
  //         spaceId,
  //         field,
  //         value: value,
  //       }),
  //   });

  router.addRoute<ClickApiResponseError, ClickUser, any>({
    method: "GET",
    inRoute: "/click/user/current",
    result: (req) => api(req).getCurrentUser(),
  });

  router.addRoute<ClickApiResponseError, ClickTeam[], any>({
    method: "GET",
    inRoute: "/click/team",
    result: (req) => api(req).getTeams(),
  });

  router.addRoute<ClickApiResponseError, ClickSpace[], { teamId: number }>({
    method: "GET",
    inRoute: "/click/space",
    paramNames: {
      teamId: "number",
    },
    result: (req, { teamId }) => api(req).getSpaces(teamId),
  });

  router.addRoute<
    ClickApiResponseError,
    ClickTask,
    ClickApiUpdateExternalIdsParams
  >({
    method: "POST",
    inRoute: "/click/task/field",
    paramNames: {
      //   externalIds: ClickApiExternalIds,
      externalIds: "object",
      taskId: "string",
    },
    codec: ClickTask,
    result: (req, params) => api(req).updateTaskExternalIds(params),
  });

  router.addRoute<ClickApiResponseError, boolean, ClickApiUpdateTaskParams>({
    method: "PUT",
    inRoute: "/click/task",
    paramNames: {
      taskId: "string",
      data: "object",
    },
    result: (req, params) => api(req).updateTask(params),
  });

  router.addRoute<
    ClickApiResponseError,
    ClickTask[],
    ClickApiSearchTasksParams
  >({
    method: "GET",
    inRoute: "/click/task/search",
    paramNames: {
      teamId: "number",
      query: "string",
    },
    result: (req, params) => api(req).searchTasks(params),
  });
}

interface ClickApiServer {
  oauthToken(
    params: ClickApiAuthorizeParams
  ): ClickApiResponse<ClickApiAuthorizeResult>;

  login(params: ClickApiLoginParams): ClickApiResponse<ClickApiLoginResult>;

  refresh(): ClickApiResponse<ClickApiRefreshResult>;
}

function api(request: ApiServerIncomingRequest): ClickApi & ClickApiServer {
  //   request.headers["host"] = "api.clickup.com";
  const clickupApi = createApi(request, clickupApiWithRequest);
  const clickupWeb = createApi(request, clickupWebWithRequest);

  return {
    getTimesheets,
    getTimesheetsHours,
    getTasksByCustomField,
    getTaskById,
    getCurrentUser,
    getSpaces: (teamId: number) => tryCatch(() => getSpaces(teamId)),
    getTeams,
    updateTaskExternalIds,
    updateTask,
    searchTasks,
    updateTimesheets,
    oauthToken,
    login,
    refresh,
  };

  function oauthToken({ code }: ClickApiAuthorizeParams) {
    return tryCatch(() => {
      return clickupApi.post<ClickApiAuthorizeResult, any>("/oauth/token", {
        data: {
          client_id: CLICK_OAUTH_CLIENT_ID,
          client_secret: CLICK_OAUTH_CLIENT_SECRET,
          code,
        },
      });
    });
  }

  function login(params: ClickApiLoginParams) {
    return tryCatch(() => {
      return clickupWeb.post<ClickApiLoginResult, ClickApiLoginParams>(
        "/v1/login",
        {
          data: params,
        }
      );
    });
  }

  function refresh() {
    return tryCatch(() => {
      return clickupWeb.post<ClickApiRefreshResult>("/v1/refresh_token");
    });
  }

  function getTimesheets(
    params: ClickApiTimesheetOptions
  ): ClickApiResponse<ClickTimesheet[]> {
    return tryCatch(() => getTimesheetsInternal(params));
  }

  function updateTimesheets({
    teamId,
    items: updateItems,
  }: ClickUpdateTimesheetParams): ClickApiResponse<
    ClickUpdateTimesheetResult[]
  > {
    return tryCatch(async () => {
      if (!updateItems.length) {
        throw new ApiError("No items to update.");
      }

      const result: ClickNativeTimeEntryResult[] = await pipe(
        updateItems,
        A.map<ClickUpdateTimesheetItem, Task<ClickNativeTimeEntryResult>>(
          (item) => {
            switch (item.type) {
              case "add":
                return () =>
                  addNativeTimeEntry(teamId, {
                    start: item.start,
                    stop: item.end,
                    tid: item.taskId,
                    assignee: item.userId,
                    description: item.description,
                    billable: item.billable,
                  }).then((x) => x.data);

              case "update":
                return () =>
                  updateNativeTimeEntry(teamId, {
                    id: item.id,
                    start: item.start,
                    end: item.end,
                    tid: item.taskId,
                    description: item.description,
                    billable: item.billable,
                  }).then((x) => x.data[0]);

              case "delete":
                return () =>
                  deleteNativeTimeEntry(teamId, item.id).then((x) => x.data[0]);
            }
          }
        ),
        A.map((x) => tryCatch(x)),
        TE.sequenceArray,
        TE.map((x) => x.map((i) => ({ ...i }))),
        TE.getOrElse((x) => {
          throw new ApiError("Update items failed.", {
            errorData: x,
          });
        })
      )();

      return result;
    });
  }

  //   function processUpdateTimesheets(
  //     items: Task<Either<ClickApiResponseError, ClickNativeTimeEntryResult>[]>
  //   ): TaskEither<string, ClickNativeTimeEntryResult> {

  //   }

  type TimesheetDataGrouped = Record<
    number,
    Record<string, ClickTimesheetHours>
  >;

  function getTimesheetsHours(
    params: ClickApiTimesheetOptions
  ): ClickApiResponse<ClickTimesheetHours[]> {
    return tryCatch(async () => {
      const timesheets: ClickTimesheet[] = await getTimesheetsInternal(params);

      const grouped = timesheets.reduce<TimesheetDataGrouped>(
        (result, item) => {
          const startUtc = dateStartOfDayUtc(item.start);
          const key1 = Math.floor(startUtc.getTime() / 100000); // lze uriznout cast s casem
          const key2 = item.task?.id ?? "_UNKNOWN_";

          if (!result[key1]) {
            result[key1] = {};
          }

          if (!result[key1][key2]) {
            result[key1][key2] = {
              id: `${key1}_${key2}`,
              date: startUtc,
              duration: DurationTime.zero(),
              task:
                (item.task && {
                  ...item.task,
                }) ||
                null,
              timesheets: [],
            };
          }

          const current = result[key1][key2];

          current.duration = addDuration(current.duration, item.duration);
          current.timesheets.push(item.id);

          return result;
        },
        {}
      );

      return Object.keys(grouped).reduce<ClickTimesheetHours[]>(
        (result, key1) => {
          for (const key2 in grouped[key1]) {
            result.push(grouped[key1][key2]);
          }

          return result;
        },
        []
      );
    });
  }

  function getTimesheetsInternal({
    teamId,
    start,
    end,
    userId,
  }: ClickApiTimesheetOptions): Promise<ClickTimesheet[]> {
    return clickupApi
      .get<ClickNativeTimesheetCollection>(`/team/${teamId}/time_entries`, {
        start_date: start.getTime(),
        end_date: end.getTime(),
        assignee: userId,
      })
      .then((x) => clickTimesheetMapper(x.data));
  }

  function updateTask({
    taskId,
    data,
  }: ClickApiUpdateTaskParams): ClickApiResponse<boolean> {
    return tryCatch(async () => {
      await updateTaskNative(taskId, {
        name: data.name,
      });

      return true;
    });
  }

  function addNativeTimeEntry(
    teamId: number,
    item: ClickNativeNewTimeEntry
  ): Promise<ClickNativeNewTimeEntryResult> {
    return clickupApi.post(`/team/${teamId}/time_entries`, {
      data: item,
      dataCodec: ClickNativeNewTimeEntry,
      codec: ClickNativeNewTimeEntryResult,
    });
  }

  function updateNativeTimeEntry(
    teamId: number,
    item: ClickNativeUpdateTimeEntry
  ): Promise<ClickNativeUpdateTimeEntryResult> {
    return clickupApi.put<
      ClickNativeUpdateTimeEntryResult,
      ClickNativeUpdateTimeEntry
    >(`/team/${teamId}/time_entries/${item.id}`, {
      data: item,
      dataCodec: ClickNativeUpdateTimeEntry,
      codec: ClickNativeUpdateTimeEntryResult,
    });
  }

  function deleteNativeTimeEntry(
    teamId: number,
    id: string
  ): Promise<ClickNativeDeleteTimeEntryResult> {
    return clickupApi.delete<ClickNativeDeleteTimeEntryResult>(
      `/team/${teamId}/time_entries/${id}`,
      {
        codec: ClickNativeDeleteTimeEntryResult,
      }
    );
  }

  function searchTasks({
    teamId,
    query,
  }: ClickApiSearchTasksParams): ClickApiResponse<ClickTask[]> {
    return tryCatch(async () => {
      const spaces = await getSpacesCached(teamId);
      const lists = await Promise.all(spaces.map((x) => getLists(x.id))).then(
        (x) => x.flat()
      );
      const taskIds = await Promise.all(
        lists.map((x) =>
          searchNativeListTaskIds(x.id + "", query).catch((x) => [] as string[])
        )
      ).then((results) => results.flat());

      return getNativeTasks(taskIds).then((x) => x.map(clickTaskMapper));
    });
  }

  function getNativeTasks(taskIds: string[]): Promise<ClickNativeTask[]> {
    return clickupWeb
      .post<ClickNativeTaskCollection, any>("/v2/task", {
        data: {
          show_closed_subtasks: true,
          task_ids: taskIds,
        },
      })
      .then((x) => x.tasks);
  }

  function searchNativeListTaskIds(
    listId: string,
    query: string
  ): Promise<string[]> {
    return clickupWeb
      .post<ClickNativeGenericView, ClickNativeGenericViewData>(
        `/v1/genericView`,
        {
          data: {
            id: "list",
            name: "List",
            type: 1,
            parent: {
              id: listId,
              type: ClickNativeParentType.list,
            },
            filters: {
              search: query,
              op: "AND",
              search_custom_fields: false,
              search_description: true,
              search_name: true,
              show_closed: true,
            },
          },
        }
      )
      .then((x) => x.list.groups.map((x) => x.task_ids).flat());
  }

  function updateTaskExternalIds({
    taskId,
    externalIds,
  }: ClickApiUpdateExternalIdsParams): ClickApiResponse<ClickTask> {
    return tryCatch(async () => {
      const task = await getNativeTaskById(taskId);
      const parsed = parseTaskName(task.name);

      if (externalIds.RM !== undefined) {
        parsed.redmineId = Number(externalIds.RM) || undefined;
      }

      if (externalIds.IFS !== undefined) {
        parsed.ifsId = externalIds.IFS;
      }

      const updatedTask = await updateTaskNative(task.id, {
        name: createTaskName(parsed),
      });

      return clickTaskMapper(updatedTask);
    });
  }

  function updateTaskNative(
    id: string,
    data: Partial<ClickNativeTask>
  ): Promise<ClickNativeTask> {
    return clickupApi.put(`/task/${id}`, {
      data,
    });
  }

  function getNativeTaskRecord(taskIds: string[]): Promise<ClickTaskRecord> {
    return getNativeTasks(taskIds).then((tasks) =>
      tasks.reduce<ClickTaskRecord>((result, task) => {
        result[task.id] = clickTaskMapper(task);
        return result;
      }, {})
    );
  }

  async function clickTimesheetMapper(
    timesheets: ClickNativeTimesheet[]
  ): Promise<ClickTimesheet[]> {
    const taskIds = timesheets
      .filter((x) => x.task && x.task.id)
      .map((x) => x.task.id);
    const tasks = await getNativeTaskRecord(taskIds);

    return timesheets
      .filter((x) => x.end)
      .map<ClickTimesheet>((timesheet) => {
        const start = new Date(Number(timesheet.start));
        const end = new Date(Number(timesheet.end));
        const duration =
          (timesheet.duration && Number(timesheet.duration)) ||
          end.getTime() - start.getTime();

        return {
          ...timesheet,
          task: (timesheet.task && tasks[timesheet.task.id]) || null,
          start,
          end,
          duration: parseDurationFromMilis(duration),
          user: {
            ...timesheet.user,
            fullname: timesheet.user.username,
          },
        };
      });
  }

  function getTaskById(taskId: string): ClickApiResponse<ClickTask> {
    return tryCatch(() => getNativeTaskById(taskId).then(clickTaskMapper));
  }

  function getNativeTaskById(taskId: string): Promise<ClickNativeTask> {
    return clickupApi.get<ClickNativeTask>(`/task/${taskId}`);
  }

  function getTasksByCustomField({
    spaceId,
    field: fieldName,
    value,
  }: ClickApiTasksByCustomFieldParams): ClickApiResponse<ClickTask[]> {
    return tryCatch(async () => {
      const isFiltered = fieldName && fieldName.length && value && value.length;
      const lists = await getLists(spaceId);

      return promiseAllArrays(
        lists.map((list) => {
          let fieldsFilter: TaskCustomFieldFilter[] = [];
          if (isFiltered) {
            fieldsFilter = list.customFields
              .filter((field) => field.name === fieldName)
              .map((field) => ({
                field_id: field.id,
                operator: "=",
                value: value,
              }));

            if (!fieldsFilter.length) {
              return Promise.resolve([]);
            }
          }

          return getListTasks(list.id, {
            custom_fields: fieldsFilter,
          });
        })
      );
    });
  }

  function getCurrentUser(): ClickApiResponse<ClickUser> {
    return tryCatch(() =>
      clickupApi.get<ClickNativeCurrentUser>("/user").then((current) => ({
        id: current.user.id,
        fullname: current.user.username,
      }))
    );
  }

  function getTeams(): ClickApiResponse<ClickTeam[]> {
    return tryCatch(() =>
      clickupApi.get<ClickNativeTeamCollection>("/team").then((x) =>
        x.teams.map<ClickTeam>((team) => ({
          id: Number(team.id),
          name: team.name,
        }))
      )
    );
  }

  async function getLists(spaceId: number): Promise<ClickServerList[]> {
    if (!isValidId(spaceId)) return [];

    return clickupFromCache<ClickServerList[]>(
      `CLICK_LIST_ALL_SPACE_${spaceId}`,
      async () => {
        const folders: ClickNativeFolder[] = await getFolders(spaceId);
        const lists = await promiseAllArrays<ClickNativeList>([
          getFolderLessLists(spaceId),
          ...folders.map((folder) => getFolderLists(Number(folder.id))),
        ]);

        return Promise.all<ClickServerList>(
          lists.map((list, index) => {
            const listId = Number(list.id);

            return getCustomFields(listId).then((fields) => {
              return {
                id: listId,
                name: list.name,
                customFields: fields,
              };
            });
          })
        );
      }
    );
  }

  function getCustomFields(listId: number): Promise<ClickNativeCustomField[]> {
    return clickupApi
      .get<ClickNativeCustomFieldCollection>(`/list/${listId}/field`)
      .then((col) => col.fields);
  }

  async function getFolderLists(folderId: number): Promise<ClickNativeList[]> {
    if (!isValidId(folderId)) return [];

    return clickupApi
      .get<ClickNativeListCollection>(`/folder/${folderId}/list`)
      .then((x) => x.lists);
  }

  async function getFolderLessLists(
    spaceId: number
  ): Promise<ClickNativeList[]> {
    if (!isValidId(spaceId)) return [];

    return clickupApi
      .get<ClickNativeListCollection>(`/space/${spaceId}/list`)
      .then((x) => x.lists);
  }

  async function getFolders(spaceId: number) {
    if (!isValidId(spaceId)) {
      return [];
    }

    return clickupApi
      .get<ClickNativeFolderCollection>(`/space/${spaceId}/folder`)
      .then((x) => x.folders);
  }

  async function getListTasks(
    listId: number,
    { custom_fields, subtasks }: ListTaskParams
  ): Promise<ClickTask[]> {
    if (!isValidId(listId)) return [];
    return clickupApi
      .get<ClickNativeTaskCollection>(`/list/${listId}/task`, {
        subtasks: subtasks === undefined ? true : subtasks,
        custom_fields: JSON.stringify(custom_fields || []),
      })
      .then((x) => x.tasks.map(clickTaskMapper));
  }

  async function getTeamTasks(
    teamId: number,
    { list_ids, space_ids, subtasks }: TeamTaskParams
  ): Promise<ClickTask[]> {
    if (!isValidId(teamId)) return [];
    return clickupApi
      .get<ClickNativeTaskCollection>(`/team/${teamId}/task`, {
        list_ids: list_ids || [],
        space_ids: space_ids || [],
        subtasks: subtasks === undefined ? true : subtasks,
      })
      .then((x) => x.tasks.map(clickTaskMapper));
  }

  async function getSpacesCached(teamId: number): Promise<ClickSpace[]> {
    if (!isValidId(teamId)) return [];

    return clickupFromCache("CLICK_TEAM_SPACE_ALL_" + teamId, () =>
      getSpaces(teamId)
    );
  }

  async function getSpaces(teamId: number): Promise<ClickSpace[]> {
    if (!isValidId(teamId)) return [];

    return clickupApi
      .get<ClickNativeSpaceCollection>(`/team/${teamId}/space`)
      .then((x) =>
        x.spaces.map<ClickSpace>((space) => ({
          id: Number(space.id),
          name: space.name,
        }))
      );
  }

  //   /**
  //    * @deprecated Neni vyreseno cachovani podle aktualniho uzivatele!!
  //    */
  //   function clickupFromApiCache<T>(
  //     key: string,
  //     url: string,
  //     params?: ApiParams
  //   ): Promise<T> {
  //     const getValue = () => {
  //       return clickupApi.get<T>(url, params);
  //     };

  //     return clickupFromCache(key, getValue);
  //   }

  function getTimesheetsByHours({
    teamId,
    userId,
    start,
    end,
  }: ClickApiTimesheetOptions): ClickApiResponse<ClickTimesheet[]> {
    return tryCatch(async () => {
      return [];
      const response = await clickupApi.get<ClickNativeTimesheetCollection>(
        `/team/${teamId}/time_entries`,
        {
          // start_date: startOfDay(start).getTime(),
          // end_date: endOfDay(end).getTime(),
          assignee: userId,
        }
      );

      const timesheets = response.data.map(clickOutTimesheetMapper);

      const sumHours = {} as Record<
        string,
        Record<string, TimeEntryWithoutDuration>
      >;

      for (const ts of timesheets) {
        const key1 = formatDateSql(ts.start);
        const key2 = ts.task.id;
        if (!sumHours[key1]) {
          sumHours[key1] = {};
        }

        if (!sumHours[key1][key2]) {
          sumHours[key1][key2] = {
            id: ts.id,
            start: ts.start,
            end: ts.end,
            task: ts.task,
            user: ts.user,
            seconds: 0,
          };
        }

        sumHours[key1][key2].seconds += Math.floor(ts.durationMilis / 1000);
      }

      return [];
      // return Object.keys(sumHours).reduce<ClickTimesheet[]>((result, key1) => {
      //   for (const key2 in sumHours[key1]) {
      //     const ts = sumHours[key1][key2];
      //     result.push({
      //       id: ts.id,
      //       start: ts.start,
      //       end: ts.end,
      //       //   project: ts.task.
      //       task: {
      //         id: ts.task.id,
      //         name: ts.task.name,
      //       },
      //       duration: parseDurationFromSeconds(ts.seconds),
      //     });
      //   }

      //   return result;
      // }, []);
    });
  }
}

type ClickTaskPart = Pick<ClickTask, "name" | "ifsId" | "redmineId">;

function createTaskName({ redmineId, ifsId, name }: ClickTaskPart): string {
  let parsedName = name;

  if (redmineId) {
    parsedName = replaceRedmineIdFromTaskName(parsedName, redmineId);
  }

  if (ifsId) {
    parsedName += ` IFS${ifsId}`;
  }

  return parsedName;
}

function replaceRedmineIdFromTaskName(
  taskName: string,
  redmineId: number
): string {
  const match = taskName.match(/^((\s*#\d+\s+\-\s+)+)/);

  if (match) {
    taskName = taskName.replace(match[0], "");
  }

  return `#${redmineId} - ${taskName}`;
}

function parseTaskName(taskName: string): ClickTaskPart {
  const ifsId = getTaskExternalIdByTaskName(taskName, "IFS");
  const redmineId: number | undefined = getPositiveNumber(
    getTaskExternalIdByTaskName(taskName, "RM"),
    undefined
  );

  if (ifsId) {
    taskName = taskName.replace(`IFS${ifsId}`, "").trim();
  }

  if (redmineId) {
    taskName = replaceRedmineIdFromTaskName(taskName, redmineId);
  }

  return {
    name: taskName,
    ifsId,
    redmineId,
  };
}

export function getTaskExternalIdByTaskName(
  taskName: string,
  externalName: ClickExternalName
): string | undefined {
  let match: RegExpMatchArray | RegExpMatchArray[] | null;
  switch (externalName) {
    case "IFS":
      match = [...taskName.matchAll(/IFS(([\da-z\.\-])+(_([\da-z])+)?)/gi)];
      return (match.length && match[match.length - 1][1]) || undefined;
    case "RM":
      match = taskName.match(/#(\d+)/);
      return (match && match.length && match[1]) || undefined;
    default:
      return undefined;
  }
}

function getExternalName(name: string): ClickExternalName | undefined {
  switch (name) {
    case ClickupConfig.CLICK_IFS_ID_FIELD_NAME:
      return "IFS";
    case ClickupConfig.CLICK_RM_ID_FIELD_NAME:
      return "RM";
    default:
      return undefined;
  }
}

function tryCatch<T>(resolve: () => Promise<T>) {
  return TE.tryCatch(resolve, onRejected);
}

function onRejected(reason: unknown): ClickApiResponseError {
  return pipe(
    handleApiServerError<ClickNativeError>(reason),
    E.fold(
      // left
      (error) => {
        return {
          ...error,
          code: "Click",
        };
      },
      // right
      (result) => ({
        code: "Click",
        statusCode: result.status,
        message: "Click API error",
        clickError: result.data,
      })
    )
  );
}

// async function getCustomFieldIdByList(
//   spaceId: number,
//   listId: number,
//   name: string
// ): Promise<string | null> {
//   const listFields = await getCustomFields(spaceId);

//   if (!listFields[listId]) {
//     return null;
//   }

//   return listFields[listId].find((list) => list.name === name).id;
// }

// async function getCustomFields(
//   spaceId: number
// ): Promise<ClickNativeCustomListFields[]> {
//   return clickupFromCache(`CLICK_CUSTOM_FIELD_SPACE_${spaceId}`, async () => {
//     const lists = await getLists(spaceId);

//   });
// }

interface TaskCustomFieldFilter {
  field_id: string;
  value: any;
  operator:
    | "="
    | "<"
    | "<="
    | ">"
    | ">="
    | "!="
    | "IS NULL"
    | "IS NOT NULL"
    | "RANGE"
    | "ANY"
    | "ALL"
    | "NOT ANY"
    | "NOT ALL";
}
interface ListTaskParams {
  subtasks?: boolean;
  custom_fields?: TaskCustomFieldFilter[];
}
interface TeamTaskParams extends Omit<ListTaskParams, "custom_fields"> {
  list_ids: number[];
  space_ids?: number[];
}

function clickupFromCache<T>(key: string, value: CacheGetValue<T>) {
  return fromCacheHour({
    key,
    getValue: value,
  });
}

function isValidId(value: number) {
  return !isNaN(value) && value > 0;
}

function promiseAllArrays<T>(promises: Promise<T[]>[]): Promise<T[]> {
  return Promise.all(promises).then((arr) => arr[0].concat(...arr.splice(1)));
}

function getValidIds(arr: Array<string | number>) {
  return arr.map((x) => Number(x)).filter((x) => isValidId(x));
}

function clickTaskMapper(task: ClickNativeTask): ClickTask {
  const parsed = parseTaskName(task.name);
  return {
    id: task.id,
    name: parsed.name,
    // customFields: task.custom_fields
    //   .filter((x) => x.value !== undefined)
    //   .map((field) => {
    //     return {
    //       name: field.name,
    //       value: field.value,
    //     };
    //   }),
    redmineId: parsed.redmineId,
    ifsId: parsed.ifsId,
  };
}

function findCustomFields(
  names: string[],
  fieldColleciton: ClickNativeTaskCustomField[]
): Record<string, string> {
  const result: Record<string, string> = {};

  for (const field of fieldColleciton) {
    if (names.indexOf(field.name) !== -1) {
      result[field.name] = field.value;
    }
  }

  return result;
}

type ClickTaskRecord = Record<string, ClickTask>;
