import { IfsConfig } from "../../../src/config";
import {
  IfsApiResponse,
  IfsApiResponseError,
  IfsApi,
  IfsTimesheet,
  IfsTimesheetParams,
  TE,
  IfsTimesheetHours,
  IfsSearchParams,
  IfsProject,
  IfsProjectReport,
  IfsWorkOrder,
  IfsWorkOrderOperationParams,
  IfsWorkOrderOperation,
  IfsProjectSearchParams,
  IfsProjectReportParams,
  IfsAttendanceHoursParams,
  IfsAttendanceHours,
  IfsEmployeeDayInfoParams,
  IfsEmployeeDayInfo,
  IfsUpdateTimesheetHoursParams,
  IfsApiNativeResponseError,
  IfsUpdateTimesheetHoursItem,
  Task,
  ApiRequest,
  IfsCurrentUserInfo,
  IfsApiNativeUser,
} from "../../lib/types";
import {
  addDuration,
  C,
  pipe,
  dateCompare,
  DateFromISOString,
  DateFromSqlString,
  dateIsSame,
  dateTimeCompare,
  durationCompare,
  durationSafeTotalHours,
  DurationTime,
  durationTotalHoursRounded,
  escapeString,
  formatDateSql,
  iterMap,
  iterSome,
  parseDurationFromHours,
  t,
  trimToFirstChar,
  parseCookie,
  compareTimesheets,
  getUpdateSelection,
  filterMapTimesheets,
} from "../../lib/utils";
import { Router } from "../router";
import { ApiMethod } from "../../lib/types";
import {
  createODataApi,
  createODataApiWithRequest,
  ODataApi,
  ODataApiWithRequest,
} from "./odataApiServer";
import { ApiError } from "../../lib/Errors";
import {
  ApiServerIncomingRequest,
  ApiServerResponseError,
  ClientRequest,
  handleApiServerError,
} from "./apiServer";
import { E, T } from "../../lib/utils/fptsUtil";

export interface IfsNativeHoursC
  extends t.Type<DurationTime, string | number, unknown> {}

export const IfsNativeHours: IfsNativeHoursC = new t.Type<
  DurationTime,
  string,
  unknown
>(
  "IfsNativeHours",
  (u): u is DurationTime => u instanceof DurationTime,
  (u, c) =>
    E.either.chain(t.union([t.string, t.number]).validate(u, c), (s) => {
      try {
        const d = durationSafeTotalHours(Number(s) || 0);
        return t.success(d);
      } catch {
        return t.failure(u, c);
      }
    }),
  (a) => (durationTotalHoursRounded(a) || 0) + ""
);
interface IfsNativeCollection<T> {
  value: T[];
}

function IfsNativeCollection<T extends C>(codec: T) {
  return t.type({
    value: t.array(codec),
  });
}

type IfsNativeModule = t.TypeOf<typeof IfsNativeModule>;
const IfsNativeModule = t.keyof({
  PRJREP: null,
  WO: null,
});

interface IfsNativeLovResource {
  luname: string;
  keyref: string;
  ResourceId: string;
  Description: string;
  Company: string;
}

const IfsNativeCompanyPerson = t.type({
  CompanyId: t.string,
  EmpNo: t.string,
  EmployeeName: t.string,
});

export type IfsNativeBaseTimesheet = t.TypeOf<typeof IfsNativeBaseTimesheet>;

const IfsNativeBaseTimesheet = t.type({
  // PK
  keyref: t.string,

  Objid: t.string,
  Objversion: t.string,

  CompanyId: t.string,
  EmpNo: t.string,
  //   EmployeeName: t.string,
  AccountDate: DateFromSqlString,
  Hours: IfsNativeHours,

  Col1: t.string,
  Col2: t.string,
  //   Col4: t.union([t.string, t.undefined]),
  Label: t.string,

  // refs
  CompanySiteRef: t.any,
  CompanyPersonRef: IfsNativeCompanyPerson,
});

export type IfsNativeProjectTimesheet = t.TypeOf<
  typeof IfsNativeProjectTimesheet
>;

const IfsNativeProjectTimesheet = t.intersection([
  IfsNativeBaseTimesheet,
  t.type({
    Module: t.literal("PRJREP"),
    // PRJREP
    Description: t.string,
    Activity: t.string,
    ResourceName: t.string,
  }),
]);

export type IfsNativeWorkOrderTimesheet = t.TypeOf<
  typeof IfsNativeWorkOrderTimesheet
>;

const IfsNativeWorkOrderTimesheet = t.intersection([
  IfsNativeBaseTimesheet,
  t.type({
    Module: t.literal("WO"),
    // WO
    WorkOrder: t.string,
    WorkTask: t.string,
  }),
]);

export type IfsNativeTimesheetCombined = Omit<
  IfsNativeProjectTimesheet,
  "Module"
> &
  Omit<IfsNativeWorkOrderTimesheet, "Module"> & {
    Module: IfsNativeModule;
  };

export type IfsNativeTimesheet = t.TypeOf<typeof IfsNativeTimesheet>;

export const IfsNativeTimesheet = t.union([
  IfsNativeProjectTimesheet,
  IfsNativeWorkOrderTimesheet,
]);

type IfsNativeTimesheetCollection = t.TypeOf<
  typeof IfsNativeTimesheetCollection
>;

export const IfsNativeTimesheetCollection =
  IfsNativeCollection(IfsNativeTimesheet);

type IfsNativeAddProjectTimesheet = t.TypeOf<
  typeof IfsNativeAddProjectTimesheet
>;

export type IfsNativeDeleteTimesheet = t.TypeOf<
  typeof IfsNativeDeleteTimesheet
>;

const IfsNativeDeleteTimesheet = t.type({
  Module: IfsNativeModule,
  CompanyId: t.string,
  EmpNo: t.string,
  AccountDate: DateFromSqlString,
  Objid: t.string,
  Objversion: t.string,
});

const IfsNativeTimesheetBase = t.type({
  CompanyId: t.string,
  EmpNo: t.string,
  AccountDateDate: DateFromSqlString,
  JobTransactionParams: t.type({}),
  DayHours: IfsNativeHours,
});

const IfsNativeAddProjectTimesheet = t.intersection([
  IfsNativeTimesheetBase,
  t.type({
    AttPrjrep: t.literal(true),
    ShortName: t.string,
    ReportCostCode: t.string, // FIXME: asi se vyplnuje automaticky pokud jich neni vice
  }),
]);

type IfsNativeAddWorkOrderTimesheet = t.TypeOf<
  typeof IfsNativeAddWorkOrderTimesheet
>;

const IfsNativeAddWorkOrderTimesheet = t.intersection([
  IfsNativeTimesheetBase,
  t.type({
    AttWo: t.literal(true),
    MaintOrg: t.string,
    WorkOrder: t.string,
    WorkTask: t.string,
  }),
]);

export type IfsNativeAddTimesheet = t.TypeOf<typeof IfsNativeAddTimesheet>;
const IfsNativeAddTimesheet = t.union([
  IfsNativeAddProjectTimesheet,
  IfsNativeAddWorkOrderTimesheet,
]);

type IfsNativeUpdateWorkOrderTimesheet = t.TypeOf<
  typeof IfsNativeUpdateWorkOrderTimesheet
>;

const IfsNativeUpdateWorkOrderTimesheet = t.intersection([
  IfsNativeTimesheetBase,
  t.type({
    EntryType: t.literal("WO"),
    MaintOrg: t.string,
    WorkOrder: t.string,
    WorkTask: t.string,
    Objid: t.string,
    Objversion: t.string,
  }),
]);

type IfsNativeUpdateProjectTimesheet = t.TypeOf<
  typeof IfsNativeUpdateProjectTimesheet
>;

const IfsNativeUpdateProjectTimesheet = t.intersection([
  IfsNativeTimesheetBase,
  t.type({
    EntryType: t.literal("PRJREP"),
    ShortName: t.string,
    ResourceId: t.string,
    Objid: t.string,
    Objversion: t.string,
  }),
]);

export type IfsNativeUpdateTimesheet = t.TypeOf<
  typeof IfsNativeUpdateTimesheet
>;
const IfsNativeUpdateTimesheet = t.union([
  IfsNativeUpdateWorkOrderTimesheet,
  IfsNativeUpdateProjectTimesheet,
]);

const ifsApiWithRequest = createODataApiWithRequest({
  baseServerUrl: IfsConfig.IFS_URL,
  headers: {
    "Content-Type": "application/json;IEEE754Compatible=true",
  },
  allowedReqHeaders: ["Cookie", "Host", "X-XSRF-TOKEN"],
});

export function useNativeIFS(router: Router) {
  router.addRoute({
    method: "ALL",
    inRoute: "/ifs_native/*",
    result: (req) =>
      tryCatch(() => {
        const path = req.url.replace("/ifs_native/", "");
        return ifsApiWithRequest.all({
          request: req,
          path,
          method: <ApiMethod>req.method,
          data: req.body,
        });
      }),
  });
}

export function useIFS(router: Router) {
  // timesheets
  //   router.addRoute<TmpTimesheet[], IfsNativeTimesheetParams>({
  //     method: "GET",
  //     inRoute: "/ifs/timesheets",
  //     paramNames: {
  //       companyId: "string",
  //       empNo: "number",
  //       start: "date",
  //       end: "date",
  //     },
  //     result: (req, { companyId, empNo: empNo, start, end }) =>
  //       getTimesheetsInInterval(companyId, empNo, start, end),
  //   });

  //   router.addRoute<IfsApiResponseError, any, any>({
  //     inRoute: "/ifs/test",
  //     method: "GET",
  //     result: (req) =>
  //       tryCatch(() => {
  //         return api(req).getNativeResources(IfsConfig.IFS_COMPANY_ID, [
  //           "Odbornost Redat Programátor - WEB",
  //           "Odbornost Redat Programátor - JAVA",
  //         ]);
  //       }),
  //   });

  router.addRoute<IfsApiResponseError, IfsCurrentUserInfo>({
    method: "GET",
    inRoute: "/ifs/user/current",
    result: (req) => api(req).getCurrentUser(),
  });

  // timesheets hours
  router.addRoute<IfsApiResponseError, IfsTimesheetHours[], IfsTimesheetParams>(
    {
      method: "GET",
      inRoute: "/ifs/timesheet/hours",
      codec: t.array(IfsTimesheetHours),
      paramNames: {
        companyId: "string",
        empNo: "number",
        start: "date",
        end: "date",
        top: "number",
        skip: "number",
      },
      result: (req, params) => api(req).getTimesheetsByHours(params),
    }
  );

  router.addRoute<
    IfsApiResponseError,
    IfsTimesheetHours[],
    IfsUpdateTimesheetHoursParams
  >({
    method: "PUT",
    inRoute: "/ifs/timesheet/hours",
    codec: t.array(IfsTimesheetHours),
    paramNames: {
      companyId: "string",
      empNo: "number",
      items: t.array(IfsUpdateTimesheetHoursItem),
    },
    result: (req, params) => api(req).updateTimesheetsHours(params),
  });

  // attendance hours
  router.addRoute<
    IfsApiResponseError,
    IfsAttendanceHours[],
    IfsAttendanceHoursParams
  >({
    method: "GET",
    inRoute: "/ifs/attendance/hours",
    codec: t.array(IfsAttendanceHours),
    paramNames: {
      companyId: "string",
      empNo: "number",
      start: "date",
      end: "date",
      top: "number",
      skip: "number",
    },
    result: (req, params) => api(req).getAttendanceHours(params),
  });

  // employee day info
  router.addRoute<
    IfsApiResponseError,
    IfsEmployeeDayInfo[],
    IfsEmployeeDayInfoParams
  >({
    method: "GET",
    inRoute: "/ifs/employee/day",
    codec: t.array(IfsEmployeeDayInfo),
    paramNames: {
      companyId: "string",
      empNo: "number",
      start: "date",
      end: "date",
      top: "number",
      skip: "number",
    },
    result: (req, params) => api(req).getEmployeeDayInfo(params),
  });

  // search project
  router.addRoute<IfsApiResponseError, IfsProject[], IfsProjectSearchParams>({
    method: "GET",
    inRoute: "/ifs/project/search",
    paramNames: {
      companyId: "string",
      date: "date",
      query: "string",
      top: "number",
      skip: "number",
    },
    result: (req, params) => api(req).searchProject(params),
  });

  // search work orders
  router.addRoute<IfsApiResponseError, IfsWorkOrder[], IfsSearchParams>({
    method: "GET",
    inRoute: "/ifs/wo/search",
    paramNames: {
      companyId: "string",
      query: "string",
      skip: "number",
      top: "number",
    },
    result: (req, params) => api(req).searchWorkOrder(params),
  });

  // get project reports
  router.addRoute<
    IfsApiResponseError,
    IfsProjectReport[],
    IfsProjectReportParams
  >({
    method: "GET",
    inRoute: "/ifs/project/report",
    paramNames: {
      companyId: "string",
      date: "date",
      projectId: "string",
      skip: "number",
      top: "number",
    },
    result: (req, params) => api(req).getProjectReports(params),
  });

  // get work order operations
  router.addRoute<
    IfsApiResponseError,
    IfsWorkOrderOperation[],
    IfsWorkOrderOperationParams
  >({
    method: "GET",
    inRoute: "/ifs/wo/operation",
    paramNames: {
      companyId: "string",
      workOrderId: "number",
      skip: "number",
      top: "number",
    },
    result: (req, params) => api(req).getWorkOrderOperations(params),
  });
}

function api({ headers }: ApiServerIncomingRequest): IfsApi {
  const request = {
    headers: {
      "X-XSRF-TOKEN": IfsConfig.IFS_XSRF_TOKEN,
      Cookie: parseCookie({
        IFSSESSIONID48080: headers["authorization"],
        "XSRF-TOKEN": IfsConfig.IFS_XSRF_TOKEN,
      }),
    },
  };
  const ifsApi = createODataApi(request, ifsApiWithRequest);

  return {
    getCurrentUser,
    getTimesheetsByHours,
    getAttendanceHours,
    getEmployeeDayInfo,

    updateTimesheetsHours,

    searchProject,
    searchWorkOrder,

    getProjectReports,
    getWorkOrderOperations,
  };

  function getCurrentUser(): IfsApiResponse<IfsCurrentUserInfo> {
    return tryCatch(() =>
      ifsApi
        .get<IfsNativeCollection<IfsApiNativeUser>, IfsApiNativeUser>({
          path: "/TimeRegistrationManagerHandling.svc/Employees",
          select: ["CompanyId", "EmpNo", "EmployeeName", "PersonId"],
          orderBy: "DefaultCompany desc",
        })
        .then((col) => {
          const user = col.value[0];

          return {
            companyId: user.CompanyId,
            empNo: Number(user.EmpNo),
            fullname: user.EmployeeName,
            personId: Number(user.PersonId),
          };
        })
    );
  }

  function searchProject({
    companyId,
    date,
    query,
    top,
    skip,
  }: IfsProjectSearchParams): IfsApiResponse<IfsProject[]> {
    query = escapeString(query);
    return tryCatch(async () => {
      const dateStr = formatDateSql(date);
      const projects = await ifsApi.get<
        IfsNativeCollection<IfsNativeProject>,
        IfsNativeProject[]
      >({
        path: `/TimeRegistrationManagerHandling.svc/GetValidEmpPrjrepAct(CompanyId='${companyId}',AccountDate=${dateStr})`,
        filter: [
          {
            Company: companyId,
          },
          {
            or: [
              `startswith(ShortName, '${query}')`,
              `contains(tolower(Project), '${query}')`,
              `contains(tolower(SubProject), '${query}')`,
              `contains(tolower(Activity), '${query}')`,
              `contains(tolower(Description), '${query}')`,
            ],
          },
        ],
        top,
        skip,
      });

      return projects.value.map<IfsProject>((itm) => ({
        id: itm.ShortName,
        name: itm.Description,
        type: "Project",
      }));
    });
  }

  function getProjectReports({
    companyId,
    date,
    projectId,
    skip,
    top,
  }: IfsProjectReportParams): IfsApiResponse<IfsProjectReport[]> {
    // /TimeRegistrationManagerHandling.svc/GetValidActReportCode(AccountDate=2021-11-04,CompanyId='RETIA',ShortName='RP0003.07.07-11')?$filter=(CompanyId%20eq%20%27RETIA%27)&$skip=0&$top=101
    const dateStr = formatDateSql(date);
    return tryCatch(async () => {
      const reports = await ifsApi.get<
        IfsNativeCollection<IfsNativeProjectReport>,
        IfsNativeProjectReport
      >({
        path: `/TimeRegistrationManagerHandling.svc/GetValidActReportCode(AccountDate=${dateStr},CompanyId='${companyId}',ShortName='${projectId}')`,
        filter: { CompanyId: companyId },
        top,
        skip,
      });

      return reports.value.map<IfsProjectReport>((itm) => ({
        id: `${itm.ShortName}_${itm.ReportCostCode}`,
        type: "Project",
        name: itm.ReportCode,
        projectId: itm.ShortName,
      }));
    });
  }

  function searchWorkOrder({
    companyId,
    query,
  }: IfsSearchParams): IfsApiResponse<IfsWorkOrder[]> {
    query = escapeString(query);
    return tryCatch(async () => {
      const works = await ifsApi.get<
        IfsNativeCollection<IfsNativeWorkOrder>,
        IfsNativeWorkOrder
      >({
        path: `/TimeRegistrationManagerHandling.svc/Reference_LovActiveWorkOrder2`,
        filter: [
          { Company: companyId },
          {
            or: [
              `startswith(cast(WoNo, Edm.String), '${query}')`,
              `contains(tolower(ErrDescr), '${query}')`,
            ],
          },
        ],
      });

      return works.value.map<IfsWorkOrder>((itm) => ({
        id: itm.WoNo,
        name: itm.ErrDescr,
        type: "WorkOrder",
      }));
    });
  }

  function getWorkOrderOperations({
    companyId,
    workOrderId,
    skip,
    top,
  }: IfsWorkOrderOperationParams) {
    return tryCatch(async () => {
      const operations = await ifsApi.get<
        IfsNativeCollection<IfsNativeWorkOrderOperation>,
        IfsNativeWorkOrderOperation
      >({
        path: `/TimeRegistrationManagerHandling.svc/Reference_JtTask`,
        filter: [{ Company: companyId }, { WoNo: workOrderId }],
        top,
        skip,
      });

      return operations.value.map<IfsWorkOrderOperation>((itm) => ({
        id: `${itm.WoNo}_${itm.TaskSeq}`,
        type: "WorkOrder",
        name: itm.Description,
        operationId: itm.TaskSeq,
        workOrderId: itm.WoNo,
      }));
    });
  }

  function getTimesheetsByHours({
    companyId,
    empNo,
    start,
    end,
  }: IfsTimesheetParams): IfsApiResponse<IfsTimesheetHours[]> {
    return tryCatch(() =>
      getTimesheetsInInterval({
        companyId,
        empNo,
        begin: start,
        end,
      })
    );
  }

  function getAttendanceHours({
    companyId,
    empNo,
    start,
    end,
  }: IfsAttendanceHoursParams): IfsApiResponse<IfsAttendanceHours[]> {
    const startStr = formatDateSql(start);
    const endStr = formatDateSql(end);

    return tryCatch(async () => {
      const items = await ifsApi.get<
        IfsNativeCollection<IfsNativeAttendance>,
        IfsNativeAttendance
      >({
        path: `/TimeRegistrationManagerHandling.svc/Employees(CompanyId='${companyId}',EmpNo='${empNo}')/AttendanceItemSet`,
        //   expand: {
        //     EmpDayInfoRef: {
        //       select: ["IsDayAuthorized"],
        //     },
        //     WageCodeRef: {
        //       select: ["WageName"],
        //     },
        //     CompanyOrgRef: {
        //       select: ["OrgName"],
        //     },
        //     AbsenceTypeRef: {
        //       select: ["AbsenceTypeName", "Objgrants"],
        //     },
        //   },
        filter: {
          AccountDate: {
            ge: {
              type: "raw",
              value: startStr,
            },
            le: {
              type: "raw",
              value: endStr,
            },
          },
        },
      });

      return items.value.map<IfsAttendanceHours>((itm) => ({
        date: new Date(itm.AccountDate),
        hours: parseDurationFromHours(itm.Hours),
        userId: itm.EmpNo,
      }));
    });
  }

  function getEmployeeDayInfo({
    companyId,
    empNo,
    start,
    end,
  }: IfsEmployeeDayInfoParams): IfsApiResponse<IfsEmployeeDayInfo[]> {
    const startStr = formatDateSql(start);
    const endStr = formatDateSql(end);
    return tryCatch(async () => {
      const items = await ifsApi.get<
        IfsNativeCollection<IfsNativeEmployeeDayInfo>,
        IfsNativeEmployeeDayInfo
      >({
        path: `/TimeRegistrationManagerHandling.svc/Employees(CompanyId='${companyId}',EmpNo='${empNo}')/EmpDayInfoSet`,
        //   codec: t.array(IfsNativeEmployeeDayInfo),
        filter: {
          AccountDate: {
            ge: {
              type: "raw",
              value: startStr,
            },
            le: {
              type: "raw",
              value: endStr,
            },
          },
        },
      });

      return items.value.map<IfsEmployeeDayInfo>((itm) => {
        return {
          date: new Date(itm.AccountDate),
          userId: itm.EmpNo,
          status: itm.DayStatus === "1" ? "FINISHED" : "INWORK",
        };
      });
    });
  }

  function addNativeTimesheet(
    item: IfsNativeAddTimesheet
  ): Promise<AddNativeTimesheetResponse> {
    return ifsApi.post({
      path: "/TimeRegistrationManagerHandling.svc/ReportTimeWithIntervals",
      data: item,
      dataCodec: IfsNativeAddTimesheet,
      codec: AddNativeTimesheetResponse,
    });
  }

  function updateNativeTimesheet(
    item: IfsNativeUpdateTimesheet
  ): Promise<UpdateNativeTimesheetResponse> {
    return ifsApi.post({
      path: "/TimeRegistrationManagerHandling.svc/UpdateTime",
      data: item,
      dataCodec: IfsNativeUpdateTimesheet,
      codec: UpdateNativeTimesheetResponse,
    });
  }

  function deleteNativeTimesheet(
    item: IfsNativeDeleteTimesheet
  ): Promise<UpdateNativeTimesheetResponse> {
    return ifsApi.post({
      path: "/TimeRegistrationManagerHandling.svc/DeleteReportItem",
      data: item,
      dataCodec: IfsNativeDeleteTimesheet,
      codec: UpdateNativeTimesheetResponse,
    });
  }

  function processUpdateSelection({
    toUpdate,
    toDelete,
    toAdd,
  }: IfsUpdateSelection): Promise<boolean> {
    const tasks: Task<boolean>[] = [];
    const createTask = <T, R>(method: (i: T) => Promise<R>) => {
      return (itm: T): Task<boolean> =>
        () =>
          method(itm)
            .then((x) => {
              return true;
            })
            .catch((x) => {
              return false;
            });
    };

    tasks.push(...toAdd.map(createTask(addNativeTimesheet)));
    tasks.push(...toUpdate.map(createTask(updateNativeTimesheet)));
    tasks.push(...toDelete.map(createTask(deleteNativeTimesheet)));

    return Promise.all(tasks.map((x) => x())).then((results) => {
      return !results.some((x) => !x);
    });
  }

  function updateTimesheetsHours({
    companyId,
    empNo,
    items,
  }: IfsUpdateTimesheetHoursParams): IfsApiResponse<IfsTimesheetHours[]> {
    return tryCatch(async () => {
      let begin: Date | undefined, end: Date | undefined;
      const taskIds: Set<string> = new Set();
      const resourceNames: Set<string> = new Set();

      for (const itm of items) {
        if (!begin || dateTimeCompare(itm.date, begin) < 0) {
          begin = itm.date;
        }

        if (!end || dateTimeCompare(itm.date, end) > 0) {
          end = itm.date;
        }

        taskIds.add(itm.taskId);
      }

      const timesheets = filterMapTimesheets(
        await getNativeTimesheets({
          companyId,
          empNo,
          begin,
          end,
        }),
        (itm) => taskIds.has(getTimesheetTaskId(itm)),
        (itm) => {
          if (itm.Module === "PRJREP") {
            resourceNames.add(itm.ResourceName);
          }
          return itm;
        }
      );

      const resourceIdsByName = (
        await getNativeResources(companyId, resourceNames.values())
      ).reduce((map, rs) => {
        map[rs.Description] = rs.ResourceId;
        return map;
      }, {});

      const updateSel = getIfsUpdateSelection({
        companyId,
        empNo,
        serverItems: timesheets,
        updateItems: items,
        resourceIdsByName,
      });

      if (!(await processUpdateSelection(updateSel))) {
        throw new ApiError("Update IFS timesheet hours failed.");
      }

      const newTimesheetHours = filterMapTimesheets(
        await getNativeTimesheets({
          companyId,
          empNo,
          begin,
          end,
        }),
        (itm) => taskIds.has(getTimesheetTaskId(itm)),
        (itm) => ifsTimesheetMapper(itm)
      );

      return compareIfsTimesheets(newTimesheetHours, items);
    });
  }

  async function getNativeTimesheets({
    companyId,
    empNo,
    begin,
    end,
  }: NativeTimesheetsParams): Promise<IfsNativeTimesheet[]> {
    const startStr = formatDateSql(begin);
    const endStr = formatDateSql(end);

    return ifsApi
      .get<IfsNativeTimesheetCollection, IfsNativeTimesheetCombined>({
        codec: IfsNativeTimesheetCollection,
        path: `/TimeRegistrationManagerHandling.svc/Employees(CompanyId='${companyId}',EmpNo='${empNo}')/ReportItemSet`,

        select: [
          "keyref",
          "Objid",
          "Objversion",

          "CompanyId",
          "EmpNo",
          "AccountDate",
          "Hours",
          "Col1",
          "Col2",
          "Label",

          // PRJ
          "Description",
          "Activity",
          "ResourceName",

          // WO
          "WorkOrder",
          "WorkTask",
        ],

        expand: {
          CompanySiteRef: {
            select: ["Site"],
          },
          CompanyPersonRef: {
            select: ["EmployeeName"],
          },
        },

        filter: {
          AccountDate: {
            ge: {
              type: "raw",
              value: startStr,
            },
            le: {
              type: "raw",
              value: endStr,
            },
          },
        },
      })
      .then((x) => x.value);
  }

  function getNativeResources(
    companyId: string,
    descriptions: Iterable<string>
  ): Promise<IfsNativeLovResource[]> {
    if (!iterSome(descriptions, (x) => !!(x && x.length))) {
      // je prazdny
      return Promise.resolve([]);
    }

    const filter = [
      {
        Company: companyId,
      },
      {
        or: iterMap(
          descriptions,
          (x) => `startswith(Description, '${encodeURI(x)}')`
        ),
      },
    ];

    return ifsApi
      .get<IfsNativeCollection<IfsNativeLovResource>, IfsNativeLovResource>({
        path: "/TimeRegistrationManagerHandling.svc/Reference_LovResourceId",
        filter,
      })
      .then((x) => x.value);
  }

  function getTimesheetsInInterval(
    params: NativeTimesheetsParams
  ): Promise<IfsTimesheet[]> {
    return getNativeTimesheets(params).then((x) => x.map(ifsTimesheetMapper));
  }
}

type IfsNativeChangeTimesheet =
  | IfsNativeAddTimesheet
  | IfsNativeDeleteTimesheet
  | IfsNativeUpdateTimesheet;

export interface IfsUpdateSelection {
  toAdd: IfsNativeAddTimesheet[];
  toDelete: IfsNativeDeleteTimesheet[];
  toUpdate: IfsNativeUpdateTimesheet[];
}

function compareIfsTimesheets(
  timesheets: IfsTimesheetHours[],
  updateItems: IfsUpdateTimesheetHoursItem[]
): IfsTimesheetHours[] {
  return compareTimesheets(
    timesheets,
    (x) => x.duration,
    updateItems,
    (x) => x.duration,
    (ts, u) =>
      ts.task.id === u.taskId &&
      ts.task.type === u.taskType &&
      dateIsSame(ts.date, u.date)
  );
}

function toTask<T, R>(left: R, clb: (i: T) => Promise<R>): (i: T) => Task<R> {
  return (itm: T) => () =>
    clb(itm)
      .then((x) => {
        return x;
      })
      .catch((x) => {
        return left;
      });
}

interface UpdateSelectionParams {
  companyId: string;
  empNo: number;
  updateItems: Iterable<IfsUpdateTimesheetHoursItem>;
  serverItems: IfsNativeTimesheet[];
  resourceIdsByName: Record<string, string>;
}

export function getIfsUpdateSelection({
  companyId,
  empNo,
  updateItems,
  serverItems,
  resourceIdsByName,
}: UpdateSelectionParams): IfsUpdateSelection {
  return getUpdateSelection({
    serverItems,
    getServerKey: (i) => ({
      date: i.AccountDate,
      taskId: getTimesheetTaskId(i),
    }),
    getServerHours: (i) => i.Hours,

    updateItems,
    getUpdateKey: (i) => ({ date: i.date, taskId: i.taskId }),
    getUpdateHours: (i) => i.duration,

    createAdd: (item) => makeAddItem({ item, companyId, empNo: empNo + "" }),
    createUpdate: (s, u) => makeUpdateItem(s, resourceIdsByName, u.duration),
    createDelete: (item) => makeDeleteItem(item),
  });
}

interface MakeItemParams<T> {
  companyId: string;
  empNo: string;
  item: T;
}

function makeAddItem({
  item,
  companyId,
  empNo,
}: MakeItemParams<IfsUpdateTimesheetHoursItem>): IfsNativeAddTimesheet {
  switch (item.taskType) {
    case "Project":
      const prjParsed = parseIfsTimesheetWorkOrderId(item.taskId);
      return {
        AccountDateDate: item.date,
        AttPrjrep: true,
        CompanyId: companyId,
        EmpNo: empNo,
        DayHours: item.duration,
        JobTransactionParams: {},
        ShortName: prjParsed.WorkOrder,
        ReportCostCode: prjParsed.WorkOperation,
      };
    case "WorkOrder":
      const parsed = parseIfsTimesheetWorkOrderId(item.taskId);
      return {
        AccountDateDate: item.date,
        AttWo: true,
        CompanyId: companyId,
        MaintOrg: companyId,
        EmpNo: empNo,
        DayHours: item.duration,
        JobTransactionParams: {},
        WorkOrder: parsed.WorkOrder,
        WorkTask: parsed.WorkOperation,
      };
  }
}

function makeUpdateItem(
  item: IfsNativeTimesheet,
  resourceIdsByName: Record<string, string>,
  hours: DurationTime
): IfsNativeUpdateTimesheet {
  switch (item.Module) {
    case "PRJREP":
      return {
        AccountDateDate: item.AccountDate,
        CompanyId: item.CompanyId,
        EmpNo: item.EmpNo,
        DayHours: hours,
        EntryType: "PRJREP",
        JobTransactionParams: {},
        Objid: item.Objid,
        Objversion: item.Objversion,
        ResourceId: resourceIdsByName[item.ResourceName],
        ShortName: item.Col1,
      };

    case "WO":
      return {
        EntryType: "WO",
        AccountDateDate: item.AccountDate,
        CompanyId: item.CompanyId,
        EmpNo: item.EmpNo,
        DayHours: hours,
        Objid: item.Objid,
        Objversion: item.Objversion,
        JobTransactionParams: {},
        MaintOrg: item.CompanyId,
        WorkOrder: item.Col1,
        WorkTask: item.Col2,
      };
  }
}

function makeDeleteItem(item: IfsNativeTimesheet): IfsNativeDeleteTimesheet {
  return {
    AccountDate: item.AccountDate,
    CompanyId: item.CompanyId,
    EmpNo: item.EmpNo,
    Module: item.Module,
    Objid: item.Objid,
    Objversion: item.Objversion,
  };
}

type AddNativeTimesheetResponse = t.TypeOf<typeof AddNativeTimesheetResponse>;
const AddNativeTimesheetResponse = t.type({
  EntryType: IfsNativeModule,
  Result: t.literal("TRUE"),
});

type UpdateNativeTimesheetResponse = t.TypeOf<
  typeof UpdateNativeTimesheetResponse
>;
const UpdateNativeTimesheetResponse = t.type({
  value: IfsNativeModule,
});

function savePromise<T>(promise: () => Promise<T>): Task<boolean> {
  return () =>
    promise()
      .then((res) => {
        return true;
      })
      .catch((x) => {
        return false;
      });
}

interface NativeTimesheetsParams {
  companyId: string;
  empNo: number;
  begin: Date;
  end: Date;
  // filter: {
  // 	projectIds:
  // }
}

interface IfsNativeAttendance {
  keyref: string;
  CompanyId: string;
  EmpNo: string;
  AccountDate: string;
  Hours: number;
}

type IfsNativeEmployeeDayInfo = t.TypeOf<typeof IfsNativeEmployeeDayInfo>;

const IfsNativeEmployeeDayInfo = t.type({
  keyref: t.string,
  CompanyId: t.string,
  EmpNo: t.string,
  AccountDate: t.string,
  DayStatus: t.keyof({
    "1": null,
    NOTCOMPLETE: null,
  }),
  DayCompleted: t.boolean,
  ScheduleDayTypeId: t.string,
  OrdinaryDayTypeId: t.string,
});

interface IfsNativeWorkOrder {
  keyref: string;
  Company: string;
  WoNo: number;
  ErrDescr: string;
}

interface IfsNativeWorkOrderOperation {
  keyref: string;
  TaskSeq: number;
  WoNo: number;
  Company: string;
  Description: string;
}

interface IfsNativeProject {
  keyref: string;
  Company: string;
  Name: string;
  ActivityNo: string;
  Activity: string;
  Description: string;
  Project: string;
  SubProject: string;
  ShortName: string;
}

interface IfsNativeProjectReport {
  keyref: string;
  CompanyId: string;
  ShortName: string;
  ReportCode: string;
  ReportCostCode: string;
  ReportCostName: string;
}

function ifsTimesheetMapper(ts: IfsNativeTimesheet): IfsTimesheet {
  switch (ts.Module) {
    case "PRJREP":
      return {
        id: ts.keyref,
        task: {
          id: getIfsTimesheetProjectId(ts),
          name: ts.Description,
          type: "Project",
        },
        user: {
          id: ts.EmpNo,
          fullname: ts.CompanyPersonRef.EmployeeName,
        },
        duration: ts.Hours,
        date: new Date(ts.AccountDate),
      };
    case "WO": {
      return {
        id: ts.keyref,
        task: {
          id: getIfsTimesheetWorkOrderId(ts),
          name: parseWorkTaskName(ts.WorkOrder, ts.WorkTask),
          type: "WorkOrder",
        },
        user: {
          id: ts.EmpNo,
          fullname: ts.CompanyPersonRef.EmployeeName,
        },
        duration: ts.Hours,
        date: new Date(ts.AccountDate),
      };
    }
  }
}

function getTimesheetTaskId(item: IfsNativeTimesheet): string | undefined {
  switch (item.Module) {
    case "WO":
      return getIfsTimesheetWorkOrderId(item);

    case "PRJREP":
      return getIfsTimesheetProjectId(item);

    default:
      return undefined;
  }
}

export function getIfsTimesheetProjectId(item: IfsNativeTimesheet): string {
  return `${item.Col1}_${item.Col2}`;
}

export function parseIfsTimesheetWorkOrderId(taskId: string): {
  WorkOrder: string;
  WorkOperation: string;
} {
  const [col1, col2] = taskId.split("_");
  return {
    WorkOrder: col1,
    WorkOperation: col2,
  };
}

export function getIfsTimesheetWorkOrderId(item: IfsNativeTimesheet): string {
  return `${item.Col1}_${item.Col2}`;
}

function parseWorkTaskName(order: string, task: string) {
  return trimToFirstChar(order, "-") + " / " + trimToFirstChar(task, "-");
}

function tryCatch<T>(resolve: () => Promise<T>) {
  return TE.tryCatch(resolve, (err) =>
    onRejected({
      error: err,
    })
  );
}

interface OnReject {
  error: any;
  //   request?: ExpressRequest;
}

function onRejected({ error }: OnReject): IfsApiResponseError {
  let errorRequest: ApiRequest = error && error._request;

  return pipe(
    handleApiServerError<IfsApiNativeResponseError>(error),
    E.fold(
      // left
      (response) => {
        // const errorData = error && (error._errorData || error._errorData);

        return {
          ...response,
          code: "IFS",
        };
      },
      // right
      (response) => {
        return {
          code: "IFS",
          statusCode: response.status,
          message: response.data.error.message,
          ifsError: response.data.error,
          error: new ApiError(response.data.error.message),
          data: {
            request: errorRequest,
            errorData: undefined,
          },
        };
      }
    )
  );
}
