import axios, {
  Axios,
  AxiosRequestHeaders,
  AxiosError,
  AxiosRequestConfig,
} from "axios";
import { Agent } from "https";
import { ApiError, ApiStatusError } from "../../lib/Errors";
export { ClientRequest } from "http";
import {
  Api,
  ApiAllOptions,
  ApiData,
  ApiMethod,
  ApiOptions,
  ApiParams,
  ApiResponseError,
  BasicApiResponseError,
  Either,
  O,
} from "../../lib/types";
import {
  C,
  E,
  objectMap,
  pipe,
  R,
  resolveAxiosErrorResponse,
  TypeC,
} from "../../lib/utils";
import { IncomingHttpHeaders } from "http";
import { ApiStatusCodes } from "../../config";

export interface ApiServerIncomingRequest {
  headers: IncomingHttpHeaders;
}

interface Options {
  baseServerUrl: string;
  headers: AxiosRequestHeaders;
  allowedReqHeaders: string[];
  httpsAgent?: Agent;
  transformPath?: (path: string) => string;
  transformHeaders?: (headers: AxiosRequestHeaders) => AxiosRequestHeaders;
}

interface ApiServerBasicParams<T> {
  request: ApiServerIncomingRequest;
  baseUrl?: string;
  path: string;
  params?: ApiParams;
  codec?: TypeC<T>;
}

export interface ApiServerGetRequestParams<T> extends ApiServerBasicParams<T> {
  method: "GET";
}

export interface ApiServerDataRequestParams<T, D>
  extends ApiServerBasicParams<T> {
  method: "POST" | "PUT" | "DELETE";
  data?: D;
  dataCodec?: TypeC<D>;
}

type ApiServerDataNoMethodRequestParams<T, D> = Omit<
  ApiServerDataRequestParams<T, D>,
  "method"
>;

// export type ApiServerRequestAllParams<T, D> = Omit<
//   ApiServerGetRequestParams<T>,
//   "method"
// > &
//   Omit<ApiServerDataRequestParams<T, D>, "method"> & {
//     method: ApiMethod;
//   };

export type ApiServerRequestParams<T, D = ApiData> =
  | ApiServerGetRequestParams<T>
  | ApiServerDataRequestParams<T, D>;

export type ApiServerGetParams<T> = Omit<
  ApiServerGetRequestParams<T>,
  "request" | "path" | "method"
>;
export type ApiServerDataParams<T, D> = Omit<
  ApiServerDataRequestParams<T, D>,
  "request" | "path" | "method"
>;

export type ApiServerParams<T, D = ApiData> =
  | ApiServerGetParams<T>
  | ApiServerDataParams<T, D>;

export interface ApiServerResponseError<ResData = any, ReqData = any>
  extends AxiosError<ResData, ReqData> {}

export interface ApiServer {
  get<T>(path: string, params?: ApiParams, codec?: TypeC<T>): Promise<T>;
  post<T, D = any>(
    path: string,
    params?: ApiServerDataParams<T, D>
  ): Promise<T>;
  put<T, D = any>(path: string, params?: ApiServerDataParams<T, D>): Promise<T>;
  delete<T, D = any>(path: string, params?: ApiServerDataParams<T, D>);
  //   all<T, D = ApiData>(path: string, options: ApiServerParams<T, D>): Promise<T>;
}

export interface ApiServerWithRequest {
  get<T>(params: ApiServerGetRequestParams<T>): Promise<T>;
  post<T, D>(
    params: Omit<ApiServerDataRequestParams<T, D>, "method">
  ): Promise<T>;
  put<T, D>(
    params: Omit<ApiServerDataRequestParams<T, D>, "method">
  ): Promise<T>;
  all<T, D = ApiData>(options: ApiServerRequestParams<T, D>): Promise<T>;
}

export function createApi(
  request: ApiServerIncomingRequest,
  requestApi: ApiServerWithRequest
): ApiServer {
  return {
    get: (path, params, codec) =>
      requestApi.get({
        method: "GET",
        params,
        path,
        request,
        codec,
      }),
    put: (path, params) =>
      requestApi.put({
        ...params,
        path,
        request,
      }),
    post: (path, params) =>
      requestApi.post({
        ...params,
        path,
        request,
      }),

    delete: (path, params) =>
      requestApi.all({
        ...params,
        path,
        request,
        method: "DELETE",
      }),
  };
}

export function createApiWithRequest({
  baseServerUrl,
  headers,
  allowedReqHeaders,
  httpsAgent,
  transformPath,
  transformHeaders,
}: Options): ApiServerWithRequest {
  const instance = axios.create({
    baseURL: baseServerUrl,
    headers,
    httpsAgent,
  });

  //   instance.interceptors.request.use((x) => {
  //     console.log("Request", x);
  //     return x;
  //   });

  //   instance.interceptors.response.use((x) => {
  //     console.log("Response", x);
  //     return x;
  //   });

  function getResponse<T, D>(
    options: ApiServerRequestParams<T, D>
  ): Promise<T> {
    const {
      request: { headers: reqHeaders },
      baseUrl,
      method,
      codec,
    } = options;
    let path = options.path,
      params = options.params,
      data: D | undefined = undefined;

    if (method !== "GET") {
      data =
        options.data && options.dataCodec
          ? options.dataCodec.encode(options.data)
          : options.data;
    }

    if (transformPath) {
      path = transformPath(path);
    }

    if (params && typeof params === "string") {
      path += `?${params}`;
      params = undefined;
    }

    let transformedHeaders = transformRequestHeaders(
      reqHeaders,
      allowedReqHeaders
    );

    if (transformHeaders) {
      transformedHeaders = transformHeaders(transformedHeaders);
    }

    return instance
      .request<T>({
        method,
        params,
        url: path,
        baseURL: baseUrl,
        data,
        // headers,
        headers: transformedHeaders,
      })
      .then((x) => {
        if (codec) {
          const validation = codec.decode(x.data);
          switch (validation._tag) {
            case "Right":
              return validation.right;
            case "Left":
              throw new ApiStatusError(
                ApiStatusCodes.API_VALIDATION_DATA,
                "Validation result data failed.",
                {
                  errorData: validation.left,
                  request: {
                    method,
                    params,
                    url: path,
                    data,
                  },
                }
              );
          }
        }

        return x.data;
      })
      .catch((error) => {
        if (axios.isAxiosError(error)) {
          throw error;
        }

        if (error instanceof ApiError) {
          throw error;
        }

        throw new ApiError("", {
          errorData: {
            ...error,
          },
          request: {
            method,
            params,
            url: path,
            data,
          },
        });
      });
  }

  return {
    get: <T>(params: ApiServerRequestParams<T>) => getResponse(params),
    post: <T, D>(params: ApiServerDataNoMethodRequestParams<T, D>) =>
      getResponse({
        ...params,
        method: "POST",
      }),
    put: <T, D>(params: ApiServerDataNoMethodRequestParams<T, D>) =>
      getResponse({
        ...params,
        method: "PUT",
      }),
    all: <T, D>(params: ApiServerRequestParams<T, D>) => getResponse(params),
  };
}

function parseParams() {}

interface Error<T> {
  status: number;
  data: T;
}

// export function transformIncomingRequest({
//   headers,
// }: ApiServerIncomingRequest): AxiosRequestConfig {
//   return {
//     headers: transformRequestHeaders(headers),
//   };
// }

function transformRequestHeaders(
  headers: IncomingHttpHeaders,
  allowed: string[]
): AxiosRequestHeaders {
  allowed = allowed.map((x) => x.toLowerCase());
  return pipe(
    headers,
    R.filterMapWithIndex((k, v) => {
      return allowed.includes(k.toLowerCase()) ? O.of(v) : O.none;
    }),
    R.map((v) => (typeof v === "string" ? v : v.join(", ")))
  );
}

export function handleApiServerError<T>(
  e: any
): Either<BasicApiResponseError, Error<T>> {
  return pipe(
    resolveAxiosErrorResponse<T>(e),
    E.chain((result) => {
      if (result.kind === "NotFound") {
        return E.left({
          status: ApiStatusCodes.EXTERNAL_API_NOT_AVAILABLE,
          statusText: `External API '${result.hostname}' is not available.`,
        });
      }

      if (result.kind === "Status") {
        return E.left({
          status: result.status,
          statusText: result.statusText,
        });
      }

      if (result.kind === "AxiosCode") {
        return E.left({
          statusCode: ApiStatusCodes.EXTERNAL_API_AXIOS_CODE,
          message: `${result.code}: ${result.message}`,
        });
      }

      if (!result.contentType.includes("application/json")) {
        return E.left({
          status: result.status,
          statusText: result.statusText,
          data: result.data,
        });
      }

      return E.right({
        data: result.data,
        status: result.status,
      });
    }),
    E.mapLeft((error) => {
      if (error instanceof ApiStatusError) {
        return {
          statusCode: error.status,
          message: error.message,
          error,
        };
      } else if (error instanceof Error) {
        return {
          statusCode: 500,
          message: error.message,
          error: error,
        };
      }

      return {
        statusCode: error.status || 500,
        message: error.statusText || "API call failed",
        data: error.data,
      };
    })
  );
}
