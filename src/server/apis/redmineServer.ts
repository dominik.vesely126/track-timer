import { RedmineConfig } from "../../config";
import { Agent } from "https";
import { Router } from "../router";
import {
  RMApi,
  RMTimesheet,
  RMApiResponseError,
  TaskEither,
  TE,
  ApiResponseError,
  RMApiTimesheetsParams,
  RMApiResponse,
  RMApiTimesheetHours,
  RMTask,
  RMApiSearchTasksParams,
  RMSearchTask,
  RMApiUpdateTimesheetsHours,
  RMApiUpdateTimesheetsHoursParams,
  RMApiActivity,
  RMUser,
  ApiResponse,
  O,
  Either,
} from "../../lib/types";
import {
  RMNativeTimeEntryCollection,
  RMNativeTimeEntryHours,
  RMNativeAddTimeEntry,
  RMNativeTimeEntry,
  RMNativeCurrentUser,
  RMNativeIssueCollection,
  RMNativeTimeEntryDayHour,
  RMNativeSearchResultCollection,
  RMNativeActivityCollection,
  RMNativeError,
} from "../types/redmineTypes";
import {
  A,
  addDuration,
  arrayEveryAll,
  compareTimesheets,
  dateIsSame,
  dateMinAndMax,
  durationCompare,
  DurationTime,
  E,
  formatDateSql,
  formatDuration,
  getUpdateSelection,
  objectMap,
  objectReduce,
  parseDurationFromHours,
  pipe,
  subDuration,
  T,
  t,
  UpdateSelection,
} from "../../lib/utils";
import { ApiMethod } from "../../lib/types";
import {
  ApiServerIncomingRequest,
  createApi,
  createApiWithRequest,
  handleApiServerError,
} from "./apiServer";
import { ApiError } from "../../lib/Errors";
import { JSDOM } from "jsdom";

const httpsAgent = new Agent({
  minVersion: "TLSv1",
  maxVersion: "TLSv1.2",
});

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

type TimeEntryWithoutDuration = RMNativeTimeEntryDayHour;

const redmineApiWithRequest = createApiWithRequest({
  baseServerUrl: RedmineConfig.RM_URL,
  headers: {
    "Content-Type": "application/json",
    // "X-Redmine-API-Key": RedmineConfig.RM_API_KEY,
  },
  allowedReqHeaders: ["X-Redmine-API-Key"],
  httpsAgent,
  transformPath: (path: string) => addExtension(path, "json"),
});

function api(req: ApiServerIncomingRequest): RMApi {
  const redmineApi = createApi(req, redmineApiWithRequest);

  return {
    getCurrentUser,
    getTimesheets,
    getTimesheetsByHours,
    getActivities,
    updateTimesheetsHours,
    getTaskById,
    searchTasks,
  };

  function getCurrentUser(): RMApiResponse<RMUser> {
    return tryCatch(() =>
      redmineApi.get<RMNativeCurrentUser>("/users/current").then((current) => {
        return {
          id: current.user.id,
          fullname: `${current.user.firstname} ${current.user.lastname}`,
        };
      })
    );
  }

  function getTimesheets(
    params: RMApiTimesheetsParams
  ): RMApiResponse<RMTimesheet[]> {
    return tryCatch(async () => {
      const response = await getTimeEntries(
        params.userId,
        params.start,
        params.end
      );

      const issueIds = [...new Set(response.map((x) => x.issue.id))];
      const tasks = mapper(
        (await getTasks(issueIds)).issues,
        (x) => x.id,
        (v) => v.subject
      );

      return response.map<RMTimesheet>((x) => {
        return {
          id: x.id,
          duration: x.hours,
          date: x.spent_on,
          task: {
            id: x.issue.id,
            name: tasks[x.issue.id],
          },
          project: {
            id: x.project.id,
            name: x.project.name,
          },
          user: {
            id: x.user.id,
            fullname: x.user.name,
          },
        };
      });
    });
  }

  function getTimesheetsByHours({
    userId,
    start,
    end,
  }: RMApiTimesheetsParams): RMApiResponse<RMApiTimesheetHours[]> {
    return tryCatch(async () => {
      const timesheet = await getTimeEntries(userId, start, end);
      const sumHours = {} as Record<
        string,
        Record<number, TimeEntryWithoutDuration>
      >;

      for (const ts of timesheet) {
        const key1 = formatDateSql(ts.spent_on);
        const key2 = ts.issue.id;
        if (!sumHours[key1]) {
          sumHours[key1] = {};
        }

        if (!sumHours[key1][key2]) {
          sumHours[key1][key2] = {
            date: new Date(ts.spent_on),
            issueId: ts.issue.id,
            projectId: ts.project.id,
            user: ts.user,
            duration: DurationTime.zero(),
          };
        }

        sumHours[key1][key2].duration = addDuration(
          ts.hours,
          sumHours[key1][key2].duration
        );
      }

      return Object.keys(sumHours).reduce<RMApiTimesheetHours[]>(
        (result, key1) => {
          for (const key2 in sumHours[key1]) {
            const ts = sumHours[key1][key2];
            result.push({
              date: ts.date,
              issueId: ts.issueId,
              projectId: ts.projectId,
              user: {
                id: ts.user.id,
                fullname: ts.user.name,
              },
              duration: ts.duration,
            });
          }

          return result;
        },
        []
      );
    });
  }

  function getActivities(): RMApiResponse<RMApiActivity[]> {
    return tryCatch(() =>
      redmineApi
        .get<RMNativeActivityCollection>("/enumerations/time_entry_activities")
        .then((x) =>
          x.time_entry_activities.map<RMApiActivity>((item) => ({
            id: item.id,
            name: item.name,
          }))
        )
    );
  }

  function updateTimesheetsHours({
    items: updateItems,
  }: RMApiUpdateTimesheetsHoursParams): RMApiResponse<RMApiTimesheetHours[]> {
    return tryCatch(async () => {
      if (!updateItems.length) {
        throw new Error("No update items sent.");
      }
      const sameUserId = arrayEveryAll(
        updateItems,
        (a, b) => a.userId === b.userId
      );

      if (sameUserId) {
        throw new Error("Updated items has different user ids.");
      }

      const userId = updateItems[0].userId;
      const [start, end] = dateMinAndMax(updateItems, (x) => x.date);
      const serverItems = await getTimeEntries(userId, start, end);

      const updateSelection = redmineUpdateSelection(
        serverItems,
        (i) => i.hours,
        (i) => ({ taskId: i.issue.id, date: i.spent_on }),

        updateItems,
        (i) => i.duration,
        (i) => ({ taskId: i.issueId, date: i.date }),

        (i, hours) => ({
          time_entry: {
            issue_id: i.issueId,
            spent_on: i.date,
            user_id: i.userId,
            activity_id: i.activityId,
            hours: hours,
            comments: undefined,
          },
        })
      );

      //   make create
      const updateResult: Either<string, any> = await pipe(
        updateSelection,
        A.map((x) => tryCatch(() => createNativeEntry(x))),
        TE.sequenceArray,
        TE.mapLeft((x) => "Update redmine items failed.")
      )();

      if (updateResult._tag === "Left") {
        throw new ApiError(updateResult.left);
      }

      const compareTsAndUpdate: (
        ts: RMApiTimesheetHours,
        update: RMApiUpdateTimesheetsHours
      ) => boolean = (ts, update) =>
        ts.issueId === update.issueId && dateIsSame(ts.date, update.date);

      const newTimesheets = await pipe(
        getTimesheetsByHours({ userId, start, end }),
        TE.map((items) =>
          pipe(
            items,
            A.filterMap((itm) => {
              if (updateItems.find((x) => compareTsAndUpdate(itm, x))) {
                return O.of(itm);
              }
              return O.none;
            })
          )
        ),
        TE.getOrElse((x) => [] as any)
      )();

      const compared = compareTimesheets(
        newTimesheets,
        (x) => x.duration,
        updateItems,
        (x) => x.duration,
        compareTsAndUpdate
      );

      return compared;
    });
  }

  function searchTasks({
    query,
    top,
    skip,
  }: RMApiSearchTasksParams): RMApiResponse<RMSearchTask[]> {
    // vyhledavani neni omezene pouze na TASKy!!!
    return tryCatch(async () => {
      const search = await redmineApi.get<RMNativeSearchResultCollection>(
        "/search",
        {
          q: query,
          limit: top,
          offset: skip,
        }
      );

      if (typeof search === "string") {
        return [parseTaskFromHTMLContent(search)];
      } else {
        return search.results
          .filter((x) => x.type === "issue")
          .map<RMSearchTask>((task) => ({
            id: task.id,
            name: task.title,
          }));
      }
    });
  }

  function parseTaskFromHTMLContent(htmlContent: string): RMTask {
    // quick jump to issue ID
    const { document } = new JSDOM(htmlContent).window;
    const content = document.querySelector("#content");

    if (!content) {
      throw new Error("HTML element #content not found.");
    }

    const h2 = content.querySelector("h2")?.textContent;

    if (!h2) {
      throw new Error("HTML element H2 not found.");
    }

    const h3 = content.querySelector("h3")?.textContent;

    if (!h3) {
      throw new Error("HTML element H2 not found.");
    }

    const id = Number(h2.substr(h2.lastIndexOf("#") + 1));

    if (isNaN(id)) {
      throw new Error(`ID was not found in H2 '${h2}'.`);
    }

    return {
      id,
      name: h3,
    } as RMTask;
  }

  function getTaskById(id: number): RMApiResponse<RMTask | null> {
    return tryCatch(async () => {
      const response = await getTasks([id]);

      if (!response.issues || response.issues.length < 1) {
        return null;
      }

      const issue = response.issues[0];

      return {
        id: issue.id,
        name: issue.subject,
        zakazka:
          issue.custom_fields.find(
            (x) => x.name === RedmineConfig.RM_CF_ZAKAZKA
          )?.value || null,
      };
    });
  }

  function getTimeEntries(
    userId: number,
    start: Date,
    end: Date
  ): Promise<RMNativeTimeEntryHours[]> {
    return redmineApi
      .get<RMNativeTimeEntryCollection>(
        "/time_entries",
        {
          user_id: userId,
          from: formatDateParameter(start),
          to: formatDateParameter(end),
        },
        RMNativeTimeEntryCollection
      )
      .then((x) => {
        return x.time_entries;
      });
  }

  function getTasks(issueId: number[]): Promise<RMNativeIssueCollection> {
    return redmineApi.get<RMNativeIssueCollection>("/issues", {
      issue_id: issueId.join(","),
      status_id: "*",
    });
  }

  function createNativeEntry(entry: RMNativeAddTimeEntry): Promise<any> {
    return redmineApi.post<any, RMNativeAddTimeEntry>("/time_entries", {
      data: entry,
      dataCodec: RMNativeAddTimeEntry,
    });
  }
}

function addExtension(url: string, extension: "json" | "xml") {
  const index = url.indexOf("?");

  if (index === -1) {
    return `${url}.${extension}`;
  } else {
    return `${url.substring(0, index)}.${extension}${url.substring(index)}`;
  }
}

export function useNativeRedmine(router: Router) {
  router.addRoute<ApiResponseError, any, any>({
    method: "ALL",
    inRoute: "/rm_native/*",
    result: (req) =>
      tryCatch(() => {
        const path = req.url.replace("/rm_native/", "");
        return redmineApiWithRequest.all({
          request: req,
          path,
          method: <ApiMethod>req.method,
          params: req.body,
        });
      }),
  });
}

export function useRedmine(router: Router) {
  router.addRoute<ApiResponseError, RMUser>({
    method: "GET",
    inRoute: "/rm/user/current",
    codec: RMUser,
    result: (req) => api(req).getCurrentUser(),
  });

  // get timesheets
  router.addRoute<ApiResponseError, RMTimesheet[], RMApiTimesheetsParams>({
    method: "GET",
    inRoute: "/rm/timesheet",
    codec: t.array(RMTimesheet),
    paramNames: {
      userId: "number",
      start: "date",
      end: "date",
    },
    result: (req, params) => {
      return api(req).getTimesheets(params);
    },
  });

  // get timesheets day hours
  router.addRoute<
    RMApiResponseError,
    RMApiTimesheetHours[],
    RMApiTimesheetsParams
  >({
    method: "GET",
    inRoute: "/rm/timesheet/hours",
    codec: t.array(RMApiTimesheetHours),
    paramNames: {
      userId: "number",
      start: "date",
      end: "date",
    },
    result: (req, params) => {
      return api(req).getTimesheetsByHours(params);
    },
  });

  router.addRoute<RMApiResponseError, RMApiActivity[], any>({
    method: "GET",
    inRoute: "/rm/timesheet/activities",
    result: (req) => api(req).getActivities(),
  });

  router.addRoute<
    RMApiResponseError,
    RMApiTimesheetHours[],
    RMApiUpdateTimesheetsHoursParams
  >({
    method: "PUT",
    inRoute: "/rm/timesheet/hours",
    paramNames: {
      items: t.array(RMApiUpdateTimesheetsHours),
    },
    codec: t.array(RMApiTimesheetHours),
    result: (req, params) => api(req).updateTimesheetsHours(params),
  });

  // rm task
  router.addRoute<RMApiResponseError, RMTask, { id: number }>({
    method: "GET",
    inRoute: "/rm/task",
    paramNames: {
      id: "number",
    },
    result: (req, { id }) => api(req).getTaskById(id),
  });

  // rm task search
  router.addRoute<RMApiResponseError, RMSearchTask[], RMApiSearchTasksParams>({
    method: "GET",
    inRoute: "/rm/task/search",
    paramNames: {
      query: "string",
      skip: "number",
      top: "number",
    },
    result: (req, params) => api(req).searchTasks(params),
  });
}

function mapper<T, K extends number | string | symbol, V>(
  arr: T[],
  key: (i: T) => K,
  value: (i: T) => V
): Record<K, V> {
  const result = {} as Record<K, V>;

  for (const itm of arr) {
    result[key(itm)] = value(itm);
  }

  return result;
}

function formatDateParameter(date: Date) {
  return formatDateSql(date);
}

function tryCatch<T>(resolve: () => Promise<T>): RMApiResponse<T> {
  return TE.tryCatch<RMApiResponseError, T>(resolve, onRejected);
}

function onRejected(reason: unknown): RMApiResponseError {
  return pipe(
    handleApiServerError<RMNativeError>(reason),
    E.fold(
      // left
      (e) => {
        return {
          ...e,
          code: "RM",
          rmErrors: [],
        };
      },
      // right
      (native) => {
        return {
          rmErrors: native.data.errors,
          code: "RM",
          statusCode: native.status,
          message: "Server error",
        };
      }
    )
  );
}

function redmineUpdateSelection<T, U>(
  serverItems: T[],
  serverHours: (i: T) => DurationTime,
  serverKey: (i: T) => { taskId: number; date: Date },
  updateItems: U[],
  updateHours: (u: U) => DurationTime,
  updateKey: (u: U) => { taskId: number; date: Date },

  createAdd: (u: U, hours: DurationTime) => RMNativeAddTimeEntry
): RMNativeAddTimeEntry[] {
  const grouped: Record<string, DurationTime> = {};
  const createKey = ({ taskId, date }: { taskId: number; date: Date }) =>
    `${formatDateSql(date)}_${taskId}`;

  for (const ts of serverItems) {
    const tsKey = createKey(serverKey(ts));
    if (!grouped[tsKey]) {
      grouped[tsKey] = DurationTime.zero();
    }

    grouped[tsKey] = addDuration(grouped[tsKey], serverHours(ts));
  }

  const reuslt: RMNativeAddTimeEntry[] = [];
  const errors: [U, string][] = [];

  for (const update of updateItems) {
    const key = createKey(updateKey(update));
    const uHours = updateHours(update);

    if (!grouped[key]) {
      reuslt.push(createAdd(update, uHours));
      continue;
    }

    const tsHours = grouped[key];
    const diff = subDuration(uHours, tsHours);

    if (durationCompare(diff, DurationTime.zero()) <= 0) {
      errors.push([update, "Hours are less or equals like on server."]);
      continue;
    }

    reuslt.push(createAdd(update, diff));
  }

  if (errors.length > 0) {
    throw new ApiError("Redmine can just add new time entries.", {
      errorData: errors,
    });
  }

  return reuslt;
}
