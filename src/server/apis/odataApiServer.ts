import { AxiosRequestHeaders } from "axios";
import buildQuery, { Expand, Filter, OrderBy } from "odata-query";
import { ApiError } from "../../lib/Errors";
import { ApiData, ApiMethod, ApiParams } from "../../lib/types";
import { C } from "../../lib/utils";
import {
  ApiServerIncomingRequest,
  createApi,
  createApiWithRequest,
} from "./apiServer";

type ODataApiFilterItem<T> = Record<keyof T, [string, string]>[];

interface ODataBaseRequestOptions {
  request: ApiServerIncomingRequest;
  path: string;
  codec?: C;
  query?: ApiParams;
}

interface ODataRequestOptions<T extends ApiData>
  extends ODataBaseRequestOptions {
  method: "POST" | "PUT" | "DELETE";
  data?: T;
  dataCodec?: C;
}

interface ODataGetRequestOptions<T> extends ODataBaseRequestOptions {
  method: "GET";
  select?: Array<keyof T>;
  filter?: Filter;
  expand?: Expand<T>;
  orderBy?: OrderBy<T>;
  top?: number;
  skip?: number;
}

type ODataRequestAllOptions =
  | ODataGetRequestOptions<any>
  | ODataRequestOptions<any>;

interface ODataApiOptions {
  baseServerUrl: string;
  headers: AxiosRequestHeaders;
  allowedReqHeaders: string[];
}

type KindGetRequestOptions<T> = ODataGetRequestOptions<T> & {
  kind: "GET";
  method: "GET";
};

type KindDataRequestOptions<D> = ODataRequestOptions<D> & {
  kind: "DATA";
  method: "POST" | "PUT" | "DELETE";
};

type KindAllRequestOptions = ODataRequestAllOptions & {
  kind: "ALL";
  method: ApiMethod;
};

type KindRequestOptions<T, D> =
  | KindGetRequestOptions<T>
  | KindDataRequestOptions<D>
  | KindAllRequestOptions;

export interface ODataApiWithRequest {
  get<TResult, TEntity>(
    options: Omit<ODataGetRequestOptions<TEntity>, "method">
  ): Promise<TResult>;
  post<TCollection, D>(
    options: Omit<ODataRequestOptions<D>, "method">
  ): Promise<TCollection>;
  put<TCollection, D>(
    options: Omit<ODataRequestOptions<D>, "method">
  ): Promise<TCollection>;
  all<TCollection>(options: ODataRequestAllOptions): Promise<TCollection>;
}

type ODataGetOptions<T> = Omit<ODataGetRequestOptions<T>, "request">;
type ODataOptions<D> = Omit<ODataRequestOptions<D>, "request">;
type ODataAllOptions = Omit<ODataRequestAllOptions, "request">;

export interface ODataApi {
  get<TResult, TEntity>(
    options: Omit<ODataGetOptions<TEntity>, "method">
  ): Promise<TResult>;
  post<TCollection, D>(
    options: Omit<ODataOptions<D>, "method">
  ): Promise<TCollection>;
  put<TCollection, D>(
    options: Omit<ODataOptions<D>, "method">
  ): Promise<TCollection>;
  all<TCollection>(options: ODataAllOptions): Promise<TCollection>;
}

export function createODataApi(
  request: ApiServerIncomingRequest,
  requestApi: ODataApiWithRequest
): ODataApi {
  return {
    get: (params) =>
      requestApi.get({
        ...params,
        request,
      }),
    put: (params) =>
      requestApi.put({
        ...params,
        request,
      }),
    post: (params) =>
      requestApi.post({
        ...params,
        request,
      }),
    all: (params) =>
      requestApi.all({
        ...params,
        request,
      }),
  };
}

export function createODataApiWithRequest({
  baseServerUrl,
  headers,
  allowedReqHeaders,
}: ODataApiOptions): ODataApiWithRequest {
  const api = createApiWithRequest({
    baseServerUrl,
    headers,
    allowedReqHeaders,
  });

  function getResponse<T, D, TResult>(
    inputParams: KindRequestOptions<T, D>
  ): Promise<TResult> {
    const {
      query: queryString,
      path,
      codec,
      kind,
      method,
      request,
    } = inputParams;
    let params: ApiParams | string = undefined;
    if (kind === "GET") {
      params =
        queryString !== undefined
          ? queryString
          : buildODataParams({
              ...inputParams,
            });
    }

    let data: ApiData | undefined = undefined;

    if (kind === "DATA" || (kind === "ALL" && method !== "GET")) {
      if (inputParams.dataCodec) {
        const result = inputParams.dataCodec.encode(inputParams.data);

        data = result;
      } else {
        data = inputParams.data;
      }
    }

    return api.all<TResult>({
      request,
      method,
      path,
      params: params,
      data,
      codec,
    });
  }

  return {
    get: (options) =>
      getResponse({
        kind: "GET",
        method: "GET",
        ...options,
      }),
    post: (options) =>
      getResponse({
        kind: "DATA",
        method: "POST",
        ...options,
      }),
    put: (options) =>
      getResponse({
        kind: "DATA",
        method: "PUT",
        ...options,
      }),
    all: (options) =>
      getResponse({
        kind: "ALL",
        ...options,
      }),
  };
}

function buildODataParams<T>(
  options: Omit<ODataGetRequestOptions<T>, "path">
): string {
  return buildQuery({
    ...options,
  }).substr(1); // remove question mark
}
