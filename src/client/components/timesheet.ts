import {
  addDuration,
  dateIsSame,
  DurationTime,
  E,
  formatDate,
  formatDateSql,
  formatDateTime,
  formatDuration,
  iterMap,
  objectMap,
  pipe,
  T,
} from "../../lib/utils";
import { TableColumnCtx } from "element-plus/lib/components/table/src/table-column/defaults";
import {
  ApiKind,
  ApiResponse,
  ApiResponseError,
  ApiResponseErrorK,
  BasicApiResponseError,
  ClickApiResponse,
  ClickApiResponseError,
  ClickApiTimesheetOptions,
  ClickTask,
  ClickTimesheet,
  ClickTimesheetHours,
  Either,
  IfsApiResponse,
  IfsApiResponseError,
  IfsCurrentUserInfo,
  IfsEmployeeDayInfo,
  IfsEmployeeDayStatus,
  IfsTimesheetHours,
  IfsTimesheetParams,
  IfsUpdateTimesheetHoursItem,
  IfsUpdateTimesheetHoursParams,
  RMApiResponse,
  RMApiResponseError,
  RMApiTimesheetHours,
  RMApiTimesheetsParams,
  RMApiUpdateTimesheetsHours,
  RMTimesheet,
  Task,
  TaskEither,
  TE,
} from "../../lib/types";
import {
  AttendanceTimesheetItem,
  ReportTimesheetItem,
  TimesheetDayItem,
  TimesheetItem,
  UpdateRedmineTimesheetItem,
  UpdateReportTimesheetItem,
} from "../ts/types";
import { ifsClient } from "../ts/apis/ifsClient";
import {
  ifsParseTaskIdByType,
  ifsTaskTypeByTaskId,
} from "../../lib/utils/ifsUtils";
import { redmineClient } from "../ts/apis/redmineClient";
import {
  ApiStatusCodes,
  ClickupConfig,
  IfsConfig,
  RedmineConfig,
} from "../../config";
import { clickupClient } from "../ts/apis";
import {
  AuthUserInfo,
  AuthUserInfoKind,
  ClickUserInfo,
  getAuthUserInfo,
  getClickUserInfo,
  getIfsUserInfo,
  getRedmineUserInfo,
  getUserInfo,
  IfsUserInfo,
  processUserApiCall,
  RMUserInfo,
  User,
} from "../ts/utils";

export type TimesheetType = "ALL" | "RPTR" | "RM" | "CLICK";

type IfsClientTimesheetParams = Omit<IfsTimesheetParams, "companyId" | "empNo">;

type RMClientTimesheetParams = Omit<RMApiTimesheetsParams, "userId">;

type ClickClientTimesheetParams = Omit<ClickApiTimesheetOptions, "userId">;

export function dateFormatter(
  row: any,
  column: TableColumnCtx<any>,
  cellValue: any,
  index: number
) {
  const date: Date = cellValue;
  return formatDateTime(date);
}

export function durationFormatter(
  row: any,
  column: TableColumnCtx<any>,
  cellValue: any,
  index: number
) {
  const duration: DurationTime | null = cellValue;
  if (!duration) {
    return "";
  }

  return formatDuration(duration);
}

export function sumarizer<T>(
  idProp: (i: TimesheetItem) => T,
  durationProp: (i: TimesheetItem) => DurationTime | undefined
) {
  let sum: DurationTime = DurationTime.zero();
  let existIds: T[] = [];

  function add(item: TimesheetItem) {
    const id = idProp(item);
    if (existIds.includes(id)) {
      return;
    }
    sum = addDuration(sum, durationProp(item));
    existIds.push(id);
  }

  return {
    add,
    result: (): DurationTime => sum,
  };
}

export function* getTimesheetsIterator(
  items: TimesheetDayItem[]
): Iterable<TimesheetItem> {
  for (const day of items) {
    for (const itm of day.items) {
      yield itm;
    }
  }
}

export function groupTimesheetsByDay(
  items: ClickTimesheetHours[]
): TimesheetDayItem[] {
  let currentDate: number = 0;
  let current: TimesheetDayItem | null = null;
  const result: TimesheetDayItem[] = [];

  for (const item of items.sort(sortTimesheet)) {
    if (currentDate != item.date.getTime()) {
      currentDate = item.date.getTime();
      current = {
        id: currentDate,
        date: item.date,
        items: [],
      };
      result.push(current);
    }

    current!.items.push({
      id: item.id,
      date: item.date,
      duration: item.duration,
      taskId: item.task?.id || null,
      taskName: item.task?.name || null,
      timesheets: item.timesheets,
      ifsTaskId: item.task?.ifsId || null,
      redmineTaskId: item.task?.redmineId || null,
      reportHoursChanged: false,
      redmineHoursChanged: false,
    });
  }

  return result;
}

export function ifsUpdateTimesheets({
  days,
}: Omit<IfsUpdateTimesheetHoursParams, "items" | "companyId" | "empNo"> & {
  days: TimesheetDayItem[];
}): IfsApiResponse<UpdateReportTimesheetItem[]> {
  const updateItems = days.reduce((dict, day) => {
    day.items.forEach((item) => {
      if (!item.ifsTaskId || !item.reportHoursChanged) {
        return;
      }

      const taskType = ifsTaskTypeByTaskId(item.ifsTaskId);
      const taskId = ifsParseTaskIdByType(item.ifsTaskId, taskType);
      const key = `${formatDateSql(item.date)}_${taskId}_${taskType}`;

      if (!dict[key]) {
        dict[key] = {
          date: item.date,
          duration: item.reportHours || DurationTime.zero(),
          taskId,
          taskType,
        };
      }

      // dict[key].duration = addDuration(dict[key].duration, item.reportHours);
    });
    return dict;
  }, {} as Record<string, IfsUpdateTimesheetHoursItem>);

  const arrayUpdate = objectMap(updateItems, (x) => x);

  if (!arrayUpdate.length) {
    return TE.of([]);
  }

  return pipe(
    processUserApiCall("IFS", (user) =>
      ifsClient.updateTimesheetsHours({
        companyId: user.companyId,
        empNo: user.empNo,
        items: arrayUpdate,
      })
    ),
    TE.map((r) => {
      const updated = r.map<UpdateReportTimesheetItem>((itm) => ({
        date: itm.date,
        ifsTaskId: itm.task.id,
        reportHours: itm.duration,
      }));

      // add delete items
      for (const itm of arrayUpdate) {
        const exists = updated.some(
          (upd) =>
            dateIsSame(itm.date, upd.date) && itm.taskId === upd.ifsTaskId
        );

        if (exists) continue;

        // add delete item
        updated.push({
          date: itm.date,
          ifsTaskId: itm.taskId,
          reportHours: DurationTime.zero(),
        });
      }

      return updated;
    })
  );
}

interface RMUpdateTimesheetsParams {
  activityId: number;
  days: TimesheetDayItem[];
}

export function rmUpdateTimesheets({
  activityId,
  days,
}: RMUpdateTimesheetsParams): RMApiResponse<UpdateRedmineTimesheetItem[]> {
  return pipe(
    processUserApiCall("RM", (user) => {
      const items = days.reduce<Record<string, RMApiUpdateTimesheetsHours>>(
        (dict, day) => {
          day.items.forEach((item) => {
            if (!(item.redmineTaskId && item.redmineHoursChanged)) {
              return;
            }

            if (!dict[item.redmineTaskId]) {
              dict[item.redmineTaskId] = {
                activityId,
                userId: user.id,
                issueId: item.redmineTaskId,
                date: item.date,
                duration: item.redmineHours || DurationTime.zero(),
                description: undefined,
              };
            }
          });

          return dict;
        },
        {}
      );
      const arrayItems = objectMap(items, (x) => x);

      return pipe(
        redmineClient.updateTimesheetsHours({ items: arrayItems }),
        TE.map((r) => {
          const updated = r.map<UpdateRedmineTimesheetItem>((itm) => ({
            date: itm.date,
            reportHours: itm.duration,
            redmineTaskId: itm.issueId,
            redmineHours: itm.duration,
          }));

          // add delete items
          for (const itm of arrayItems) {
            const exists = updated.some(
              (upd) =>
                dateIsSame(itm.date, upd.date) &&
                itm.issueId === upd.redmineTaskId
            );

            if (exists) continue;

            // add delete item
            updated.push({
              date: itm.date,
              redmineTaskId: itm.issueId,
              redmineHours: DurationTime.zero(),
            });
          }

          return updated;
        })
      );
    })
  );
}

export function loadAttendanceHours(
  params: IfsClientTimesheetParams
): IfsApiResponse<AttendanceTimesheetItem[]> {
  return pipe(
    processUserApiCall("IFS", (user) =>
      ifsClient.getAttendanceHours({
        ...params,
        companyId: user.companyId,
        empNo: user.empNo,
      })
    ),
    TE.map((items) =>
      items.map<AttendanceTimesheetItem>((itm) => ({
        date: itm.date,
        attendanceHours: itm.hours,
      }))
    )
  );
}

export function loadClickTimesheetDetailItems(
  params: Omit<ClickApiTimesheetOptions, "userId">
): ClickApiResponse<ClickTimesheet[]> {
  return pipe(
    processUserApiCall("Click", (user) =>
      clickupClient.getTimesheets({
        ...params,
        userId: user.id,
      })
    )
  );
}

export function loadEmployeeDayInfo(
  params: IfsClientTimesheetParams
): IfsApiResponse<IfsEmployeeDayInfo[]> {
  return pipe(
    processUserApiCall("IFS", (user) =>
      ifsClient.getEmployeeDayInfo({
        ...params,
        companyId: user.companyId,
        empNo: user.empNo,
      })
    )
  );
}

export function loadReportTimesheetItems(
  params: IfsClientTimesheetParams
): IfsApiResponse<ReportTimesheetItem[]> {
  return pipe(
    processUserApiCall("IFS", (user) =>
      ifsClient.getTimesheetsByHours({
        ...params,
        companyId: user.companyId,
        empNo: user.empNo,
      })
    ),
    TE.map((items) =>
      items.map<ReportTimesheetItem>((itm) => ({
        date: itm.date,
        reportHours: itm.duration,
        reportHoursOld: itm.duration,
        reportHoursChanged: false,
        ifsTaskId: itm.task.id,
      }))
    )
  );
}

export function searchClickTasks(
  teamId: number,
  query: string
): ClickApiResponse<ClickTask[]> {
  return clickupClient.searchTasks({
    teamId,
    query,
  });
}

export function loadRedmineTimesheetItems(
  params: RMClientTimesheetParams
): RMApiResponse<RMApiTimesheetHours[]> {
  return processUserApiCall("RM", (user) =>
    redmineClient.getTimesheetsByHours({
      ...params,
      userId: user.id,
    })
  );
}

export function loadClickTimesheetItems(
  teamId: number,
  item: TimesheetItem
): ClickApiResponse<ClickTimesheet[]> {
  return pipe(
    processUserApiCall("Click", (user) =>
      clickupClient.getTimesheets({
        start: item.date,
        end: item.date,
        teamId,
        userId: user.id,
      })
    )
  );
}

function taskToTaskEither<A, L, R>(
  a: Task<A>,
  r: (a: A) => Either<L, R>
): TaskEither<L, R> {
  return pipe(a, T.map(r));
}

function toTaskEither<L, R, TL, TR>(
  taskEither: TaskEither<L, R>,
  onLeft: (i: L) => TL,
  onRight: (i: R) => TR
): TaskEither<TL, TR> {
  return pipe(
    taskEither,
    TE.map(onRight),
    TE.mapLeft((e: L) => onLeft(e))
  );
}

function toTask<L, R, T>(
  taskEither: TaskEither<L, R>,
  onLeft: (i: L) => T,
  onRight: (i: R) => T
): Task<T> {
  return TE.fold(
    (e: L) => T.of(onLeft(e)),
    (i: R) => T.of(onRight(i))
  )(taskEither);
}

function sortTimesheet(a: ClickTimesheetHours, b: ClickTimesheetHours) {
  return a.id.localeCompare(b.id); // datum + task id
}
