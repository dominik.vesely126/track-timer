import { createClientApi, resolveClientApiResponseError } from ".";
import { RedmineConfig } from "../../../config";
import {
  RMApi,
  RMApiActivity,
  RMApiResponse,
  RMApiResponseError,
  RMApiSearchTasksParams,
  RMApiTimesheetHours,
  RMApiTimesheetsParams,
  RMApiUpdateTimesheetsHoursParams,
  RMTask,
  RMTimesheet,
  RMUser,
  TE,
} from "../../../lib/types";
import { E, pipe, T, t } from "../../../lib/utils";
import { getAuthToken } from "../utils";

export const api = createClientApi({
  prefixUrl: "/rm",
  headers: {
    "Content-Type": "application/json",
  },
  dynamic: pipe(
    getAuthToken("RM"),
    T.map((token) => ({
      headers: {
        "X-Redmine-API-Key": token || "",
      },
    }))
  ),
});

export const redmineClient: RMApi = {
  getCurrentUser,
  getTimesheets,
  getTimesheetsByHours,
  getActivities,
  getTaskById,
  searchTasks,
  updateTimesheetsHours,
};

function getCurrentUser(): RMApiResponse<RMUser> {
  return tryCatch(() => api.get<RMUser>("/user/current", RMUser));
}

function searchTasks(params: RMApiSearchTasksParams): RMApiResponse<RMTask[]> {
  return tryCatch(async () => api.get<RMTask[]>("/task/search", null, params));
}

function getTaskById(id: number): RMApiResponse<RMTask | null> {
  return tryCatch(() =>
    api.get<RMTask | null>("/task", null, {
      id,
    })
  );
}

function getTimesheets(
  params: RMApiTimesheetsParams
): RMApiResponse<RMTimesheet[]> {
  return tryCatch(() =>
    api.get<RMTimesheet[]>("/timesheet", t.array(RMTimesheet), params)
  );
}

function getTimesheetsByHours(
  params: RMApiTimesheetsParams
): RMApiResponse<RMApiTimesheetHours[]> {
  return tryCatch(() =>
    api.get<RMApiTimesheetHours[]>(
      "/timesheet/hours",
      t.array(RMApiTimesheetHours),
      params
    )
  );
}

function getActivities() {
  return tryCatch(() =>
    api.get<RMApiActivity[]>("/timesheet/activities", null)
  );
}

function updateTimesheetsHours(params: RMApiUpdateTimesheetsHoursParams) {
  return tryCatch(() =>
    api.put<RMApiTimesheetHours[], RMApiUpdateTimesheetsHoursParams>(
      "/timesheet/hours",
      {
        codec: t.array(RMApiTimesheetHours),
        data: params,
        dataCodec: RMApiUpdateTimesheetsHoursParams,
      }
    )
  );
}

function tryCatch<T>(promise: () => Promise<T>): RMApiResponse<T> {
  return TE.tryCatch(promise, onRejected);
}

function onRejected(error: unknown): RMApiResponseError {
  return pipe(
    resolveClientApiResponseError<RMApiResponseError>(error),
    E.getOrElse((error) => ({
      ...error,
      code: "RM",
    }))
  );
}
