import { createClientApi, resolveClientApiResponseError } from ".";
import {
  IfsApi,
  IfsApiResponse,
  IfsApiResponseError,
  IfsProject,
  IfsProjectReport,
  IfsProjectParams,
  IfsSearchParams,
  IfsTimesheetHours,
  IfsTimesheetParams,
  IfsWorkOrder,
  IfsWorkOrderOperation,
  IfsWorkOrderOperationParams,
  TE,
  IfsProjectReportParams,
  IfsAttendanceHoursParams,
  IfsAttendanceHours,
  IfsEmployeeDayInfo,
  IfsEmployeeDayInfoParams,
  IfsUpdateTimesheetHoursParams,
  IfsCurrentUserInfo,
} from "../../../lib/types";
import { E, parseCookie, pipe, T, t } from "../../../lib/utils";
import { getAuthToken } from "../utils";

export const api = createClientApi({
  prefixUrl: "/ifs",
  dynamic: pipe(
    getAuthToken("IFS"),
    T.map((token) => ({
      headers: {
        Authorization: token || "",
        // IFS_XSRF_TOKEN: xsrfToken,
      },
    }))
  ),
});

export const ifsClient: IfsApi = {
  getCurrentUser,
  getTimesheetsByHours,
  getAttendanceHours,
  getEmployeeDayInfo,

  searchProject,
  searchWorkOrder,

  getProjectReports,
  getWorkOrderOperations,

  updateTimesheetsHours,
};

function getCurrentUser() {
  return tryCatch(() => api.get<IfsCurrentUserInfo>("/user/current", null));
}

function searchWorkOrder(
  params: IfsSearchParams
): IfsApiResponse<IfsWorkOrder[]> {
  return tryCatch(() => api.get<IfsWorkOrder[]>("/wo/search", null, params));
}

function searchProject(params: IfsProjectParams): IfsApiResponse<IfsProject[]> {
  return tryCatch(() => api.get<IfsProject[]>("/project/search", null, params));
}

function getProjectReports(
  params: IfsProjectReportParams
): IfsApiResponse<IfsProjectReport[]> {
  return tryCatch(() =>
    api.get<IfsProjectReport[]>("/project/report", null, params)
  );
}

function getWorkOrderOperations(
  params: IfsWorkOrderOperationParams
): IfsApiResponse<IfsWorkOrderOperation[]> {
  return tryCatch(() =>
    api.get<IfsWorkOrderOperation[]>("/wo/operation", null, params)
  );
}

function updateTimesheetsHours(
  params: IfsUpdateTimesheetHoursParams
): IfsApiResponse<IfsTimesheetHours[]> {
  return tryCatch(() =>
    api.put("/timesheet/hours", {
      codec: t.array(IfsTimesheetHours),
      dataCodec: IfsUpdateTimesheetHoursParams,
      data: params,
    })
  );
}

function getTimesheetsByHours(
  params: IfsTimesheetParams
): IfsApiResponse<IfsTimesheetHours[]> {
  return tryCatch(() => {
    const response = api.get<IfsTimesheetHours[]>(
      "/timesheet/hours",
      t.array(IfsTimesheetHours),
      params
    );

    return response;
  });
}

function getAttendanceHours(
  params: IfsAttendanceHoursParams
): IfsApiResponse<IfsAttendanceHours[]> {
  return tryCatch(() =>
    api.get<IfsAttendanceHours[]>(
      "/attendance/hours",
      t.array(IfsAttendanceHours),
      params
    )
  );
}

function getEmployeeDayInfo(
  params: IfsEmployeeDayInfoParams
): IfsApiResponse<IfsEmployeeDayInfo[]> {
  return tryCatch(() =>
    api.get<IfsEmployeeDayInfo[]>(
      "/employee/day",
      t.array(IfsEmployeeDayInfo),
      params
    )
  );
}

function tryCatch<T>(promise: () => Promise<T>): IfsApiResponse<T> {
  return TE.tryCatch(promise, onRejected);
}

function onRejected(reason: unknown): IfsApiResponseError {
  return pipe(
    resolveClientApiResponseError<IfsApiResponseError>(reason),
    E.getOrElse((e) => {
      return {
        code: "IFS",
        ...e,
      };
    })
  );
}
