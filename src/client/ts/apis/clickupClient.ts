import { createClientApi } from "./clientApi";
import { ClickupConfig } from "../../../config";
import {
  ClickApiResponse,
  ClickApiResponseError,
  ClickApiTasksByCustomFieldParams,
  ClickApiTimesheetOptions,
  ClickApi,
  ClickTask,
  ClickTimesheet,
  TE,
  ClickUser,
  ClickTeam,
  ClickSpace,
  ClickApiUpdateExternalIdsParams,
  ClickTimesheetHours,
  ClickApiUpdateTaskParams,
  ClickApiSearchTasksParams,
  ClickUpdateTimesheetParams,
  ClickUpdateTimesheetResult,
  ClickApiAuthorizeParams,
  ClickApiAuthorizeResult,
  ClickApiLoginParams,
  ClickApiLoginResult,
  ClickApiRefreshResult,
  ApiDynamicConfig,
} from "../../../lib/types";
import { E, pipe, T, t } from "../../../lib/utils";
import { resolveClientApiResponseError } from ".";
import { getAuthToken, getAuthUserInfo, getClickUserInfo } from "../utils";
import { AxiosRequestConfig } from "axios";
import { ApiError } from "../../../lib/Errors";

const HEADER_WEB_AUTHORIZATION = "Web-Authorization";

const api = createClientApi({
  prefixUrl: `/click`,
  headers: {
    "Content-Type": "application/json",
  },
  dynamic: pipe(
    getAuthToken("Click"),
    T.chain((token) => {
      if (!token) {
        return TE.left("Token not found");
      }

      return pipe(
        getAuthUserInfo("Click"),
        TE.map((user) => {
          return {
            token,
            webToken: user.webToken,
          };
        })
      );
    }),
    TE.map(({ token, webToken }) => ({
      headers: {
        Authorization: token || "NULL",
        [HEADER_WEB_AUTHORIZATION]: webToken || "NULL",
      },
    })),
    TE.getOrElse(() =>
      T.of({
        headers: {
          Authorization: "NULL",
          [HEADER_WEB_AUTHORIZATION]: "NULL",
        },
      })
    )
  ),
});

export const clickupClient: ClickApi = {
  oauthToken,
  login,
  refresh,
  getTimesheets,
  getTimesheetsHours,
  getTasksByCustomField,
  getTaskById,
  getCurrentUser,
  getSpaces,
  getTeams,
  updateTaskExternalIds,
  updateTask,
  searchTasks,
  updateTimesheets,
};

function oauthToken(
  params: ClickApiAuthorizeParams
): ClickApiResponse<ClickApiAuthorizeResult> {
  return tryCatch(() =>
    api.get<ClickApiAuthorizeResult>(
      "/oauth/token",
      ClickApiAuthorizeResult,
      params
    )
  );
}

function login(
  params: ClickApiLoginParams
): ClickApiResponse<ClickApiLoginResult> {
  return tryCatch(() =>
    api.post<ClickApiLoginResult>("/login", {
      headers: {
        [HEADER_WEB_AUTHORIZATION]: "",
      },
      data: params,
    })
  );
}

function refresh(): ClickApiResponse<ClickApiRefreshResult> {
  const getToken = pipe(
    getClickUserInfo(),
    TE.map((user) => user.refreshToken),
    TE.getOrElse((err) => T.of<string | undefined>(undefined))
  );

  return tryCatch(async () => {
    const token = await getToken();

    if (!token) {
      throw new ApiError("Refresh token not found.");
    }

    return api.post("/refresh", {
      headers: {
        [HEADER_WEB_AUTHORIZATION]: token,
      },
    });
  });
}

function getTimesheets(
  params: ClickApiTimesheetOptions
): ClickApiResponse<ClickTimesheet[]> {
  return tryCatch(() =>
    api.get<ClickTimesheet[]>("/timesheet", t.array(ClickTimesheet), params)
  );
}

function updateTimesheets(params: ClickUpdateTimesheetParams) {
  return tryCatch(() =>
    api.put<ClickUpdateTimesheetResult[], ClickUpdateTimesheetParams>(
      "/timesheet/update",
      {
        data: params,
        dataCodec: ClickUpdateTimesheetParams,
      }
    )
  );
}

function getTimesheetsHours(
  params: ClickApiTimesheetOptions
): ClickApiResponse<ClickTimesheetHours[]> {
  return tryCatch(() =>
    api.get<ClickTimesheetHours[]>(
      "/timesheet/hours",
      t.array(ClickTimesheetHours),
      params
    )
  );
}

function updateTaskExternalIds({
  taskId,
  externalIds,
}: ClickApiUpdateExternalIdsParams): ClickApiResponse<ClickTask> {
  return tryCatch(() => {
    return api.post<ClickTask, ClickApiUpdateExternalIdsParams>("/task/field", {
      codec: ClickTask,
      data: {
        taskId,
        externalIds,
      },
    });
  });
}

function getTaskById(taskId: string): ClickApiResponse<ClickTask> {
  return tryCatch(() =>
    api.get<ClickTask>("/task/id", ClickTask, {
      taskId,
    })
  );
}

function searchTasks(
  params: ClickApiSearchTasksParams
): ClickApiResponse<ClickTask[]> {
  return tryCatch(() => {
    return api.get("/task/search", null, params);
  });
}

function getTasksByCustomField(
  params: ClickApiTasksByCustomFieldParams
): ClickApiResponse<ClickTask[]> {
  return tryCatch(() =>
    api.get<ClickTask[]>("/task", t.array(ClickTask), params)
  );
}

function updateTask(
  params: ClickApiUpdateTaskParams
): ClickApiResponse<boolean> {
  return tryCatch(() =>
    api.put("/task", {
      data: params,
    })
  );
}

function getCurrentUser(): ClickApiResponse<ClickUser> {
  return tryCatch(() => api.get<ClickUser>("/user/current", ClickUser));
}

function getTeams(): ClickApiResponse<ClickTeam[]> {
  return tryCatch(() => api.get<ClickTeam[]>("/team", t.array(ClickTeam)));
}

function getSpaces(teamId: number): ClickApiResponse<ClickSpace[]> {
  return tryCatch(() =>
    api.get<ClickSpace[]>(`/team/${teamId}/space`, t.array(ClickSpace))
  );
}

function tryCatch<T>(promise: () => Promise<T>): ClickApiResponse<T> {
  return TE.tryCatch(promise, onRejected);
}

function onRejected(error: unknown): ClickApiResponseError {
  return pipe(
    resolveClientApiResponseError<ClickApiResponseError>(error),
    E.getOrElse((error) => ({
      ...error,
      code: "Click",
    }))
  );
}
