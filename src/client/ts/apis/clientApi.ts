import axios, { AxiosResponse } from "axios";
import { AxiosRequestHeaders, AxiosInstance } from "axios";
import { ApiStatusCodes, SERVER_HOST, SERVER_PORT } from "../../../config";
import { ApiError, ApiStatusError } from "../../../lib/Errors";
import {
  ApiData,
  ApiDynamicConfig,
  ApiHeaders,
  ApiMethod,
  ApiOptions,
  ApiParams,
  ApiResponseError,
  BasicApiResponseError,
  Either,
  Task,
  TaskEither,
  TE,
} from "../../../lib/types";
import {
  C,
  camelCaseToUnderscore,
  resolveAxiosErrorResponse,
  t,
  TypeC,
} from "../../../lib/utils";
import { E, pipe } from "../../../lib/utils/fptsUtil";

interface Options {
  prefixUrl: string;
  headers?: ApiHeaders;
  dynamic?: ApiDynamicConfig;
  //   transformPath?: (path: string) => string;
}

type ApiResponse<T> = Promise<T>;

interface Api {
  get<T>(path: string, codec: C | null, params?: ApiParams): ApiResponse<T>;
  post<T, D = any>(path: string, options: ApiOptions<D>): ApiResponse<T>;
  put<T, D = any>(path: string, options: ApiOptions<D>): ApiResponse<T>;
}

export function createClientApi({ prefixUrl, headers, dynamic }: Options): Api {
  const api = axios.create({
    baseURL: `http://${SERVER_HOST}:${SERVER_PORT}${prefixUrl}`,
    headers: headers,
  });

  const response = getResponse(api, dynamic);

  return {
    get: <T>(path: string, codec: C, params?: ApiParams) =>
      response<T>("GET", path, params, codec),
    post: <T, D>(
      path: string,
      { params, data, headers: h, codec, dataCodec }: ApiOptions<D>
    ) => response<T>("POST", path, params, codec, h, data, dataCodec),
    put: <T, D>(
      path: string,
      { params, data, codec, headers: h, dataCodec }: ApiOptions<D>
    ) => response<T>("PUT", path, params, codec, h, data, dataCodec),
  };
}

interface ClientError {
  message: string;
  status: number;
  reason: any;
}

export function resolveClientApiResponseError<T extends ApiResponseError>(
  e: any
): Either<BasicApiResponseError, T> {
  return pipe(
    resolveAxiosErrorResponse<T>(e),
    E.chain((result) => {
      if (result.kind === "NotFound") {
        return E.left({
          statusCode: ApiStatusCodes.CLIENT_API_NOT_AVAILABLE,
          message: `Server API '${result.hostname}' is not available.`,
        });
      }

      if (result.kind === "Status") {
        return E.left({
          statusCode: result.status,
          message: result.statusText,
        });
      }

      if (result.kind === "AxiosCode") {
        return E.left({
          statusCode: ApiStatusCodes.CLIENT_API_AXIOS_CODE,
          message: `${result.code}: ${result.message}`,
        });
      }

      return E.right(result.data);
    }),
    E.mapLeft((reason) => {
      if (reason instanceof ApiStatusError) {
        return {
          statusCode: reason.status,
          message: reason.message,
          error: reason,
        };
      } else if (reason instanceof Error) {
        return {
          statusCode: 500,
          message: reason.message,
          error: reason,
        };
      } else {
        return {
          statusCode: reason.statusCode || 500,
          message: reason.message || "General error",
          error: ApiError.fromUnknown(reason),
        };
      }
    })
  );
}

interface ResponseError {
  status: number;
  message: string;
}

function getResponse(api: AxiosInstance, dynamic?: ApiDynamicConfig) {
  return async <T, D = ApiData>(
    method: ApiMethod,
    path: string,
    params?: ApiParams,
    codec?: TypeC<T>,
    headers?: ApiHeaders,
    data?: D,
    dataCodec?: TypeC<D>
  ): ApiResponse<T> => {
    if (params && typeof params === "string") {
      path += `?${encodeURI(params)}`;
      params = undefined;
    }

    const task: TaskEither<ResponseError, T> = TE.tryCatch(
      async () => {
        const { headers: dynHeaders } = (dynamic && (await dynamic())) || {};

        headers = {
          ...dynHeaders,
          ...headers,
        };

        const response: AxiosResponse<T> = await api.request<T>({
          method,
          headers,
          params: transformParams(method, params),
          url: path,
          data: data && dataCodec ? dataCodec.encode(data) : data,
        });

        const responseData: T = response.data;

        if (!codec) {
          return responseData;
        }

        const validation: t.Validation<T> = codec.decode(responseData);

        switch (validation._tag) {
          case "Left":
            const message: string = validation.left
              .map((x) => x.message)
              .filter((x) => x)
              .join("\n");
            throw new ApiStatusError(520, `Validation: ${path}\n` + message);

          case "Right":
            return validation.right;
        }
      },
      (reason: any): any => {
        if (reason instanceof Error) {
          return reason;
        }

        return {
          message: (reason && reason.message) || "Api call failed.",
          status: (reason && reason.status) || 500,
        };
      }
    );

    const result = await task();

    switch (result._tag) {
      case "Left":
        if (result.left instanceof Error) {
          throw result.left;
        }
        throw new ApiStatusError(result.left.status, result.left.message);
      case "Right":
        return result.right;
    }
  };
}

function transformParams(
  method: ApiMethod,
  params?: ApiParams
): ApiParams | undefined {
  if (!params || typeof params === "string") return params;

  return Object.keys(params).reduce<Record<string, any>>((result, key) => {
    result[camelCaseToUnderscore(key)] =
      method === "GET"
        ? encodeURI(parseValue(params[key]))
        : parseValue(params[key]);
    return result;
  }, {});
}

function parseValue(value: any) {
  if (value instanceof Date) {
    return value.toJSON();
  }

  return value;
}

export type ApiTransformation<T> = {
  [P in keyof T]: (itm: T[P]) => T[P];
};

export function DateApiTransformation(d: Date) {
  return new Date(d);
}

export class DecodeError extends Error {
  name = "DecodeError";
}

export function decode<T>(codec: C): (json: unknown) => T {
  return (json) => {
    return pipe(
      codec.decode(json),
      E.fold(
        (error) => error.join("\n"),
        (data) => data
      )
    );
  };
}
