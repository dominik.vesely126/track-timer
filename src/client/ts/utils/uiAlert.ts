import { ElMessage, ElMessageBox } from "element-plus";

interface AlertProps {
  message: string;
  title?: string;
}

export function uiSingleAlert(message: string, title?: string) {
  showSingleMessage({
    message,
    title,
  });
}

type NotifyType = "error" | "success" | "warning" | "info";

export function notify(message: string, type: NotifyType) {
  ElMessage({
    message,
    type,
  });
}

const alertMessages = (() => {
  const messages: { key: string; data: AlertProps }[] = [];
  const keys = new Set<string>();

  function getKey({ title, message }: AlertProps) {
    return `${title ?? ""}${message ?? ""}`;
  }

  return {
    push(data: AlertProps): boolean {
      const key = getKey(data);
      console.log("push", key);
      if (key.length && !keys.has(key)) {
        keys.add(key);
        messages.push({
          key,
          data,
        });
        return true;
      }

      return false;
    },
    pop(): AlertProps | undefined {
      if (!messages.length) {
        return undefined;
      }

      const { key, data } = messages.shift()!;
      console.log("pop", key, data);
      keys.delete(key);
      return data;
    },
  };
})();
let currentData: AlertProps | null = null;

function showSingleMessage(props: AlertProps) {
  if (!currentData) {
    currentData = props;
    ElMessageBox.alert(props.message, props.title).finally(() => {
      currentData = null;
      const data = alertMessages.pop();
      if (data) {
        showSingleMessage(data);
      }
    });
  } else {
    alertMessages.push(props);
  }
}

// const initInstance = (): AlertComponentInstance => {
//   const container = document.createElement("div");
//   const vnode = h(
//     messageBox(),
//     defineComponent({
//       data: () => {
//         return alertData;
//       },
//       render: function () {
//         return h("span", this.message);
//       },
//     })
//   );

//   render(vnode, container);

//   document.body.appendChild(container.firstElementChild!);
//   return vnode.component?.proxy as AlertComponentInstance;
// };

// function singleAlert(msg: string, title?: string): void {
//   const key = title ?? "" + msg ?? "";

//   if (!key.length) {
//     return;
//   }

//   if (alertMessages[key]) {
//     return;
//   }

//   if (!alertContent) {
//     // alert neexistuje
//     alertContent = makeAlertContent(msg);
//     ElMessageBox.alert(h(alertContent), title).finally(
//       () => (alertContent = null)
//     );
//     return;
//   }

//   alertContent.message = msg;
// }

// interface AlertContentProps {
//   message: string;
// }

// function makeAlertContent(msg: string): AlertContentComponent {
//   return h(
//     defineComponent<AlertContentProps>({
//       msg,
//     })
//   );
// }
