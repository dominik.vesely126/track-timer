import {
  CacheKeys,
  getFromCache,
  getFromCacheIO,
  saveToCache,
  saveToCacheIO,
  tokenCacheKey,
} from ".";
import {
  ClickupConfig,
  CLICK_OAUTH_CLIENT_ID,
  CLICK_OAUTH_URI,
} from "../../../config";
import {
  ApiKind,
  ApiResponse,
  ApiResponseError,
  ApiResponseErrorK,
  ClickApiAuthorizeResult,
  ClickApiLoginResult,
  ClickUser,
  IfsCurrentUserInfo,
  RMUser,
  Task,
  TaskEither,
  TE,
} from "../../../lib/types";
import { A, createUrl, E, Ord, pipe, R, T } from "../../../lib/utils";
import { clickupClient } from "../apis";
import { ifsClient } from "../apis/ifsClient";
import { redmineClient } from "../apis/redmineClient";
import { openPoupWindow } from "./windowUtil";

export interface IfsUserInfo extends IfsCurrentUserInfo {
  kind: "IFS";
}

export interface ClickUserInfo extends ClickUser {
  kind: "Click";
  refreshToken?: ClickApiLoginResult["refresh_token"];
  webToken?: ClickApiLoginResult["token"];
  username?: ClickApiLoginResult["user"]["username"];
}

export interface RMUserInfo extends RMUser {
  kind: "RM";
}

export function isLogged(): Task<boolean> {
  return pipe(
    getClickUserInfo(),
    T.map((e) => E.isRight(e))
  );
}

export function logout() {
  localStorage.clear();
  redirect("/login");
}

export function redirect(path: string) {
  if (window.location.pathname !== path) {
    window.location.href = createClientUrl(path);
  }
}

function createClientUrl(path: string): string {
  return [window.location.protocol + "//", window.location.host, path].join("");
}

export function saveAuthToken(kind: ApiKind, token: string) {
  return saveToCacheIO(tokenCacheKey(kind), token);
}

export function getAuthToken(kind: ApiKind) {
  return getFromCacheIO<string>(tokenCacheKey(kind));
}

// export function getIfsXsrfToken(kind)

export function getIfsUserInfo(): TaskEither<string, IfsUserInfo> {
  return handleUndefined(getUserInfo("IFS"), () => "IFS user not found.");
}

export function getClickUserInfo(): TaskEither<string, ClickUserInfo> {
  return handleUndefined(getUserInfo("Click"), () => "Clickup user not found.");
}

export const CLICK_API_OAUTH_POPUP_NAME = "ClickApiOAuth";

export function authClickApi(): TaskEither<string, string> {
  return pipe(
    openPoupWindow({
      url: CLICK_OAUTH_URI,
      name: CLICK_API_OAUTH_POPUP_NAME,
    }),
    TE.mapLeft(({ blocked, closed }) => {
      const str = "Authentication to ClickUp API failed: ";
      return blocked
        ? str + "Popup window was blocked."
        : str + "Popup window was closed.";
    }),
    TE.chain(() => {
      return pipe(
        getAuthToken("Click"),
        T.map((success) => {
          return success
            ? E.right(success)
            : E.left("Click API access token not found.");
        })
      );
    })
  );
}

export function getRedmineUserInfo(): TaskEither<string, RMUserInfo> {
  return handleUndefined(getUserInfo("RM"), () => "Redmine user not found.");
}

export type AuthUserInfoKind = AuthUserInfo["kind"];

export type User<U extends AuthUserInfo["kind"]> = U extends "Click"
  ? ClickUserInfo
  : U extends "RM"
  ? RMUserInfo
  : U extends "IFS"
  ? IfsUserInfo
  : never;

export function getAuthUserInfo<U extends AuthUserInfoKind>(
  kind: U
): TaskEither<string, User<U>> {
  return handleUndefined(getUserInfo(kind), () => "User not found.");
}

function handleUndefined<T = AuthUserInfo>(
  t: Task<T | undefined>,
  onUndefined: () => string
): TaskEither<string, T> {
  return pipe(
    t,
    T.map((x) => (x ? E.right(x) : E.left(onUndefined())))
  );
}

export function getUserInfo<T = AuthUserInfo>(kind: AuthUserInfoKind) {
  return getFromCache<T>(
    {
      Click: CacheKeys.CLICK_USER,
      RM: CacheKeys.RM_USER,
      IFS: CacheKeys.IFS_USER,
    }[kind]
  );
}

export type AuthUserInfo = RMUserInfo | IfsUserInfo | ClickUserInfo;

function saveAuthInfo(user: AuthUserInfo): Task<boolean> {
  switch (user.kind) {
    case "IFS":
      return saveToCache(CacheKeys.IFS_USER, user);

    case "RM":
      return saveToCache(CacheKeys.RM_USER, user);

    case "Click":
      return saveToCache(CacheKeys.CLICK_USER, user);
  }
}

export function updateAuthInfo<
  K extends AuthUserInfoKind,
  U extends Partial<User<K>> & { kind: K }
>(kind: K, clb: (u: U) => User<K>): TaskEither<string, User<K>> {
  return pipe(
    pipe(
      getAuthUserInfo(kind),
      TE.chain((user) => TE.right(user as unknown as U)),
      TE.getOrElse(() => {
        return T.of({
          kind,
        } as U);
      })
    ),
    T.chain((user) => {
      const newUser = clb(user);
      return pipe(
        saveAuthInfo(newUser),
        T.map((success) => {
          return success ? E.right(newUser) : E.left("Save user info failed.");
        })
      );
    })
  );
}

export function loadAuthInfo<
  K extends ApiKind,
  U extends User<K>,
  E extends ApiResponseErrorK<K>
>(kind: K): ApiResponse<U, E> {
  const methods = {
    Click: pipe(
      clickupClient.getCurrentUser(),
      TE.map((x) => ({
        ...x,
        kind: "Click" as "Click",
      }))
    ),
    RM: pipe(
      redmineClient.getCurrentUser(),
      TE.map((x) => ({
        ...x,
        kind: "RM" as "RM",
      }))
    ),
    IFS: pipe(
      ifsClient.getCurrentUser(),
      TE.map((x) => ({
        ...x,
        kind: "IFS" as "IFS",
      }))
    ),
  };

  return methods[kind] as ApiResponse<U, E>;
}
