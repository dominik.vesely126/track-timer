import { TaskEither, TE } from "../../../lib/types";
import { objectMap } from "../../../lib/utils";

type WindowFeatures = Partial<{
  popup: boolean;
  left: number;
  top: number;
  width: number;
  height: number;
}> &
  Record<string, any>;

interface WindowProps {
  url: string;
  name: string;
  features?: WindowFeatures;
}

export function closePopupWindow(name: string) {
  if (window.opener) {
    dispatchCloseWindow(window.opener);
    window.close();
  }
}

function dispatchCloseWindow(wnd: Window) {
  const customEvent = new CustomEvent("closeWindow_" + name, { detail: {} });
  wnd.dispatchEvent(customEvent);
}

export function openPoupWindow({ url, name, features }: WindowProps) {
  return openWindow({
    url,
    name,
    features: {
      ...features,
      popup: true,
      width: 750,
      height: 550,
    },
  });
}

interface OpenWindowError {
  blocked: boolean;
  closed: boolean;
}

function openWindow({
  url,
  name,
  features,
}: WindowProps): TaskEither<OpenWindowError, void> {
  return TE.tryCatch(
    () =>
      new Promise((resolve, reject) => {
        const ref = window.open(url, name, concatFeatures(features));

        if (!ref) {
          reject({
            blocked: true,
            closed: true,
          });
          return;
        }

        const i = setInterval(() => {
          if (!ref.closed) return;
          reject({
            blocked: false,
            closed: true,
          });
          clearInterval(i);
        }, 500);

        if (features?.popup) {
          window.addEventListener(
            "closeWindow_" + name,
            () => {
              resolve();
              clearInterval(i);
            },
            {
              once: true,
            }
          );
        }
      }),
    (err) => err as OpenWindowError
  );
}

function concatFeatures(features?: WindowFeatures): string | undefined {
  if (features === null || features === undefined) {
    return undefined;
  }

  return objectMap(features, (val, key) => {
    if (val === undefined || val === null) return null;

    return `${key}=${parseFeatureValue(val)}`;
  })
    .filter((x) => x !== null)
    .join(",");
}

function parseFeatureValue(val: any): any {
  switch (typeof val) {
    case "boolean":
      return val ? 1 : 0;

    default:
      return val;
  }
}
