import { ApiKind, IO, Task, TaskEither, TE } from "../../../lib/types";
import {
  A,
  arrayToObject,
  fpString,
  Ord,
  pipe,
  R,
  Semigroup,
  T,
} from "../../../lib/utils";

export const CacheKeys = {
  CLICK_TOKEN: "ClickToken",
  CLICK_USER: "ClickUser",
  IFS_TOKEN: "IfsToken",
  IFS_USER: "IfsUser",
  RM_TOKEN: "RmToken",
  RM_USER: "RmUser",
  RM_ACTIVITY: "RmActivity",
};

export function tokenCacheKey(kind: ApiKind): IO<string> {
  return () =>
    ({
      RM: CacheKeys.RM_TOKEN,
      IFS: CacheKeys.IFS_TOKEN,
      Click: CacheKeys.CLICK_TOKEN,
    }[kind]);
}

export function saveToCacheIO<T>(key: IO<string>, item: T): Task<boolean> {
  return pipe(
    T.fromIO(key),
    T.chain((x) => saveToCache(x, item))
  );
}

export function saveToCache<T>(key: string, item: T): Task<boolean> {
  return () =>
    new Promise<boolean>((resolve) => {
      localStorage.setItem(
        key,
        item === null || item === undefined ? "" : JSON.stringify(item)
      );
      resolve(true);
    }).catch(() => false);
}

export function getFromCacheIO<T>(key: IO<string>): Task<T | undefined> {
  return pipe(
    T.fromIO(key),
    T.chain((x) => getFromCache(x))
  );
}

export function getFromCache<T>(key: string): Task<T | undefined> {
  return () => {
    const cached = localStorage.getItem(key);
    if (cached !== null && cached !== "") {
      return Promise.resolve(JSON.parse(cached));
    }

    return Promise.resolve(undefined);
  };
}

export function getOrSaveFromCache<T>(key: string, get: () => T): Task<T> {
  return pipe(
    getFromCache<T>(key),
    T.chain((item) => {
      if (!item) {
        item = get();
        return pipe(
          saveToCache(key, item),
          T.map((x) => item!)
        );
      }
      return T.of(item);
    })
  );
}

export function saveRecordsToCache<T extends Record<string, any>>(
  r: T
): TaskEither<Record<keyof T, boolean>, T> {
  return pipe(
    r,
    R.mapWithIndex((k, v) => saveToCache(k, v)),
    R.collect(fpString.Ord)((k: keyof T, v) =>
      pipe(
        v,
        T.map((i) => ({
          key: k,
          item: i,
        }))
      )
    ),
    T.sequenceArray,
    T.chain((cacheResults) => {
      if (cacheResults.some((x) => !x.item)) {
        return TE.left(
          arrayToObject(
            cacheResults,
            (x) => x.key,
            (x) => x.item
          )
        );
      } else {
        return TE.right(r);
      }
    })
  );
}
