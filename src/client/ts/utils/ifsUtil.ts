import { CacheKeys, getFromCache } from ".";
import { TaskEither } from "../../../lib/types";
import { E, pipe, T } from "../../../lib/utils";

interface IfsAuthInfo {
  companyId: string;
  empNo: number;
}

export function getIfsAuthInfo(): TaskEither<string, IfsAuthInfo> {
  return pipe(
    getFromCache<IfsAuthInfo>(CacheKeys.IFS_USER),
    T.map((u) => (u ? E.right(u) : E.left("IFS user data not loaded.")))
  );
}
