import { ElMessageBox, ElMessageBoxOptions, ElScrollbar } from "element-plus";
import {
  h as vueRender,
  VNode,
  DefineComponent,
  ExtractDefaultPropTypes,
  ComponentObjectPropsOptions,
  ComponentOptions,
  ComputedOptions,
  MethodOptions,
  ComponentOptionsMixin,
  EmitsOptions,
  ExtractPropTypes,
  ComponentPropsOptions,
  CreateComponentPublicInstance,
  ComponentOptionsBase,
} from "vue-demi";
import {
  AuthUserInfo,
  AuthUserInfoKind,
  getAuthToken,
  getAuthUserInfo,
  loadAuthInfo,
  uiSingleAlert,
  updateAuthInfo,
  User,
} from ".";
import { ApiStatusCodes } from "../../../config";
import {
  ApiKind,
  ApiResponse,
  ApiResponseError,
  ApiResponseErrorK,
  Task,
  TaskEither,
  TE,
} from "../../../lib/types";
import { E, pipe, T } from "../../../lib/utils";
import TokenFormVue from "../../components/TokenForm.vue";
import { clickupClient } from "../apis";

type Component<P> = DefineComponent<P, {}, any>;
type ComponentProps<P> = Readonly<ExtractPropTypes<P>>;

const h = <T>(
  component: Component<T>,
  props?: ComponentProps<T>,
  children?: {
    default: () => VNode;
  }
) => vueRender(component, props, children);

export function moveCursor(el: HTMLInputElement, pos: number) {
  el.setSelectionRange(pos, pos);
}

export function apiResolveOrDefault<T, E extends ApiResponseError>(
  task: ApiResponse<T, E>,
  defaultValue: T
) {
  return resolveOrDefault(
    pipe(task, apiResolveUnAuthorized(defaultValue)),
    defaultValue
  );
}

export function apiResolveOrAlert<T, E extends ApiResponseError>(
  task: ApiResponse<T, E>,
  defaultVal: T
) {
  return resolveOrAlert(pipe(task, apiResolveUnAuthorized(defaultVal)), () => ({
    getMsg: (e) => {
      return e.message;
    },
    getTitle: (e) => `${e.code} ${e.statusCode || ""}`,
    defaultVal,
  }));
}

export function processUserApiCall<
  T,
  K extends AuthUserInfoKind,
  E extends ApiResponseErrorK<K>
>(kind: K, api: (u: User<K>) => ApiResponse<T, E>): ApiResponse<T, E> {
  const user = getAuthUserInfo(kind);

  return pipe(
    user,
    // TE.map((user) => {}),
    TE.mapLeft(
      (x) =>
        ({
          code: kind,
          statusCode: ApiStatusCodes.CLIENT_API_USER_MISSING,
          message: x,
        } as E)
    ),
    TE.chain((u) => processApiCall(api(u)))
  );
}

export function processApiCall<T, E extends ApiResponseError>(
  api: ApiResponse<T, E>
): ApiResponse<T, E> {
  return pipe(
    api
    // TE.mapLeft((x) => x.message)
  );
}

function apiResolveUnAuthorized<T, E extends ApiResponseError>(defaultVal: T) {
  return (task: ApiResponse<T, E>): ApiResponse<T, E> => {
    const result: ApiResponse<T, E> = pipe(
      task,
      TE.orElse((error) => {
        if (error.statusCode === ApiStatusCodes.CLIENT_API_USER_MISSING) {
          return pipe(
            apiResolveUnAuthorized<AuthUserInfo | null, E>(null)(
              pipe(
                loadAuthInfo<typeof error.code, AuthUserInfo, E>(error.code),
                T.chain((either) => {
                  if (E.isLeft(either)) {
                    return T.of(E.left<E, AuthUserInfo>(either.left));
                  }

                  return pipe(
                    updateAuthInfo(either.right.kind, (u) => ({
                      ...u,
                      ...either.right,
                    })),
                    T.map(() => either)
                  );
                })
              )
            ),
            T.chain(() => apiResolveUnAuthorized<T, E>(defaultVal)(task))
          );
        }

        if (error.statusCode === 401) {
          if (error.code === "Click") {
            // try refresh token
            return pipe(
              getAuthToken("Click"),
              T.map((token) =>
                token ? E.right(token) : E.left("API token not found.")
              ),
              TE.chain(clickRefreshAccessToken),
              TE.fold(
                () => {
                  // get API token or refresh web token failed
                  return pipe(
                    showAuthorizeForm(error.code),
                    T.chain(() =>
                      apiResolveUnAuthorized<T, E>(defaultVal)(task)
                    )
                  );
                },
                // refresh access token success
                () => apiResolveUnAuthorized<T, E>(defaultVal)(task)
              )
            );
          }

          return pipe(
            showAuthorizeForm(error.code),
            T.chain(() => apiResolveUnAuthorized<T, E>(defaultVal)(task))
          );
        }
        return TE.left(error);
      })
    );

    return result;
  };
}

function clickRefreshAccessToken(): TaskEither<string, void> {
  return pipe(
    clickupClient.refresh(),
    TE.mapLeft((err) => err.clickError?.err || err.message),
    TE.chain(({ token, expiration }) => {
      return pipe(
        getAuthUserInfo("Click"),
        TE.map((user) => ({
          user,
          token,
        }))
      );
    }),
    TE.chain(({ token, user }) => {
      return updateAuthInfo("Click", (u) => {
        return {
          ...user,
          webToken: token,
        };
      });
    }),
    TE.map(() => {})
  );
}

function showAuthorizeForm(kind: ApiKind): Task<void> {
  return showInDialog(
    {
      title: `${kind} Authentizace`,
    },
    TokenFormVue,
    {
      kind,
    }
  );
}

interface DialogOptions {
  title: string;
  width?: number;
  height?: number;
}

export function showInDialog<P>(
  { title, width, height }: DialogOptions,
  component: Component<P>,
  props?: ComponentProps<P>
): Task<void> {
  return () => {
    const node: VNode = vueRender(
      ElScrollbar,
      {},
      {
        default() {
          return h(component, props);
        },
      }
    );

    const styles = {
      width: width && `${width}px`,
      height: height && `${height}px`,
      maxWidth: "100%",
      maxHeight: "100%",
    };

    return ElMessageBox.alert(node, {
      confirmButtonText: "Zavřít",
      title,
      customStyle: styles,
    })
      .then(() => {})
      .catch(() => {});
  };
}

export function resolveOrAlertMessage<T>(
  te: TaskEither<string, T>,
  defaultVal: T
) {
  return resolveOrAlert(te, () => ({
    getMsg: (e) => e,
    defaultVal,
  }));
}

async function resolveOrAlert<L, R>(
  task: TaskEither<L, R>,
  //   defaultValue: T,
  onLeft: (e: L) => {
    getTitle?: (e: L) => string;
    getMsg: (e: L) => string;
    defaultVal: R;
  }
): Promise<R> {
  return resolveOrDefault(task, (e) => {
    const { getMsg, getTitle, defaultVal } = onLeft(e);
    singleAlert(getMsg(e), getTitle && getTitle(e));
    return defaultVal;
  });
}

async function resolveOrDefault<L, R>(
  task: TaskEither<L, R>,
  onLeft: R | ((e: L) => R)
): Promise<R> {
  const result = await task();
  switch (result._tag) {
    case "Left":
      if (typeof onLeft === "function") {
        // @ts-ignore
        return onLeft(result.left);
      }
      // @ts-ignore
      return onLeft;

    case "Right":
      return result.right;
  }
}

function singleAlert(msg: string, title?: string) {
  uiSingleAlert(msg, title);
}
