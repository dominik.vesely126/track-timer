export function toObject<T, TKey extends string | number, TValue>(
  array: T[] | undefined,
  map: (itm: T) => [TKey, TValue]
): Record<TKey, TValue> {
  if (!array) {
    return {} as Record<TKey, TValue>;
  }

  return array.reduce((result, item) => {
    const [key, value] = map(item);
    result[key] = value;
    return result;
  }, {} as Record<TKey, TValue>);
}
