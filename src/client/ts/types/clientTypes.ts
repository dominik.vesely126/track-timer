import {
  ClickTask,
  ClickTimesheetHours,
  ClickUpdateTimesheetItemType,
} from "../../../lib/types";
import { DurationTime } from "../../../lib/utils";

export interface AttendanceTimesheetItem {
  date: Date;
  attendanceHours: DurationTime;
}

export type UpdateReportTimesheetItem = Omit<
  ReportTimesheetItem,
  "reportHoursOld" | "reportHoursChanged"
>;

export interface ReportTimesheetItem {
  date: Date;
  reportHours?: DurationTime;
  reportHoursOld?: DurationTime;
  reportHoursChanged: boolean;
  ifsTaskId: string | null;
}

export type UpdateRedmineTimesheetItem = Omit<
  RedmineTimesheetItem,
  "redmineHoursOld" | "redmineHoursChanged"
>;
export interface RedmineTimesheetItem {
  date: Date;
  redmineHours?: DurationTime;
  redmineHoursOld?: DurationTime;
  redmineHoursChanged: boolean;
  redmineTaskId: number | null;
}

export type TimesheetItem = Omit<ClickTimesheetHours, "task"> &
  ReportTimesheetItem &
  RedmineTimesheetItem & {
    //   attendanceHours?: DurationTime;
    taskId: string | null;
    taskName: string | null;
  };

export type TimesheetTask = Pick<ClickTask, "id" | "name"> &
  Pick<TimesheetItem, "ifsTaskId" | "redmineTaskId">;

export type TimesheetDayState = "OPENED" | "CONFIRMED" | "APPROVED";
export interface TimesheetDayItem {
  id: number | string;
  date: Date;
  reportState?: TimesheetDayState;
  attendanceHours?: DurationTime;
  totalReportHours?: DurationTime;
  totalRedmineHours?: DurationTime;
  items: TimesheetItem[];
}

export interface TimesheetDetailItem {
  id: string;
  taskId: string | undefined;
  userId: number;
  name: string | undefined;
  date: Date;
  start: DurationTime;
  end: DurationTime;
  duration: DurationTime;
  state: ClickUpdateTimesheetItemType | "idle";
}
