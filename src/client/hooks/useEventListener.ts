import { onBeforeUnmount, onMounted, Ref, isRef, watch } from "vue-demi";

type ListenerOptions = boolean | AddEventListenerOptions;

type ElementRefType<T extends HTMLElement> = Ref<T | null> | T | Window;
type ElementType<T extends HTMLElement> = T | null | Window;

interface ReturnType {
  add: () => void;
  remove: () => void;
}

// type EventMap<T> = T extends Window ? WindowEventMap : HTMLElementEventMap;
// type EventMapKeyof<T> = T extends Window
//   ? keyof WindowEventMap
//   : keyof HTMLElementEventMap;

// type EventMapKey<K> = K extends keyof WindowEventMap
//   ? WindowEventMap
//   : HTMLElementEventMap;

export function useEventListener<K extends keyof WindowEventMap>(
  el: Window,
  type: K,
  listener: (this: Window, ev: WindowEventMap[K]) => any,
  options?: ListenerOptions
): ReturnType;

export function useEventListener<
  T extends HTMLElement,
  K extends keyof HTMLElementEventMap
>(
  el: Ref<T | null> | T,
  type: K,
  listener: (this: T, ev: HTMLElementEventMap[K]) => any,
  options?: ListenerOptions
): ReturnType;

export function useEventListener<T extends HTMLElement>(
  el: ElementRefType<T>,
  type: string,
  listener: (this: any, ev: any) => any,
  options?: ListenerOptions
): ReturnType {
  const isRefElement = isRef<T | null>(el);
  const element: () => ElementType<T> = () => (isRefElement ? el.value : el);

  if (isRefElement) {
    watch(el, (newValue, oldValue) => {
      remove(oldValue);
      add(newValue);
    });
  }

  onMounted(() => {
    add(element());
  });

  onBeforeUnmount(() => {
    remove(element());
  });

  return {
    add() {
      add(element());
    },
    remove() {
      remove(element());
    },
  };

  function add<T extends HTMLElement>(el: ElementType<T>) {
    if (!el) {
      return;
    }
    remove(el);
    el.addEventListener(type, listener, options);
  }

  function remove<T extends HTMLElement>(el: ElementType<T>) {
    if (!el) {
      return;
    }

    el.removeEventListener(type, listener, options);
  }

  //   function addEventListener<K extends keyof HTMLElementEventMap>(
  //     el: Ref<HTMLElement | null>,
  //     type: K,
  //     listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
  //     options?: boolean | AddEventListenerOptions
  //   ): void {
  //     return el.addEventListener(type, listener, options);
  //   }

  //   function removeEventListener<K extends keyof HTMLElementEventMap>(
  //     el: HTMLElement,
  //     type: K,
  //     listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
  //     options?: boolean | AddEventListenerOptions
  //   ): void {
  //     return el.removeEventListener(type, listener, options);
  //   }
}
