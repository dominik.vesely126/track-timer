import { createRouter, createWebHistory } from "vue-router";
import TimesheetPageVue from "./pages/TimesheetPage.vue";
import LoginPageVue from "./pages/LoginPage.vue";
import ClickApiOauthVue from "./pages/ClickApiOauth.vue";
import ClickTimesheetsVue from "./pages/ClickTimesheets.vue";

export const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      component: TimesheetPageVue,
    },
    {
      path: "/login",
      component: LoginPageVue,
    },
    {
      path: "/click/oauth/token",
      component: ClickApiOauthVue,
    },
    {
      path: "/click/detail/:teamId/:day",
      component: ClickTimesheetsVue,
      props: true,
    },
  ],
});
