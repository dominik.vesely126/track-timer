import { createApp } from "vue";
import ElementPlus from "element-plus";
import { Check, Close } from "@element-plus/icons";
import locale from "element-plus/lib/locale/lang/cs";
import "element-plus/theme-chalk/index.css";
import "./styles/main.scss";
import App from "./App.vue";

// import dayjs from "dayjs";
import "dayjs/locale/cs";
import { router } from "./router";

const app = createApp(App);

// app.config.compilerOptions.isCustomElement = (tag) => ["close"].includes(tag);

// dayjs.locale("cs");

app.use(ElementPlus, {
  locale,
});

app.use(router);

// app.use(UIAlert);

app.component("check", Check);
app.component("close", Close);

app.mount("#app");
