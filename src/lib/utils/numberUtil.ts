export function getNumber<T>(val: any, defaultValue: T) {
  return getNumberCondition(val, (num) => true, defaultValue);
}

export function getNumberGt<T>(val: any, greaterThan: number, defaultValue: T) {
  return getNumberCondition(val, (num) => num > greaterThan, defaultValue);
}

export function getPositiveNumber<T>(val: any, defaultValue: T) {
  return getNumberCondition(val, (num) => num > 0, defaultValue);
}

/**
 * Vraci TRUE pokud ma cislo max. pocet cisel
 * za desetinnou carkou.
 * @param value
 * @param maxDigits
 */
export function hasNumberMaxFracDigits(
  value: number,
  maxDigits: number
): boolean {
  maxDigits = Math.floor(maxDigits);

  if (maxDigits < 0) {
    throw new Error("MaxDigits cannot be less than zero.");
  }
  const multiplier = Math.pow(10, maxDigits);
  return value === Math.round(value * multiplier) / multiplier;
}

function getNumberCondition<T>(
  val: any,
  condition: (num: number) => boolean,
  defaultValue: T
): number | T {
  const num = Number(val);
  return isNaN(num) || !condition(num) ? defaultValue : num;
}
