import axios, { AxiosError } from "axios";
import { E } from "./fptsUtil";
import { Either } from "../types";

interface ApiErrorStatusAndDataResponse<T> {
  kind: "Data";
  status: number;
  statusText: string;
  contentType: string;
  data: T;
}

interface ApiErrorStatusResponse {
  kind: "Status";
  status: number;
  statusText: string;
  contentType: string;
}
interface ApiErrorNotFoundResponse {
  kind: "NotFound";
  hostname: string;
}

interface ApiErrorAxiosCodeResponse {
  kind: "AxiosCode";
  code: string;
  message: string;
}

type ApiErrorResponse<T> =
  | ApiErrorStatusAndDataResponse<T>
  | ApiErrorNotFoundResponse
  | ApiErrorStatusResponse
  | ApiErrorAxiosCodeResponse;

export function resolveAxiosErrorResponse<T>(
  e: any
): Either<any, ApiErrorResponse<T>> {
  if (axios.isAxiosError(e)) {
    if (e.response) {
      if (!e.response.data) {
        return E.left({
          kind: "Status",
          status: e.response.status,
          contentType: e.response.headers["content-type"],
          statusText: e.response.statusText,
        });
      } else {
        return E.right({
          kind: "Data",
          status: e.response.status,
          contentType: e.response.headers["content-type"],
          statusText: e.response.statusText,
          data: e.response.data,
        });
      }
    } else if (e.code === "ENOTFOUND") {
      return E.right({
        kind: "NotFound",
        hostname: (e as any).hostname,
      });
    } else if (e.code) {
      return E.right({
        kind: "AxiosCode",
        code: e.code,
        message: e.message,
      });
    } else if (e.message === "Network Error") {
      return E.right({
        kind: "NotFound",
        hostname: e.config.baseURL || "unknown URL",
      });
    } else {
      return E.left(e);
    }
  } else {
    return E.left(e);
  }
}

export function parseCookie(cookies: Record<string, string>) {
  return Object.keys(cookies)
    .map((name) => `${name}=${cookies[name]}`)
    .join(";");
}
