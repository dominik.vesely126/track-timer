import {
  format,
  startOfDay as startDay,
  endOfDay as endDay,
  isSameWeek,
  startOfWeek,
  addWeeks,
  addDays,
  isDate,
  add,
} from "date-fns";
import endOfWeek from "date-fns/endOfWeek";
import { DurationTime, minAndMax } from ".";
import { E } from "./fptsUtil";
import { t } from "./iotsUtil";
import { formatWithOptions } from "date-fns/fp";
import { cs } from "date-fns/locale";

export type DateRange = {
  begin: Date;
  end: Date;
};

const locale: Locale = cs;

const weekSettings: {
  //   locale?: Locale;
  weekStartsOn?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
} = {
  weekStartsOn: 1,
};

export interface DateFromSqlStringC extends t.Type<Date, string, unknown> {}

export const DateFromSqlString: DateFromSqlStringC = new t.Type<
  Date,
  string,
  unknown
>(
  "DateFromSqlStringC",
  (u): u is Date => u instanceof Date,
  (u, c) =>
    E.either.chain(t.string.validate(u, c), (s) => {
      try {
        return t.success(new Date(s));
      } catch {
        return t.failure(u, c);
      }
    }),
  (a) => formatDateSql(a)
);

export function formatDateSql(date: Date): string {
  return format(date, "yyyy-MM-dd");
}

export function formatWeekDayShortcut(date: Date): string {
  return formatWithOptions(
    {
      locale,
    },
    "eeeeee",
    date
  );
}

export function formatDate(date: Date): string {
  return format(date, "d. M. yyyy");
}

export function formatDateTime(date: Date): string {
  return format(date, "dd. MM. yyyy HH:mm");
}

export function dateStartOfDayUtc(date: Date): Date {
  const start = dateStartOfDay(date);
  return dateToUtc(start);
}

export function dateEndOfDayUtc(date: Date): Date {
  const end = dateEndOfDay(date);
  return dateToUtc(end);
}

export function dateStartOfDay(date: Date): Date {
  return startDay(date);
}

export function dateEndOfDay(date: Date): Date {
  return endDay(date);
}

export function isDateSameWeek(d1: Date, d2: Date): boolean {
  return isSameWeek(d1, d2, weekSettings);
}

export function dateStartOfWeek(date: Date): Date {
  return startOfWeek(date, weekSettings);
}

export function dateEndOfWeek(date: Date): Date {
  return endOfWeek(date, weekSettings);
}

export function dateWeekRange(date: Date): DateRange {
  return {
    begin: dateStartOfWeek(date),
    end: dateEndOfWeek(date),
  };
}

export function* dateDaysIter({ begin, end }: DateRange): Iterable<Date> {
  let d = begin;
  while (dateCompare(d, end) <= 0) {
    yield dateStartOfDay(d);
    d = dateAddDays(d, 1);
  }
}

export function dateAddDays(date: Date, amount: number): Date {
  return addDays(date, amount);
}

export function dateAddWeeks(date: Date, amount: number): Date {
  return addWeeks(date, amount);
}

export function dateTimeCompare(d1: Date, d2: Date): number {
  return d1.getTime() - d2.getTime();
}

export function dateTimeIsSame(d1: Date, d2: Date): boolean {
  return dateTimeCompare(d1, d2) === 0;
}

export function dateIsSame(d1: Date, d2: Date): boolean {
  return dateCompare(d1, d2) === 0;
}

export function dateCompare(d1: Date, d2: Date): number {
  return formatDateSql(d1).localeCompare(formatDateSql(d2));
}

function dateToUtc(date: Date) {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000);
}

export function dateMinAndMax<T>(
  iter: Iterable<T>,
  prop: (i: T) => Date
): [Date | undefined, Date | undefined] {
  return minAndMax(iter, prop, (a, b) => dateTimeCompare(a, b));
}

export function getDateTimeRange<T>(
  items: Iterable<T>,
  getDate: (i: T) => Date
): DateRange | undefined {
  const [begin, end] = dateMinAndMax(items, getDate);

  if (!(isDate(begin) && isDate(end))) {
    return undefined;
  }

  return {
    begin: begin!,
    end: end!,
  };
}

export function dateInInterval(d: Date, { begin, end }: DateRange): boolean {
  return dateCompare(d, begin) >= 0 && dateCompare(d, end) <= 0;
}

export function dateAddDuration(date: Date, time: DurationTime): Date {
  return add(date, time);
}
