export function iterFind<T>(
  iter: Iterable<T>,
  predicate: (itm: T) => boolean
): T | undefined {
  for (const i of iter) {
    if (predicate(i)) {
      return i;
    }
  }

  return undefined;
}

export function iterSome<T>(
  iter: Iterable<T>,
  clb: (i: T) => boolean
): boolean {
  for (const itm of iter) {
    if (clb(itm)) {
      return true;
    }
  }

  return false;
}

export function iterMap<T, R>(iter: Iterable<T>, clb: (i: T) => R): R[] {
  return iterReduce<T, R[]>(
    iter,
    (prev, cur) => {
      prev.push(clb(cur));
      return prev;
    },
    []
  );
}

export function iterReduce<T, R>(
  iter: Iterable<T>,
  clb: (prev: R, current: T) => R,
  defaultVal: R
): R {
  let result: R = defaultVal;
  for (const itm of iter) {
    result = clb(result, itm);
  }
  return result;
}

export function minAndMax<T, C>(
  arr: Iterable<T>,
  transform: (i: T) => C,
  comparator: (a: C, b: C) => number,
  init?: [C, C]
): [C | undefined, C | undefined] {
  if (!arr) {
    return init ?? [undefined, undefined];
  }

  let [min, max] = init ?? [undefined, undefined];

  for (const i of arr) {
    const t = transform(i);
    if (min === undefined || comparator(t, min) < 0) {
      min = t;
    }

    if (max === undefined || comparator(t, max) > 0) {
      max = t;
    }
  }

  return [min, max];
}
