import { pipe } from "fp-ts/lib/function";
import { getNumber, hasNumberMaxFracDigits } from ".";
import { ParseError } from "../Errors/ParseError";
import { E, T } from "./fptsUtil";
import { t } from "./iotsUtil";

interface DurationTimeParams {
  hours?: number;
  minutes?: number;
  seconds?: number;
  ms?: number;
}

export class DurationTime {
  readonly hours: number;
  readonly minutes: number;
  readonly seconds: number;
  readonly ms: number;
  readonly _tag: "DurationTime";
  readonly _isNegative: boolean;
  _isNaN: boolean;

  static create(
    hours: number,
    minutes?: number,
    seconds?: number,
    ms?: number
  ) {
    return new DurationTime({
      hours,
      minutes,
      seconds,
      ms,
    });
  }

  static none() {
    const i = new DurationTime({});
    i._isNaN = true;
    return i;
  }

  static zero() {
    return new DurationTime({});
  }

  constructor({ hours, minutes, seconds, ms }: DurationTimeParams) {
    this._tag = "DurationTime";
    this._isNaN = false;
    let hh = getNumber(hours, 0);
    let mm = getNumber(minutes, 0);
    let ss = getNumber(seconds, 0);
    let mss = getNumber(ms, 0);

    const millis = hh * 3600000 + mm * 60000 + ss * 1000 + mss;
    const dur = durationFromMillis(millis);

    this._isNegative =
      dur.hours < 0 || dur.minutes < 0 || dur.seconds < 0 || dur.seconds < 0;

    this.hours = dur.hours;
    this.minutes = dur.minutes;
    this.seconds = dur.seconds;
    this.ms = dur.ms;
  }

  isNegative(): boolean {
    return this._isNegative;
  }
}

export interface DurationFromStringC
  extends t.Type<DurationTime, string, unknown> {}

export type DurationFromNumberC = t.Type<DurationTime, number, unknown>;

export const DurationFromString: DurationFromStringC = new t.Type<
  DurationTime,
  string,
  unknown
>(
  "DurationFromString",
  (u): u is DurationTime => u instanceof DurationTime,
  (u, c) =>
    E.either.chain(t.string.validate(u, c), (s) => {
      try {
        return t.success(parseDurationFromString(s));
      } catch {
        return t.failure(u, c);
      }
    }),
  (a) => formatDurationWithSeconds(a)
);

export const SafeDurationFromString: DurationFromStringC = new t.Type<
  DurationTime,
  string,
  unknown
>(
  "SafeDurationFromString",
  DurationFromString.is,
  (u, c) =>
    E.either.chain(DurationFromString.validate(u, c), (s) => {
      return t.success(durationSafeTotalHours(s));
    }),
  DurationFromString.encode
);

export const DurationMinutesFromString: DurationFromStringC = new t.Type<
  DurationTime,
  string,
  unknown
>(
  "DurationMinutesFromString",
  DurationFromString.is,
  (u, c) =>
    E.either.chain(DurationFromString.validate(u, c), (s) => {
      return t.success(durationRoundToMinutes(s));
    }),
  (a) => formatDuration(a)
);

export const SafeDurationFromHours: DurationFromNumberC = new t.Type<
  DurationTime,
  number,
  unknown
>(
  "SafeDurationFromHours",
  DurationFromString.is,
  (u, c) =>
    E.either.chain(t.number.validate(u, c), (s) => {
      if (hasNumberMaxFracDigits(s, 2)) {
        return t.success(durationSafeTotalHours(s));
      } else {
        return t.failure(u, c);
      }
    }),
  (a) => durationTotalHoursRounded(a)
);

export const DurationFromNumberMillis: DurationFromNumberC = new t.Type<
  DurationTime,
  number,
  unknown
>(
  "DurationFromNumberMillis",
  DurationFromString.is,
  (u, c) =>
    E.either.chain(t.number.validate(u, c), (s) =>
      t.success(parseDurationFromMilis(s))
    ),
  (a) => durationTotalMillis(a)
);

export function durationRoundToMinutes({
  hours,
  minutes,
  seconds,
  ms,
}: DurationTime): DurationTime {
  return new DurationTime({
    hours,
    minutes: seconds > 0 ? minutes + 1 : minutes,
    seconds: 0,
    ms: 0,
  });
}

export const DurationFromStringMillis: DurationFromStringC = new t.Type(
  "DurationFromStringMillis",
  DurationFromString.is,
  (u, c) =>
    pipe(
      t.string.validate(u, c),
      E.chain((s) => DurationFromNumberMillis.validate(Number(s), c))
    ),
  (a) => DurationFromNumberMillis.encode(a).toString()
);

/**
 * Zmeni duration nebo totalHours na validni pro konverzi na totalHours
 */
export function durationSafeTotalHours(d: DurationTime | number) {
  const totalHours = durationTotalHoursRounded(d);
  return parseDurationFromHours(totalHours);
}

export function parseDurationFromHours(value: number): DurationTime {
  const hours = Math.floor(value);
  value = Math.floor((value * 1000 - hours * 1000) * 60);

  const minutes = Math.floor(value / 1000);
  const seconds = Math.round(((value - minutes * 1000) * 60) / 1000);

  return new DurationTime({ hours, minutes, seconds });
}

export function parseDurationFromMinutes(value: number): DurationTime {
  const hours = Math.floor(value % 60);
  value = (value * 1000 - hours * 1000 * 60) / 1000;

  const minutes = Math.floor(value);
  const seconds = (value * 1000 - minutes * 1000) * 60;

  return new DurationTime({ hours, minutes, seconds });
}

export function subDuration(
  d1: DurationTime | null | undefined,
  d2: DurationTime | null | undefined
): DurationTime {
  return concatDuration(d1, d2, "SUB");
}

export function addDuration(
  d1: DurationTime | null | undefined,
  d2: DurationTime | null | undefined
): DurationTime {
  return concatDuration(d1, d2, "PLUS");
}

function concatDuration(
  d1: DurationTime | null | undefined,
  d2: DurationTime | null | undefined,
  sign: "PLUS" | "SUB"
): DurationTime {
  if (!d1 && !d2) {
    return DurationTime.zero();
  }

  const delta = sign === "PLUS" ? 1 : -1;

  if (!d1) {
    d1 = DurationTime.zero();
  }
  if (!d2) {
    d2 = DurationTime.zero();
  }

  let hours = d1.hours + delta * d2.hours;
  let minutes = d1.minutes + delta * d2.minutes;
  let seconds = d1.seconds + delta * d2.seconds;
  let ms = d1.ms + delta * d2.ms;

  return new DurationTime({ hours, minutes, seconds, ms });
}

export function parseDurationFromSeconds(value: number): DurationTime {
  return new DurationTime(durationFromSeconds(value));
}

export function parseDurationFromMilis(value: number): DurationTime {
  return new DurationTime(durationFromMillis(value));
}

export function durationFromDate(date: Date) {
  return DurationTime.create(
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
    date.getMilliseconds()
  );
}

function durationFromSeconds(value: number): {
  hours: number;
  minutes: number;
  seconds: number;
} {
  return parseDurationFromMilis(value * 1000);
}

function durationFromMillis(value: number): {
  hours: number;
  minutes: number;
  seconds: number;
  ms: number;
} {
  let abs = Math.abs(value);
  const sign = value < 0 ? -1 : 1;

  const ms = sign * Math.floor(abs % 1000);
  abs = Math.floor(abs / 1000);

  const hours = sign * Math.floor(abs / 3600);
  const minutes = sign * Math.floor((abs - sign * hours * 3600) / 60);
  const seconds =
    sign * Math.floor(abs - sign * hours * 3600 - sign * minutes * 60);

  return { hours, minutes, seconds, ms };
}

export function durationTotalHours({
  hours,
  minutes,
  seconds,
}: DurationTime): number {
  return hours + minutes / 60 + seconds / 3600;
}

export function durationTotalHoursRounded(
  hours: number | DurationTime
): number {
  if (hours instanceof DurationTime) {
    hours = durationTotalHours(hours);
  }

  return Math.round(hours * 100) / 100;
}

export function durationTotalMillis({
  hours,
  minutes,
  seconds,
  ms,
}: DurationTime): number {
  let total = ms;
  total += seconds * 1000;
  total += minutes * 60 * 1000;
  total += hours * 60 * 60 * 1000;

  return total;
}

export function parseSecondsFromDuration(value: DurationTime): number {
  const { hours, minutes, seconds } = value;

  return seconds + minutes * 60 + hours * 3600;
}

export function parseMillisFromDuration(value: DurationTime): number {
  const { hours, minutes, seconds, ms } = value;

  return (hours * 3600 + minutes * 60 + seconds) * 1000 + ms;
}

export function parseDurationFromString(s: string): DurationTime {
  if (!/^\s*-?\d+:\d\d:\d\d\s*$/.test(s)) {
    throw new ParseError(`String '${s}' has bad format for DurationTime`);
  }

  const sign = s.indexOf("-") !== -1 ? -1 : 1;

  const [hours, minutes, seconds] = s
    .split(":")
    .map((n) => Math.abs(Number(n)) * sign);
  return new DurationTime({
    hours,
    minutes,
    seconds,
  });
}

export function durationEquals(
  d1: DurationTime | undefined,
  d2: DurationTime | undefined
): boolean {
  if (d1 === undefined || d2 === undefined) {
    return d1 === d2;
  }

  return durationCompare(d1, d2) === 0;
}

export function durationCompare(d1: DurationTime, d2: DurationTime): number {
  const result = Math.round(
    parseMillisFromDuration(d1) - parseMillisFromDuration(d2)
  );
  return Math.min(Math.max(result, -1), 1);
}

export function formatDuration({ hours, minutes }: DurationTime): string {
  return formatDurationArray([hours, minutes]);
}

export function formatDurationWithSeconds({
  hours,
  minutes,
  seconds,
}: DurationTime): string {
  return formatDurationArray([hours, minutes, seconds]);
}

export function formatDurationArray(arr: number[]) {
  const negative = arr.some((x) => x < 0);

  return (negative ? "-" : "") + arr.map((x) => addZero(Math.abs(x))).join(":");
}

function addZero(value: number): string {
  return value.toString().padStart(2, "0");
}
