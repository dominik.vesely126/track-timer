import { addDuration, durationCompare, DurationTime, formatDateSql } from ".";
import { ApiError } from "../Errors";

export interface UpdateSelection<A, U, D> {
  toAdd: A[];
  toUpdate: U[];
  toDelete: D[];
}

interface KeyData {
  taskId: string | number | undefined;
  date: Date;
}

interface UpdateSelectionFactory<TItems, UItems, A, U, D> {
  createAdd: (i: UItems) => A;
  createUpdate: (i: TItems, u: UItems) => U;
  createDelete: (i: TItems) => D;
}

interface UpdateSelectionParams<TItems, UItems, A, U, D>
  extends UpdateSelectionFactory<TItems, UItems, A, U, D> {
  serverItems: TItems[];
  updateItems: Iterable<UItems>;
  getServerKey: (i: TItems) => KeyData;
  getUpdateKey: (i: UItems) => KeyData;
  getServerHours: (i: TItems) => DurationTime;
  getUpdateHours: (i: UItems) => DurationTime;
  //   resourceIdsByName: Record<string, string>;
}

export function getUpdateSelection<TItems, UItems, A, U, D>({
  //   companyId,
  //   empNo,
  updateItems,
  serverItems,
  getServerKey,
  getUpdateKey,

  getServerHours,
  getUpdateHours,

  createAdd,
  createUpdate,
  createDelete,
}: //   resourceIdsByName,
UpdateSelectionParams<TItems, UItems, A, U, D>): UpdateSelection<A, U, D> {
  const grouped: Map<string, { totalHours: DurationTime; items: TItems[] }> =
    new Map();
  const getKey = (date: Date, taskId: string | number): string => {
    return formatDateSql(date) + "_" + taskId;
  };

  for (const ts of serverItems) {
    const { taskId, date } = getServerKey(ts);

    if (!taskId) continue;

    const key = getKey(date, taskId);

    if (!grouped.has(key)) {
      grouped.set(key, {
        totalHours: DurationTime.zero(),
        items: [],
      });
    }

    const found = grouped.get(key)!;
    found.totalHours = addDuration(found.totalHours, getServerHours(ts));
    found.items.push(ts);
  }

  const { toAdd, toUpdate, toDelete }: UpdateSelection<A, U, D> = {
    toAdd: [],
    toUpdate: [],
    toDelete: [],
  };

  const zero = DurationTime.zero();
  //   const strEmpNo = empNo + "";
  for (const ts of updateItems) {
    const { taskId, date } = getUpdateKey(ts);

    if (!taskId) continue;

    const key = getKey(date, taskId);
    const isValid = durationCompare(getUpdateHours(ts), zero) > 0;
    let found = grouped.get(key);

    if (!found) {
      // item se stejnym dnem a tasKid vubec neexistuje
      if (isValid) {
        toAdd.push(createAdd(ts));
      }
      continue;
    }

    if (!found.items.length) {
      if (!isValid) continue;

      toAdd.push(createAdd(ts));
    } else {
      let deleteItems: TItems[];
      if (isValid) {
        const first = found.items[0];
        toUpdate.push(createUpdate(first, ts));
        deleteItems = found.items.slice(1);
      } else {
        deleteItems = found.items;
      }

      if (deleteItems.length) {
        toDelete.push(...deleteItems.map(createDelete));
      }
    }
  }

  return {
    toAdd,
    toUpdate,
    toDelete,
  };
}

export function compareTimesheets<T, U>(
  timesheets: T[],
  getTsHours: (i: T) => DurationTime,
  updateItems: U[],
  getUpdateHours: (i: U) => DurationTime,
  equals: (a: T, b: U) => boolean
): T[] {
  const badTimesheets: [string, any][] = [];
  const result: T[] = [];

  for (const ts of timesheets) {
    const tsHours = getTsHours(ts);
    const found = updateItems.find((x) => equals(ts, x));

    if (!found) {
      badTimesheets.push(["Timesheet not found in update items.", ts]);
      continue;
    }

    const updateHours = getUpdateHours(found);

    if (durationCompare(tsHours, updateHours) !== 0) {
      badTimesheets.push([
        "Timesheet has different hours than updated item.",
        {
          ts: tsHours,
          update: updateHours,
        },
      ]);
      continue;
    }

    result.push(ts);
  }
  const zero = DurationTime.zero();
  for (const update of updateItems) {
    const updateHours = getUpdateHours(update);
    if (durationCompare(updateHours, zero) <= 0) {
      continue;
    }
    const found = timesheets.find((x) => equals(x, update));

    if (!found) {
      badTimesheets.push(["Update item not found in timesheets.", update]);
      continue;
    }

    const tsHours = getTsHours(found);

    if (durationCompare(updateHours, tsHours) !== 0) {
      badTimesheets.push([
        "Update item has different hours than timesheet item.",
        {
          ts: tsHours,
          update: updateHours,
        },
      ]);
      continue;
    }

    // result.push(update);
  }

  if (badTimesheets.length) {
    throw new ApiError("Compare timesheets with updated items failed.", {
      errorData: badTimesheets,
    });
  }

  return result;
}

export function filterMapTimesheets<T, R>(
  timesheets: T[],
  filter: (a: T) => boolean,
  map: (a: T) => R
): R[] {
  return timesheets.reduce<R[]>((stack, item) => {
    if (filter(item)) {
      stack.push(map(item));
    }
    return stack;
  }, []);
}
