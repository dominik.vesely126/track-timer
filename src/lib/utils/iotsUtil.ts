export { DateFromISOString } from "io-ts-types";
import * as transformer from "io-ts-transformer";

export * as t from "io-ts";
export * as td from "io-ts-types";

import * as t from "io-ts";
import { E } from ".";
import { pipe } from "fp-ts/lib/function";

export type C = t.Mixed;
export class Type extends t.Type<any> {}

export type TypeC<T> = t.Type<T, any, unknown>;

export const { buildDecoder } = transformer;

export type DateFromUnixTimestampC = t.Type<Date, number, unknown>;
export type DateFromUnixTimestampStringC = t.Type<Date, string, unknown>;

export const DateFromUnixTimestamp: DateFromUnixTimestampC = new t.Type<
  Date,
  number,
  unknown
>(
  "DateFromUnixTimestamp",
  (u): u is Date => u instanceof Date,
  (u, c) =>
    E.either.chain(t.number.validate(u, c), (s) => {
      return t.success(new Date(s));
    }),
  (a) => a.getTime()
);

export const DateFromUnixTimestampString: DateFromUnixTimestampStringC =
  new t.Type(
    "DateFromUnixTimestampString",
    DateFromUnixTimestamp.is,
    (u, c) =>
      pipe(
        t.string.validate(u, c),
        E.chain((s) => DateFromUnixTimestamp.validate(Number(s), c))
      ),
    (a) => DateFromUnixTimestamp.encode(a).toString()
  );
