export function camelCaseToUnderscore(value: string) {
  return value.replace(/([A-Z])/g, "_$1").toLowerCase();
}

export function trimToFirstChar(value: string, char: string): string {
  if (!value) return "";
  return value.substr(value.indexOf(char) + 1).trim();
}

export function trimFromLastChar(value: string, char: string): string {
  if (!value) return "";

  const lastIndex = value.lastIndexOf(char);

  if (lastIndex !== -1) {
    return value.substr(0, lastIndex).trim();
  } else {
    return value;
  }
}

export function escapeString(value: string): string {
  return (value && value.replace("'", "''")) || value;
}
