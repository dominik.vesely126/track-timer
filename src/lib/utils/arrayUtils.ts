import { TTRangeError } from "../Errors/TTRangeError";

export function getSingle<T>(arr: T[]): T {
  if (arr.length !== 1) {
    throw new TTRangeError("Array has no single item." + JSON.stringify(arr));
  }

  return arr[0];
}

export function arrayEveryAll<T>(
  arr: T[],
  predicate: (a: T, b: T) => boolean
): boolean {
  if (arr.length === 0) return true;

  return arr.some((x) => !predicate(x, arr[0]));
}

export function objectMap<K extends keyof V, V, R>(
  obj: Record<K, V>,
  clb: (val: V, key: K) => R
): R[] {
  const result: R[] = [];
  for (const key in obj) {
    result.push(clb(obj[key], key));
  }

  return result;
}

export function objectReduce<
  V,
  K extends string | number | symbol = keyof V,
  R = any
>(obj: Record<K, V>, clb: (prev: R, currVal: V, currKey: K) => R, init: R): R {
  return objectKeys(obj).reduce((prev, key) => clb(prev, obj[key], key), init);
}

export function objectKeys<V, K extends string | number | symbol = keyof V>(
  obj: Record<K, V>
): K[] {
  return Object.keys(obj) as any;
}

export function arrayToObject<T, K extends string | number | symbol, V>(
  arr: readonly T[],
  key: (i: T) => K,
  val: (i: T) => V
): Record<K, V> {
  return arr.reduce((r, i) => {
    r[key(i)] = val(i);
    return r;
  }, {} as Record<K, V>);
}
