import * as fp from "fp-ts/lib/function";
import * as iE from "fp-ts/lib/Either";
import * as iAP from "fp-ts/lib/Apply";
import * as iT from "fp-ts/lib/Task";
import * as iA from "fp-ts/lib/Array";
import * as iR from "fp-ts/lib/Record";
import * as iOrd from "fp-ts/lib/Ord";
import * as iSemigroup from "fp-ts/lib/Semigroup";
import * as iString from "fp-ts/lib/string";
import * as iIO from "fp-ts/IO";

export const IO = {
  ...iIO,
};
// export { Ord } from "fp-ts/lib/string";

export const pipe = fp.pipe;

export const E = {
  ...iE,
};

export const AP = {
  ...iAP,
};

export const T = {
  ...iT,
};

export const A = {
  ...iA,
};

export const R = {
  ...iR,
};

export const Semigroup = {
  ...iSemigroup,
};

export const Ord = {
  ...iOrd,
};

export const fpString = {
  ...iString,
};
