import { IfsTaskType } from "../types";

export function ifsTaskTypeByTaskId(taskId: string): IfsTaskType {
  if (taskId && taskId.indexOf(".") !== -1) {
    return "Project";
  } else {
    return "WorkOrder";
  }
}

export function ifsParseTaskIdByType(
  taskId: string,
  type: IfsTaskType
): string {
  switch (type) {
    case "Project":
      //   return trimFromLastChar(taskId, "_");
      return taskId;

    case "WorkOrder":
      return taskId;
  }
}

export function parseIfsTimesheetProjectId(projectId: string) {
  //   return trimToFirstChar(projectId, ".");
  return projectId;
}
