export function createUrl({
  url,
  query,
}: {
  url: string;
  query?: Record<string, any>;
}): string {
  const queryString = parseQueryString(query);

  if (queryString.length) {
    url += `?${queryString}`;
  }

  return url;
}

export function parseQueryString(query?: Record<string, any>) {
  if (!query || typeof query !== "object") return "";

  return Object.keys(query)
    .reduce<string[]>((result, key) => {
      const val = query[key];

      if (val !== null && val !== undefined) {
        result.push(`${key}=${val}`);
      }

      return result;
    }, [])
    .join("&");
}
