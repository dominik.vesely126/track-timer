import { BaseError } from "./BaseError";

export class ParseError extends BaseError {}
