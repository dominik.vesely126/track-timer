import { ApiRequest } from "../types";
import { BaseError } from "./BaseError";

interface ApiErrorData {
  errorData?: any;
  request?: ApiRequest;
}

export class ApiError extends BaseError {
  _request: ApiRequest | undefined;
  _errorData: any;

  static fromUnknown(error: unknown, data?: ApiErrorData) {
    if (!error) {
      return new ApiError("Api call failed.", data);
    }

    switch (typeof error) {
      case "object":
        return new ApiError("Api error is object.", data);
      default:
        return new ApiError(error + "", data);
    }
  }

  constructor(msg: string, data?: ApiErrorData) {
    super(msg);
    this._errorData = data?.errorData;
    this._request = data?.request;
  }
}

export class ApiStatusError extends ApiError {
  status;

  constructor(status: number, msg: string, data?: ApiErrorData) {
    super(msg, data);
    this.status = status;
  }
}

// export class ApiCodecError extends ApiError {
//   _codecError;

//   constructor(codecError: unknown) {
//     super("Api codec validation error");
//     this._codecError = codecError;
//   }

//   getCodecError() {
//     return this._codecError;
//   }
// }
