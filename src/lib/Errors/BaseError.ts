export abstract class BaseError extends Error {
  _previous: any;

  constructor(message: string, previous?: any) {
    super(message);
    this._previous = previous;
  }

  toString(): string {
    return this.message;
  }

  stackTrace(): string | undefined {
    return this.stack;
  }
}
