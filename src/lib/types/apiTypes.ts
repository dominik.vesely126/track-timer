import { AxiosRequestConfig, AxiosRequestHeaders } from "axios";
import { IfsApiNativeResponseError, Task, TaskEither } from ".";
import { ClickNativeError } from "../../server/types";
import { ApiError } from "../Errors/ApiError";
import { C } from "../utils";

export type ApiDynamicConfig = Task<AxiosRequestConfig>;

export type ApiHeaders = AxiosRequestHeaders;

export interface BasicApiResponseError {
  statusCode: number;
  message: string;
  data?: any;
  error?: ApiError | Error;
}

export type ApiKind = "RM" | "Click" | "IFS";

export type ApiResponseErrorK<T extends ApiResponseError["code"]> =
  T extends "Click"
    ? ClickApiResponseError
    : T extends "RM"
    ? RMApiResponseError
    : T extends "IFS"
    ? IfsApiResponseError
    : never;

export type ApiResponse<
  T,
  E extends BasicApiResponseError = BasicApiResponseError
> = TaskEither<E, T>;

export type RMApiResponseError = BasicApiResponseError & {
  code: "RM";
  rmErrors?: string[];
};

export type ClickApiType = "WEB" | "API";

export interface ClickApiResponseError extends BasicApiResponseError {
  code: "Click";
  api?: ClickApiType;
  clickError?: ClickNativeError;
}
export interface IfsApiResponseError extends BasicApiResponseError {
  code: "IFS";
  ifsError?: IfsApiNativeResponseError;
  data?: unknown;
}

export type ApiResponseError =
  | IfsApiResponseError
  | RMApiResponseError
  | ClickApiResponseError;

export interface ApiRequest {
  method: ApiMethod;
  url: string;
  data?: ApiData;
  params?: ApiParams;
}

// export type ApiResponseHandler<T extends ApiError> = (error: unknown) => T;
export type ApiParams = Record<string, any> | string;
export type ApiData = Record<string, any>;
export type ApiMethod = "GET" | "POST" | "PUT" | "DELETE";

export interface ApiAllOptions {
  path: string;
  params: ApiParams;
  method: ApiMethod;
  data?: ApiData;
  codec?: C;
  //   httpsAgent?: Agent;
}

export interface ApiOptions<T> {
  headers?: ApiHeaders;
  params?: ApiParams;
  data?: ApiData | T;
  dataCodec?: C;
  codec?: C;
  //   dynamic: ApiDynamicConfig;
}

export interface Api {
  get<T>(path: string, params?: ApiParams): Promise<T>;
  post<T, D>(path: string, options: ApiOptions<D>): Promise<T>;
  put<T, D>(path: string, options: ApiOptions<D>): Promise<T>;
  all<T>(options: ApiAllOptions): Promise<T>;
}
