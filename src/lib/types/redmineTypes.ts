import { DateFromISOString } from "io-ts-types";
import {
  ApiResponse,
  ApiResponseError,
  RMApiResponseError,
  TaskEither,
} from ".";
import {
  DurationFromString,
  DurationTime,
  SafeDurationFromHours,
  t,
} from "../utils";
import { Project } from "./types";

export type RMUser = t.TypeOf<typeof RMUser>;

export const RMUser = t.type({
  id: t.number,
  fullname: t.string,
});

export interface RMProject extends Project {}

export interface RMApiActivity {
  id: number;
  name: string;
}
export interface RMTask {
  id: number;
  name: string;
  zakazka: string | null;
}

export interface RMSearchTask extends Pick<RMTask, "id" | "name"> {}

export interface RMBaseParams {
  top?: number;
  skip?: number;
}

export type RMTimesheet = t.TypeOf<typeof RMTimesheet>;

export const RMTimesheet = t.type({
  id: t.number,
  date: DateFromISOString,
  duration: DurationFromString,
  project: t.type({
    id: t.number,
    name: t.string,
  }),
  task: t.type({
    id: t.number,
    name: t.string,
  }),
  user: t.type({
    id: t.number,
    fullname: t.string,
  }),
});

export type RMApiUpdateTimesheetsHours = t.TypeOf<
  typeof RMApiUpdateTimesheetsHours
>;

export const RMApiUpdateTimesheetsHours = t.type({
  issueId: t.number,
  date: DateFromISOString,
  userId: t.number,
  duration: DurationFromString,
  activityId: t.number,
  description: t.union([t.string, t.undefined]),
});

export type RMApiUpdateTimesheetsHoursParams = t.TypeOf<
  typeof RMApiUpdateTimesheetsHoursParams
>;
export const RMApiUpdateTimesheetsHoursParams = t.type({
  items: t.array(RMApiUpdateTimesheetsHours),
});

export interface RMApiTimesheetsParams {
  userId: number;
  start: Date;
  end: Date;
}

export const RMApiTimesheetHours = t.type({
  issueId: t.number,
  projectId: t.union([t.undefined, t.number]),
  user: t.type({
    id: t.number,
    fullname: t.string,
  }),
  date: DateFromISOString,
  duration: DurationFromString,
});

export type RMApiTimesheetHours = t.TypeOf<typeof RMApiTimesheetHours>;

export type RMApiResponse<T> = ApiResponse<T, RMApiResponseError>;

export interface RMApiSearchTasksParams extends RMBaseParams {
  query: string;
}

export interface RMApi {
  getCurrentUser(): RMApiResponse<RMUser>;

  getTimesheets(options: RMApiTimesheetsParams): RMApiResponse<RMTimesheet[]>;

  getTimesheetsByHours(
    options: RMApiTimesheetsParams
  ): RMApiResponse<RMApiTimesheetHours[]>;

  getActivities(): RMApiResponse<RMApiActivity[]>;

  getTaskById(id: number): RMApiResponse<RMTask | null>;

  searchTasks(params: RMApiSearchTasksParams): RMApiResponse<RMSearchTask[]>;

  updateTimesheetsHours(
    params: RMApiUpdateTimesheetsHoursParams
  ): RMApiResponse<RMApiTimesheetHours[]>;
}
