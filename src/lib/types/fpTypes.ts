export type { Either } from "fp-ts/Either";
export type { TaskEither } from "fp-ts/TaskEither";
export type { Task } from "fp-ts/Task";
export type { IO } from "fp-ts/IO";
export * as O from "fp-ts/lib/Option";

import * as iTE from "fp-ts/TaskEither";

export const TE = {
  ...iTE,
};
