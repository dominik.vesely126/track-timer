import { t } from "../utils";

export const Culure = t.keyof({
  "cs-CZ": null,
  "en-US": null,
});

export interface Project {
  id: string;
  name: string;
}

export interface User<T> {
  id: T;
  fullname: string;
}

function isType(o: unknown, tag: string): boolean {
  // @ts-ignore
  return o && typeof o === "object" && o._tag === tag;
}
