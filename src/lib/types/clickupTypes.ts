import { TaskEither } from "./fpTypes";
import { ApiResponseError } from "./apiTypes";
import { DurationFromString, DateFromISOString, t, td } from "../utils";
import { Project } from "./types";
import { ApiResponse, ClickApiResponseError } from ".";

export interface ClickProject extends Project {}

// export type ClickTaskCustomField = t.TypeOf<typeof ClickTaskCustomField>;

export type ClickTask = t.TypeOf<typeof ClickTask>;

// export const ClickTaskCustomField = t.type({
//   name: t.string,
//   value: t.string,
// });

export const ClickTask = t.type({
  id: t.string,
  name: t.string,
  //   customFields: t.array(ClickTaskCustomField),
  redmineId: t.union([t.number, t.undefined]),
  ifsId: t.union([t.string, t.undefined]),
});

export type ClickUser = t.TypeOf<typeof ClickUser>;

export const ClickUser = t.type({
  id: t.number,
  fullname: t.string,
});

const UpdateType = t.keyof({
  add: null,
  update: null,
  delete: null,
});

const ClickUpdateTimesheetItemBase = t.type({
  start: DateFromISOString,
  end: DateFromISOString,
  taskId: t.string,
  //   userId: t.number,
  description: t.string,
  billable: t.boolean,
});

export type ClickUpdateTimesheetItemType = t.TypeOf<
  typeof ClickUpdateTimesheetItemType
>;

export const ClickUpdateTimesheetItemType = t.keyof({
  add: null,
  update: null,
  delete: null,
});

export type ClickUpdateTimesheetItem = t.TypeOf<
  typeof ClickUpdateTimesheetItem
>;

export const ClickUpdateTimesheetItem = t.union([
  t.intersection([
    t.type({
      type: t.literal("add"),
      userId: t.number,
    }),
    ClickUpdateTimesheetItemBase,
  ]),
  t.intersection([
    t.type({
      type: t.literal("update"),
      id: t.string,
    }),
    ClickUpdateTimesheetItemBase,
  ]),
  t.type({
    type: t.literal("delete"),
    id: t.string,
  }),
]);

export type ClickUpdateTimesheetParams = t.TypeOf<
  typeof ClickUpdateTimesheetParams
>;

export const ClickUpdateTimesheetParams = t.type({
  teamId: t.number,
  items: t.array(ClickUpdateTimesheetItem),
});

export type ClickUpdateTimesheetResult = t.TypeOf<
  typeof ClickUpdateTimesheetResult
>;
export const ClickUpdateTimesheetResult = t.type({
  id: t.string,
  task: t.type({
    id: t.string,
    name: t.string,
  }),
  user: t.type({
    id: t.number,
    username: t.string,
  }),
  billable: t.boolean,
  start: DateFromISOString,
  end: DateFromISOString,
  duration: DurationFromString,
  description: t.string,
});

export type ClickTimesheet = t.TypeOf<typeof ClickTimesheet>;

export const ClickTimesheet = t.type({
  id: t.string,
  start: td.DateFromISOString,
  end: td.DateFromISOString,
  duration: DurationFromString,
  task: t.union([ClickTask, t.null]),
  user: t.type({
    id: t.number,
    fullname: t.string,
  }),
});

export type ClickTimesheetHours = t.TypeOf<typeof ClickTimesheetHours>;

export const ClickTimesheetHours = t.type({
  id: t.string, // datum + task id
  date: td.DateFromISOString,
  duration: DurationFromString,
  task: t.union([ClickTask, t.null]),
  timesheets: t.array(t.string),
});

export type ClickTeam = t.TypeOf<typeof ClickTeam>;

export const ClickTeam = t.type({
  id: t.number,
  name: t.string,
});

export type ClickSpace = t.TypeOf<typeof ClickSpace>;

export const ClickSpace = t.type({
  id: t.number,
  name: t.string,
});

export interface ClickApiTimesheetOptions {
  teamId: number;
  userId: number;
  start: Date;
  end: Date;
}

export type ClickApiResponse<T> = ApiResponse<T, ClickApiResponseError>;

export interface ClickApiTasksByCustomFieldParams {
  spaceId: number;
  field: string;
  value: any;
}

export interface ClickApiTaskByIdParams {
  taskId: string;
}

export interface ClickApiSearchTasksParams {
  teamId: number;
  query: string;
}

export type ClickApiAuthorizeResult = t.TypeOf<typeof ClickApiAuthorizeResult>;

export const ClickApiAuthorizeResult = t.type({
  access_token: t.string,
});

export interface ClickApiAuthorizeParams {
  code: string;
}

export interface ClickApiLoginParams {
  email: string;
  password: string;
}

export interface ClickApiLoginResult {
  user: {
    id: number;
    username: string;
    timezone_offset: string;
    email: string;
  };
  token: string;
  form_token: string;
  expiration: number;
  refresh_token: string;
}

export interface ClickApiRefreshResult {
  token: string;
  expiration: number;
}

export interface ClickApi {
  oauthToken(
    params: ClickApiAuthorizeParams
  ): ClickApiResponse<ClickApiAuthorizeResult>;

  login(params: ClickApiLoginParams): ClickApiResponse<ClickApiLoginResult>;

  refresh(): ClickApiResponse<ClickApiRefreshResult>;

  getTimesheets: (
    options: ClickApiTimesheetOptions
  ) => ClickApiResponse<ClickTimesheet[]>;

  getTimesheetsHours: (
    options: ClickApiTimesheetOptions
  ) => ClickApiResponse<ClickTimesheetHours[]>;

  getTasksByCustomField(
    options: ClickApiTasksByCustomFieldParams
  ): ClickApiResponse<ClickTask[]>;

  getTaskById(taskId: string): ClickApiResponse<ClickTask>;

  searchTasks(params: ClickApiSearchTasksParams): ClickApiResponse<ClickTask[]>;

  getCurrentUser(): ClickApiResponse<ClickUser>;

  getTeams(): ClickApiResponse<ClickTeam[]>;

  getSpaces(teamId: number): ClickApiResponse<ClickSpace[]>;

  updateTimesheets(
    params: ClickUpdateTimesheetParams
  ): ClickApiResponse<ClickUpdateTimesheetResult[]>;

  updateTaskExternalIds(
    params: ClickApiUpdateExternalIdsParams
  ): ClickApiResponse<ClickTask>;

  updateTask(params: ClickApiUpdateTaskParams): ClickApiResponse<boolean>;
}

export interface ClickApiUpdateTaskParams {
  taskId: string;
  data: Partial<Omit<ClickTask, "id">>;
}

export type ClickExternalName = t.TypeOf<typeof ClickExternalName>;

export const ClickExternalName = t.keyof({
  IFS: null,
  RM: null,
});

export type ClickApiExternalIds = t.TypeOf<typeof ClickApiExternalIds>;
export const ClickApiExternalIds = t.record(ClickExternalName, t.string);

export type ClickApiUpdateExternalIdsParams = t.TypeOf<
  typeof ClickApiUpdateExternalIdsParams
>;

export const ClickApiUpdateExternalIdsParams = t.type({
  taskId: t.string,
  externalIds: ClickApiExternalIds,
});
