import { DateFromISOString } from "io-ts-types";
import {
  ApiResponse,
  ApiResponseError,
  IfsApiResponseError,
  TaskEither,
} from ".";
import { DurationFromString, SafeDurationFromString, t, td } from "../utils";
import { Project, User } from "./types";

export type IfsTaskType = t.TypeOf<typeof IfsTaskType>;

// export interface IfsTaskTypeC extends t.Type<string, string, unknown> {}

export const IfsTaskType = new t.Type<"Project" | "WorkOrder", string, unknown>(
  "IfsTaskType",
  (u): u is "Project" | "WorkOrder" => u === "Project" || u === "WorkOrder",
  (u, c) =>
    u === "Project" || u === "WorkOrder" ? t.success(u) : t.failure(u, c),
  t.identity
);

export type IfsCurrentUserInfo = t.TypeOf<typeof IfsCurrentUserInfo>;

export const IfsCurrentUserInfo = t.type({
  fullname: t.string,
  empNo: t.number,
  personId: t.number,
  companyId: t.string,
});

export interface IfsProject extends Project {}

export type IfsTask = t.TypeOf<typeof IfsTask>;

export const IfsTask = t.type({
  id: t.string,
  name: t.string,
  type: IfsTaskType,
});

export type IfsTimesheet = t.TypeOf<typeof IfsTimesheet>;

export const IfsTimesheet = t.type({
  id: t.string,
  date: td.DateFromISOString,
  duration: DurationFromString,
  task: IfsTask,
  user: t.type({
    id: t.string,
    fullname: t.string,
  }),
});

export const IfsTimesheetHours = t.type({
  task: IfsTask,
  user: t.type({
    id: t.string,
    fullname: t.string,
  }),
  duration: DurationFromString,
  date: DateFromISOString,
});

export type IfsTimesheetHours = t.TypeOf<typeof IfsTimesheetHours>;

export type IfsAttendanceHours = t.TypeOf<typeof IfsAttendanceHours>;

export const IfsAttendanceHours = t.type({
  date: DateFromISOString,
  hours: DurationFromString,
  userId: t.string,
});

export type IfsEmployeeDayStatus = t.TypeOf<typeof IfsEmployeeDayStatus>;

const IfsEmployeeDayStatus = t.keyof({
  FINISHED: null,
  INWORK: null,
});

export type IfsEmployeeDayInfo = t.TypeOf<typeof IfsEmployeeDayInfo>;

export const IfsEmployeeDayInfo = t.type({
  date: DateFromISOString,
  status: IfsEmployeeDayStatus,
  userId: t.string,
});

export type IfsApiNativeUser = t.TypeOf<typeof IfsApiNativeUser>;

export const IfsApiNativeUser = t.type({
  CompanyId: t.string,
  EmpNo: t.string,
  EmployeeName: t.string,
  PersonId: t.string,
});

export interface IfsApiNativeResponseError {
  error: {
    code: string;
    message: string;
  };
}

export type IfsApiResponse<T> = ApiResponse<T, IfsApiResponseError>;

export interface IfsBaseParams {
  top?: number;
  skip?: number;
}

export interface IfsTimesheetParams extends IfsBaseParams {
  companyId: string;
  empNo: number;
  start: Date;
  end: Date;
  // TimezoneHours: number;
}

export const IfsUpdateTimesheetHoursItem = t.type({
  date: DateFromISOString,
  duration: SafeDurationFromString,
  taskId: t.string,
  taskType: IfsTaskType,
});

export type IfsUpdateTimesheetHoursItem = t.TypeOf<
  typeof IfsUpdateTimesheetHoursItem
>;

export type IfsUpdateTimesheetHoursParams = t.TypeOf<
  typeof IfsUpdateTimesheetHoursParams
>;

export const IfsUpdateTimesheetHoursParams = t.type({
  companyId: t.string,
  empNo: t.number,
  items: t.array(IfsUpdateTimesheetHoursItem),
});

export interface IfsAttendanceHoursParams extends IfsTimesheetParams {}

export interface IfsEmployeeDayInfoParams extends IfsTimesheetParams {}

export interface IfsSearchParams extends IfsBaseParams {
  companyId: string;
  //   empNo: number;
  //   date: Date;
  query: string;
}

export interface IfsProjectSearchParams extends IfsSearchParams {
  date: Date;
}

export interface IfsWorkOrderOperationParams extends IfsBaseParams {
  companyId: string;
  workOrderId: number;
}

export interface IfsProjectParams extends IfsBaseParams {
  companyId: string;
  date: Date;
  query: string;
}

export interface IfsProjectReportParams extends IfsBaseParams {
  companyId: string;
  date: Date;
  projectId: string;
}

export interface IfsApiTimesheetsResponse {}
export interface IfsApi {
  getCurrentUser(): IfsApiResponse<IfsCurrentUserInfo>;

  getTimesheetsByHours(
    params: IfsTimesheetParams
  ): IfsApiResponse<IfsTimesheetHours[]>;

  getAttendanceHours(
    params: IfsAttendanceHoursParams
  ): IfsApiResponse<IfsAttendanceHours[]>;

  getEmployeeDayInfo(
    params: IfsEmployeeDayInfoParams
  ): IfsApiResponse<IfsEmployeeDayInfo[]>;

  searchWorkOrder(params: IfsSearchParams): IfsApiResponse<IfsWorkOrder[]>;
  searchProject(params: IfsProjectParams): IfsApiResponse<IfsProject[]>;

  getProjectReports(
    params: IfsProjectReportParams
  ): IfsApiResponse<IfsProjectReport[]>;
  getWorkOrderOperations(
    params: IfsWorkOrderOperationParams
  ): IfsApiResponse<IfsWorkOrderOperation[]>;

  updateTimesheetsHours(
    params: IfsUpdateTimesheetHoursParams
  ): IfsApiResponse<IfsTimesheetHours[]>;
}

export interface IfsProject {
  id: string;
  name: string;
  type: "Project";
}

export interface IfsProjectReport {
  type: "Project";
  id: string;
  name: string;
  projectId: string;
}

export interface IfsWorkOrder {
  id: number;
  name: string;
  type: "WorkOrder";
}

export interface IfsWorkOrderOperation {
  type: "WorkOrder";
  id: string;
  name: string;
  operationId: number;
  workOrderId: number;
}
