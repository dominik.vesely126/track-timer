import { hasNumberMaxFracDigits } from "../../lib/utils";

describe("has number max frac digit", () => {
  test("has same digits", () => {
    expectHasNumberMaxFrag(2.33, 2, true);
    expectHasNumberMaxFrag(2.336, 3, true);
    expectHasNumberMaxFrag(2.0, 0, true);
    expectHasNumberMaxFrag(2.0, 4, true);
    expectHasNumberMaxFrag(2, 0, true);
    expectHasNumberMaxFrag(2, 5, true);
    expectHasNumberMaxFrag(2.3, 2, true);
    expectHasNumberMaxFrag(2.1, 1, true);

    expectHasNumberMaxFrag(2.33, 1, false);
    expectHasNumberMaxFrag(2.1456, 3, false);
  });

  function expectHasNumberMaxFrag(
    input: number,
    max: number,
    expected: boolean
  ) {
    const actual = hasNumberMaxFracDigits(input, max);

    expect({
      input,
      result: actual,
    }).toEqual({
      input,
      result: expected,
    });
  }
});
