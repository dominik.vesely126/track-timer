import { ParseError } from "../../lib/Errors/ParseError";
import {
  addDuration,
  durationCompare,
  durationEquals,
  durationRoundToMinutes,
  durationSafeTotalHours,
  DurationTime,
  durationTotalHoursRounded,
  formatDuration,
  formatDurationWithSeconds,
  parseDurationFromSeconds,
  parseDurationFromString,
  parseSecondsFromDuration,
  subDuration,
} from "../../lib/utils";

describe("test constructor", () => {
  test("positive duration", () => {
    durationStringEquals(DurationTime.create(1, 70, 90, 1200), "2:11:31.200");
    durationStringEquals(DurationTime.create(0, 60, 60, 1000), "1:01:01.000");

    durationStringEquals(DurationTime.create(1, -20, -30, -400), "0:39:29.600");
  });

  test("negative duration", () => {
    durationStringEquals(
      DurationTime.create(-2, 70, 90, 1200),
      "0:-48:-28.-800"
    );
    durationStringEquals(
      DurationTime.create(0, -70, -90, -1200),
      "-1:-11:-31.-200"
    );
  });

  test("add duration", () => {
    addDurationEquals("2:10:40.120", "6:40:50.999", "8:51:31.119");
    addDurationEquals("2:10:40.120", "-6:40:50.999", "-3:-8:-28.-881");
    addDurationEquals("-2:10:40.120", "6:40:50.999", "4:51:31.119");
  });

  test("sub duration", () => {
    subDurationEquals("2:10:40.120", "6:40:50.999", "-4:-30:-10.-879");
    subDurationEquals("-2:10:40.120", "6:40:50.999", "-8:-30:-10.-879");
    subDurationEquals("2:10:40.120", "-6:40:50.999", "7:29:49.121");
  });

  test("format duration", () => {
    const input = DurationTime.create(2, 3, 4, 200);
    const output = "02:03";

    expect(formatDuration(input)).toEqual(output);
  });

  test("format negative duration", () => {
    const input = DurationTime.create(-2, -3, -4, -200);
    const output = "-02:03";

    expect(formatDuration(input)).toEqual(output);
  });

  test("format duration with seconds", () => {
    const input = DurationTime.create(2, 3, 4, 200);
    const output = "02:03:04";

    expect(formatDurationWithSeconds(input)).toEqual(output);
  });

  test("format negative duration with seconds", () => {
    const input = DurationTime.create(-2, -3, -4, -200);
    const output = "-02:03:04";

    expect(formatDurationWithSeconds(input)).toEqual(output);
  });

  test("compare duration", () => {
    const i1 = DurationTime.create(1, 2, 3, 4);
    const i2 = DurationTime.create(1, 2, 3, 5);
    const i3 = DurationTime.create(1, 2, 3, 6);
    const i4 = DurationTime.create(1, 2, 3, 5);

    expect(durationCompare(i1, i2)).toEqual(-1);
    expect(durationCompare(i2, i1)).toEqual(1);
    expect(durationCompare(i1, i3)).toEqual(-1);
    expect(durationCompare(i2, i4)).toEqual(0);
  });

  test("duration equals", () => {
    const i1 = DurationTime.create(1, 2, 3, 4);
    const i2 = DurationTime.create(1, 2, 3, 5);
    const i3 = DurationTime.create(1, 2, 3, 6);
    const i4 = DurationTime.create(1, 2, 3, 5);

    expect(durationEquals(i1, i2)).toEqual(false);
    expect(durationEquals(i2, i1)).toEqual(false);
    expect(durationEquals(i1, i3)).toEqual(false);
    expect(durationEquals(i2, i4)).toEqual(true);
    expect(durationEquals(DurationTime.zero(), DurationTime.zero())).toEqual(
      true
    );
    expect(
      durationEquals(DurationTime.create(0, 0, 0, 0), DurationTime.zero())
    ).toEqual(true);
  });
});

describe("test round duration to minutes", () => {
  const d1 = DurationTime.create(1, 2, 30);
  const d2 = DurationTime.create(1, 2, 31);
  const d3 = DurationTime.create(1, 2, 59);
  const d4 = DurationTime.create(1, 2, 0);
  const d5 = DurationTime.create(1, 59, 31);

  expectDuration(durationRoundToMinutes(d1), "1:03:00.000");
  expectDuration(durationRoundToMinutes(d2), "1:03:00.000");
  expectDuration(durationRoundToMinutes(d3), "1:03:00.000");
  expectDuration(durationRoundToMinutes(d4), "1:02:00.000");
  expectDuration(durationRoundToMinutes(d5), "2:00:00.000");
});

describe("round duration hours", () => {
  test("round number hours", () => {
    const i1 = 2.335;
    const i2 = 2.334;
    const i3 = 2.395;

    expect(durationTotalHoursRounded(i1)).toEqual(2.34);
    expect(durationTotalHoursRounded(i2)).toEqual(2.33);
    expect(durationTotalHoursRounded(i3)).toEqual(2.4);
  });

  test("round hours from duration", () => {
    const d1 = DurationTime.create(1, 20);
    const d2 = DurationTime.create(1, 40);
    const d3 = DurationTime.create(1, 15);
    const d4 = DurationTime.create(1, 30);
    const d5 = DurationTime.create(1, 45);

    expect(durationTotalHoursRounded(d1)).toEqual(1.33);
    expect(durationTotalHoursRounded(d2)).toEqual(1.67);
    expect(durationTotalHoursRounded(d3)).toEqual(1.25);
    expect(durationTotalHoursRounded(d4)).toEqual(1.5);
    expect(durationTotalHoursRounded(d5)).toEqual(1.75);
  });

  test("duration with safe total hours", () => {
    const d1 = DurationTime.create(1, 20);
    const d2 = DurationTime.create(1, 40);
    const d3 = DurationTime.create(1, 15);

    expectDuration(durationSafeTotalHours(d1), "1:19:48.000");
    expectDuration(durationSafeTotalHours(d2), "1:40:12.000");
    expectDuration(durationSafeTotalHours(d3), "1:15:00.000");
  });
});

describe("format duration", () => {
  test("format hours and minutes", () => {
    const d1 = DurationTime.create(1, 2, 59);
    const d2 = DurationTime.create(1, 21);
    const d3 = DurationTime.create(11, 2);
    const d4 = DurationTime.create(0, 2);

    expect(formatDuration(d1)).toEqual("01:02");
    expect(formatDuration(d2)).toEqual("01:21");
    expect(formatDuration(d3)).toEqual("11:02");
    expect(formatDuration(d4)).toEqual("00:02");
  });

  test("format negative hours and minutes", () => {
    const d1 = DurationTime.create(-1, -2, -59);

    expect(formatDuration(d1)).toEqual("-01:02");
  });

  test("format hours, minutes and seconds", () => {
    const d1 = DurationTime.create(1, 2, 59, 2);
    const d2 = DurationTime.create(1, 2, 9, 2);
    const d3 = DurationTime.create(1, 21);
    const d4 = DurationTime.create(0, 2);

    expect(formatDurationWithSeconds(d1)).toEqual("01:02:59");
    expect(formatDurationWithSeconds(d2)).toEqual("01:02:09");
    expect(formatDurationWithSeconds(d3)).toEqual("01:21:00");
    expect(formatDurationWithSeconds(d4)).toEqual("00:02:00");
  });

  test("format negative hours, minutes and seconds", () => {
    const d1 = DurationTime.create(-1, -2, -59, -300);

    expect(formatDurationWithSeconds(d1)).toEqual("-01:02:59");
  });

  test("parse duration from string", () => {
    expect(parseDurationFromString("01:20:30")).toEqual(
      DurationTime.create(1, 20, 30)
    );
    expect(parseDurationFromString("20:01:03")).toEqual(
      DurationTime.create(20, 1, 3)
    );
  });

  test("parse negative duration from string", () => {
    expect(parseDurationFromString("-01:20:30")).toEqual(
      DurationTime.create(-1, -20, -30)
    );
    expect(parseDurationFromString("-20:01:03")).toEqual(
      DurationTime.create(-20, -1, -3)
    );
  });

  test("throw if parse duration from string with millis", () => {
    expect(() => parseDurationFromString("01:20:30.2")).toThrow(ParseError);
    expect(() => parseDurationFromString("20:01:03.")).toThrow(ParseError);
  });
});

function subDurationEquals(d1: string, d2: string, output: string) {
  const dur1 = getDurationFromString(d1);
  const dur2 = getDurationFromString(d2);
  const result = subDuration(dur1, dur2);

  expectDuration(result, output);
}

function addDurationEquals(d1: string, d2: string, output: string) {
  const dur1 = getDurationFromString(d1);
  const dur2 = getDurationFromString(d2);
  const result = addDuration(dur1, dur2);

  expectDuration(result, output);
}

function durationStringEquals(dur: DurationTime, output: string) {
  return expect(getStringDuration(dur)).toEqual(output);
}

function getDurationFromString(dur: string) {
  let [hours, minutes, seconds] = dur.split(":").map(Number);
  const ms = (seconds * 1000) % 1000;
  seconds = Math.floor(seconds);

  return DurationTime.create(hours, minutes, seconds, ms);
}

function expectDuration(actual: DurationTime, expected: string | DurationTime) {
  if (typeof expected !== "string") {
    expected = getStringDuration(expected);
  }
  expect(getStringDuration(actual)).toEqual(expected);
}

function getStringDuration({ hours, minutes, seconds, ms }: DurationTime) {
  return (
    hours +
    ":" +
    [minutes, seconds].map(addZero(2)).join(":") +
    "." +
    addZero(3)(ms)
  );
}

function addZero(count: number) {
  return (val) => `${val}`.padStart(count, "0");
}

// test("1:20:30", "2:10:40", "3:31:10");

function aaaa(d1: string, d2: string, concat: string) {
  const dur1 = parseDurationFromString(d1);
  const dur2 = parseDurationFromString(d2);

  const sum = addDuration(dur1, dur2);

  const sumStr = formatDurationWithSeconds(sum);
  const concatDur = parseDurationFromString(concat);
  concat = formatDurationWithSeconds(concatDur);

  console.log("sum", sumStr, concat, sumStr === concat);

  const sec1 = parseSecondsFromDuration(dur1);
  const sec2 = parseSecondsFromDuration(dur2);
  const subSec = sec1 - sec2;
  const durSub = subDuration(dur1, dur2);

  const calcDur2 = addDuration(dur1, durSub);

  const strDurSubSec = formatDurationWithSeconds(
    parseDurationFromSeconds(subSec)
  );
  const strDurSub = formatDurationWithSeconds(durSub);
  const strCalcDur2 = formatDurationWithSeconds(calcDur2);
  const strDur2 = formatDurationWithSeconds(dur2);

  console.log({
    subSec,
    strDurSubSec,
    strDurSub,
    equalsSub: strDurSubSec === strDurSub,
    strDur2,
    strCalcDur2,
    equalsDur2: strDur2 === strCalcDur2,
  });
}
