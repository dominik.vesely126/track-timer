import { IfsUpdateTimesheetHoursItem } from "../../../lib/types";
import { parseDurationFromHours } from "../../../lib/utils";
import {
  getIfsTimesheetProjectId,
  getIfsTimesheetWorkOrderId,
  getIfsUpdateSelection,
  IfsNativeAddTimesheet,
  IfsNativeBaseTimesheet,
  IfsNativeDeleteTimesheet,
  IfsNativeProjectTimesheet,
  IfsNativeTimesheet,
  IfsNativeTimesheetCollection,
  IfsNativeUpdateTimesheet,
  IfsNativeWorkOrderTimesheet,
  IfsUpdateSelection,
  parseIfsTimesheetWorkOrderId,
} from "../../../server/apis";
import { ifsTimesheetCollectionMock } from "../../mock/ifs/ifsTimesheet.mock";

const COMPANY_ID = "RETIA";
const EMP_NO = 232;

describe("test ifs server data", () => {
  const mock = ifsTimesheetCollectionMock;

  const result = IfsNativeTimesheetCollection.decode(mock);
  expect(result._tag).toEqual("Right");
});

describe("create ifs task id", () => {
  test("create project id", () => {
    const input = {
      Col1: "Test20.AA.BB",
      Col2: "To ignore",
    } as IfsNativeProjectTimesheet;

    expect(getIfsTimesheetProjectId(input)).toEqual("AA.BB");
  });

  test("create work order id", () => {
    const input = {
      Col1: "WO20 - t",
      Col2: "Task30 - v",
    } as IfsNativeWorkOrderTimesheet;

    expect(getIfsTimesheetWorkOrderId(input)).toEqual("WO20 - t_Task30 - v");
  });

  test("parse task id to WO and WT", () => {
    const taskId = "WO10_TT10";

    expect(parseIfsTimesheetWorkOrderId(taskId)).toEqual({
      WorkOrder: "WO10",
      WorkOperation: "TT10",
    });
  });
});

describe("basic update selection", () => {
  test("should add 2 items", () => {
    expectUpdateSelection(
      [
        {
          date: "2021-11-01",
          hours: 12.34,
          taskType: "Project",
          taskId: "10",
        },
        {
          date: "2021-11-01",
          hours: 2.21,
          taskType: "WorkOrder",
          taskId: "10_20",
        },
      ],
      {
        toAdd: [
          {
            type: "Project",
            date: "2021-11-01",
            hours: 12.34,
            shortName: "10",
            costCode: "Cost",
          },
          {
            type: "WorkOrder",
            date: "2021-11-01",
            hours: 2.21,
            workOrder: "10",
            workTask: "20",
          },
        ],
      }
    );
  });

  test("should update 2 items", () => {
    expectUpdateSelection(
      [
        {
          taskType: "Project",
          date: "2021-10-01",
          hours: 6.78,
          taskId: "100",
        },
        {
          taskType: "WorkOrder",
          date: "2021-10-01",
          hours: 1.36,
          taskId: "31_41",
        },
      ],
      {
        toUpdate: [
          {
            type: "Project",
            Objid: "2",
            Objversion: "v2",
            date: "2021-10-01",
            hours: 6.78,
            resourceId: "RES_ID_1",
            shortName: "100",
          },
          {
            type: "WorkOrder",
            Objid: "4",
            Objversion: "v4",
            date: "2021-10-01",
            hours: 1.36,
            workOrder: "31",
            workTask: "41",
          },
        ],
      }
    );
  });

  test("should delete 2 items", () => {
    expectUpdateSelection(
      [
        {
          taskType: "Project",
          date: "2021-10-01",
          hours: 0,
          taskId: "100",
        },
        {
          taskType: "WorkOrder",
          date: "2021-10-01",
          hours: -2,
          taskId: "31_41",
        },
      ],
      {
        toDelete: [
          {
            type: "Project",
            date: "2021-10-01",
            objId: "2",
            objVersion: "v2",
          },
          {
            type: "WorkOrder",
            date: "2021-10-01",
            objId: "4",
            objVersion: "v4",
          },
        ],
      }
    );
  });

  test("should not add negative or zero items", () => {
    expectUpdateSelection(
      [
        {
          taskType: "Project",
          date: "2022-10-01",
          hours: 0,
          taskId: "100",
        },
        {
          taskType: "WorkOrder",
          date: "2022-10-01",
          hours: -2,
          taskId: "31_41",
        },
      ],
      {}
    );
  });
});

describe("combined update selection", () => {
  test("should update 1 and delete 2", () => {
    expectUpdateSelection(
      [
        {
          taskType: "Project",
          date: "2021-10-01",
          hours: 20,
          taskId: "10",
        },
      ],
      {
        toUpdate: [
          {
            type: "Project",
            Objid: "1",
            Objversion: "v1",
            resourceId: "RES_ID_1",
            date: "2021-10-01",
            hours: 20,
            shortName: "10",
          },
        ],

        toDelete: [
          {
            type: "Project",
            date: "2021-10-01",
            objId: "10",
            objVersion: "v10",
          },
          {
            type: "Project",
            date: "2021-10-01",
            objId: "20",
            objVersion: "v20",
          },
        ],
      }
    );
  });
});

interface UpdateSelection {
  toAdd?: ToAddItem[];
  toUpdate?: ToUpdateItem[];
  toDelete?: ToDeleteItem[];
}

function expectUpdateSelection(
  items: UpdateTimesheetHours[],
  expected: UpdateSelection
) {
  const result = getIfsUpdateSelection({
    companyId: COMPANY_ID,
    empNo: EMP_NO,
    serverItems: serverTimesheets(),
    updateItems: updateItems(items),
    resourceIdsByName: {
      RES_NAME_1: "RES_ID_1",
      RES_NAME_2: "RES_ID_2",
    },
  });

  expect(result).toEqual({
    toAdd: (expected.toAdd && expected.toAdd.map(toAddItem)) || [],
    toUpdate: (expected.toUpdate && expected.toUpdate.map(toUpdateItem)) || [],
    toDelete: (expected.toDelete && expected.toDelete.map(toDeleteItem)) || [],
  } as IfsUpdateSelection);
}

type ToBaseItem = {
  date: string;
  hours: number;
};

type ToAddItem = ToBaseItem &
  (
    | {
        type: "Project";
        shortName: string;
        costCode: string;
      }
    | {
        type: "WorkOrder";
        workOrder: string;
        workTask: string;
      }
  );

function toAddItem(item: ToAddItem): IfsNativeAddTimesheet {
  switch (item.type) {
    case "Project":
      return {
        AccountDateDate: new Date(item.date),
        DayHours: parseDurationFromHours(item.hours),
        ShortName: item.shortName,
        ReportCostCode: item.costCode,
        CompanyId: COMPANY_ID,
        JobTransactionParams: {},
        EmpNo: EMP_NO + "",
        AttPrjrep: true,
      };

    case "WorkOrder":
      return {
        AccountDateDate: new Date(item.date),
        DayHours: parseDurationFromHours(item.hours),
        WorkOrder: item.workOrder,
        WorkTask: item.workTask,
        CompanyId: COMPANY_ID,
        EmpNo: EMP_NO + "",
        JobTransactionParams: {},

        AttWo: true,
        MaintOrg: COMPANY_ID,
      };
  }
}

type ToUpdateItem = ToBaseItem & {
  Objid: string;
  Objversion: string;
} & (
    | {
        type: "Project";
        shortName: string;
        resourceId: string;
      }
    | {
        type: "WorkOrder";
        workOrder: string;
        workTask: string;
      }
  );

function toUpdateItem(item: ToUpdateItem): IfsNativeUpdateTimesheet {
  switch (item.type) {
    case "Project":
      return {
        EntryType: "PRJREP",
        AccountDateDate: new Date(item.date),
        DayHours: parseDurationFromHours(item.hours),
        CompanyId: COMPANY_ID,
        EmpNo: EMP_NO + "",
        JobTransactionParams: {},
        Objid: item.Objid,
        Objversion: item.Objversion,
        ShortName: item.shortName,
        ResourceId: item.resourceId,
      };

    case "WorkOrder":
      return {
        EntryType: "WO",
        AccountDateDate: new Date(item.date),
        DayHours: parseDurationFromHours(item.hours),
        CompanyId: COMPANY_ID,
        EmpNo: EMP_NO + "",
        JobTransactionParams: {},
        MaintOrg: COMPANY_ID,
        Objid: item.Objid,
        Objversion: item.Objversion,
        WorkOrder: item.workOrder,
        WorkTask: item.workTask,
      };
  }
}

type ToDeleteItem = {
  type: "Project" | "WorkOrder";
  date: string;
  objId: string;
  objVersion: string;
};

function toDeleteItem(item: ToDeleteItem): IfsNativeDeleteTimesheet {
  switch (item.type) {
    case "Project":
      return {
        Module: "PRJREP",
        AccountDate: new Date(item.date),
        Objid: item.objId,
        Objversion: item.objVersion,
        CompanyId: COMPANY_ID,
        EmpNo: EMP_NO + "",
      };

    case "WorkOrder":
      return {
        Module: "WO",
        AccountDate: new Date(item.date),
        Objid: item.objId,
        Objversion: item.objVersion,
        CompanyId: COMPANY_ID,
        EmpNo: EMP_NO + "",
      };
  }
}

function updateItems(
  items: UpdateTimesheetHours[]
): IfsUpdateTimesheetHoursItem[] {
  return items.map(createUpdateItem);
}

type UpdateTimesheetHours = Pick<
  IfsUpdateTimesheetHoursItem,
  "taskId" | "taskType"
> & {
  date: string;
  hours: number;
};

function createUpdateItem(
  itm: UpdateTimesheetHours
): IfsUpdateTimesheetHoursItem {
  return {
    ...itm,
    date: new Date(itm.date),
    duration: parseDurationFromHours(itm.hours),
  };
}

function serverTimesheets(): IfsNativeTimesheet[] {
  const { project, wo } = createGenerator();
  return [
    project({
      Objid: "1",
      Objversion: "v1",
      date: "2021-10-01",
      Col1: "10",
      Col2: "20",
      ResourceName: "RES_NAME_1",
      hours: 2,
    }),
    project({
      Objid: "10",
      Objversion: "v10",
      date: "2021-10-01",
      Col1: "10",
      Col2: "20",
      ResourceName: "RES_NAME_1",
      hours: 4,
    }),
    project({
      Objid: "20",
      Objversion: "v20",
      date: "2021-10-01",
      Col1: "10",
      Col2: "20",
      ResourceName: "RES_NAME_1",
      hours: 6,
    }),
    project({
      Objid: "2",
      Objversion: "v2",
      date: "2021-10-01",
      Col1: "100",
      Col2: "20",
      ResourceName: "RES_NAME_1",
      hours: 2,
    }),
    wo({
      Objid: "3",
      Objversion: "v3",
      date: "2021-10-01",
      Col1: "30",
      Col2: "40",
      hours: 2,
    }),
    wo({
      Objid: "5",
      Objversion: "v5",
      date: "2021-10-01",
      Col1: "30",
      Col2: "40",
      hours: 3,
    }),
    wo({
      Objid: "4",
      Objversion: "v4",
      date: "2021-10-01",
      Col1: "31",
      Col2: "41",
      hours: 2,
    }),
  ];
}

function createGenerator() {
  const g = timesheetGenerator("ID");
  return {
    project(i: NativeProjectTimesheet) {
      return createProjectTimesheet(g.next(), i);
    },
    wo(i: NativeWorkOrderTimesheet) {
      return createWorkOrderTimesheet(g.next(), i);
    },
  };
}

type NativeProjectTimesheet = Pick<
  IfsNativeProjectTimesheet,
  "ResourceName" | "Col1" | "Col2" | "Objid" | "Objversion"
> & {
  date: string;
  hours: number;
};

type NativeWorkOrderTimesheet = Pick<
  IfsNativeWorkOrderTimesheet,
  "Col1" | "Col2" | "Objid" | "Objversion"
> & {
  date: string;
  hours: number;
};

function createProjectTimesheet(
  base: NativeBaseTimesheet,
  data: NativeProjectTimesheet
): IfsNativeProjectTimesheet {
  const obj: IfsNativeProjectTimesheet = {
    ...base,
    ...data,
    Module: "PRJREP",
    AccountDate: new Date(data.date),
    Hours: parseDurationFromHours(data.hours),
    Activity: "",
    Description: "",
  };

  return obj;
}

function createWorkOrderTimesheet(
  base: NativeBaseTimesheet,
  data: NativeWorkOrderTimesheet
): IfsNativeWorkOrderTimesheet {
  const obj: IfsNativeWorkOrderTimesheet = {
    ...base,
    ...data,
    Module: "WO",
    AccountDate: new Date(data.date),
    Hours: parseDurationFromHours(data.hours),
    WorkOrder: data.Col1 + " name",
    WorkTask: data.Col2 + " name",
  };
  return obj;
}

type NativeBaseTimesheet = Omit<
  IfsNativeBaseTimesheet,
  "Objid" | "Objversion" | "AccountDate" | "Hours" | "Col1" | "Col2"
>;

interface Generator {
  next: () => NativeBaseTimesheet;
}

function timesheetGenerator(prefix: string): Generator {
  let id = 0;

  return {
    next(): NativeBaseTimesheet {
      id++;
      //   const objId = `${prefix}_${id}`;
      return {
        CompanyId: COMPANY_ID,
        CompanyPersonRef: null,
        CompanySiteRef: null,
        EmpNo: EMP_NO + "",
        // EmployeeName: "DV",
        Label: "lbl",
        keyref: "keyref",
      };
    },
  };
}
