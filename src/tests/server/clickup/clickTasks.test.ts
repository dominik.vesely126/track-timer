import { ClickExternalName } from "../../../lib/types";
import { getTaskExternalIdByTaskName } from "../../../server/apis";

describe("get task by external name", () => {
  test("ifs not found", () => {
    expectTaskExternalId("IF07.07-11", "IFS", undefined);
    expectTaskExternalId("FS07.07-11", "IFS", undefined);
    expectTaskExternalId("bIFSa", "IFS", "a");
  });

  test("ifs project name", () => {
    expectTaskExternalId("IFS07.07-11", "IFS", "07.07-11");
    expectTaskExternalId("task name IFS07.07-11", "IFS", "07.07-11");
    expectTaskExternalId("asdIFS07.07-11 vvv", "IFS", "07.07-11");
  });

  test("ifs get last IFS prefix", () => {
    expectTaskExternalId("asd asd IFS00 IFS33 IFS44", "IFS", "44");
    expectTaskExternalId("asd asd IFS00 IFS33 IFS44 bbb", "IFS", "44");
  });

  test("ifs work order", () => {
    expectTaskExternalId("IFS123_458", "IFS", "123_458");
    expectTaskExternalId("Task anme IFS123_458", "IFS", "123_458");
    expectTaskExternalId("dalsi IFS123_458 konec", "IFS", "123_458");
  });

  test("rm task id", () => {
    expectTaskExternalId("#14366", "RM", "14366");
    expectTaskExternalId("#1", "RM", "1");
    expectTaskExternalId("#1a4366", "RM", "1");
    expectTaskExternalId("456", "RM", undefined);

    expectTaskExternalId("asd asd a b#456a asd", "RM", "456");
  });

  function expectTaskExternalId(
    input: string,
    type: ClickExternalName,
    expected: string | undefined
  ) {
    const actual = getTaskExternalIdByTaskName(input, type);
    expect({
      input,
      result: actual,
    }).toEqual({
      input,
      result: expected,
    });
  }
});
