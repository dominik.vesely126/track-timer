FROM node:16

WORKDIR /opt/redat/app

COPY package*.json ./
COPY src/server/package*.json ./src/server/
COPY src/lib/package*.json ./src/lib/

# RUN ls -la ./server

RUN npm install
RUN cd src/server && npm install
RUN cd src/lib && npm install

# app dir

# install app dep.ends
COPY src/* ./src/
COPY src/server/ ./src/server/
COPY src/lib/ ./src/lib/

# prod npm ci --only=production

# bundle app source
# COPY . .

EXPOSE 8080

CMD ["npm", "run", "server:dev"]
# CMD ["echo", "test"]